using System;
using Irrelephant.SurgeProtector.Game;

namespace Irrelephant
{
    public class Host : IConsoleCommand
    {
        private string[] names;
        public string[] Names
        {
            get
            {
                return names ?? (names = new[] { "host", "h" });
            }
        }

        public string HelpString
        {
            get
            {
                return "Starts up the game server and joins game\nUsage:\nhost (Starts the server and joins the game)\nhost 4445 (Starts dedicated on port 4445)";
            }
        }

        public string ToExecute(String actionName, params string[] args)
        {
            throw new NotImplementedException("Networking is scrapped for now.");
        }

    }
}
