using System;
using System.Linq;
using Irrelephant.SurgeProtector.Game;
using Irrelephant.SurgeProtector.Game.Entities;

namespace Irrelephant
{
    public class Kill : IConsoleCommand
    {
        private string[] names;

        public string[] Names
        {
            get { return names ?? (names = new[] { "kill", "die" }); }
        }

        public string HelpString
        {
            get
            {
                return
                    "Destroys the current player's ship (commits suicide) or destroys specific ship by its ID (in current instance).";
            }
        }

        public string ToExecute(String actionName, params string[] args)
        {
            if (args.Length == 0)
            {
                GameManager.Instance.PlayerShip.TakeDamage(int.MaxValue);
                return "Suicided";
            }

            var id = int.Parse(args[0]);
            var entity = (EntityShip)GameManager.Instance.CurrentInstance.Entities.FirstOrDefault(e => e.Id == id);
            if (entity == null)
            {
                return "No such ship.";
            }

            entity.TakeDamage(int.MaxValue);
            return "Killed id =" + id;
        }
    }
}
