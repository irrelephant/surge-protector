﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Irrelephant
{
    /// <summary>
    /// Represents command information and command parser functionality.
    /// </summary>
    public class CommandInfo
    {
        private static readonly Regex regex = new Regex(@"[\""].+?[\""]|[^ ]+");

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="commandName">Command name.</param>
        /// <param name="actionName">Action name.</param>
        /// <param name="arguments">Command arguments.</param>
        private CommandInfo(String commandName, String actionName, String[] arguments)
        {
            CommandName = commandName;
            ActionName = (actionName ?? string.Empty).Replace("-", String.Empty);
            Arguments = arguments ?? new String[0];
        }

        #region Properties

        /// <summary>
        /// Gets command name.
        /// </summary>
        public String CommandName
        {
            get; private set;
        }

        /// <summary>
        /// Gets command action name.
        /// </summary>
        public String ActionName
        {
            get; private set;
        }

        /// <summary>
        /// Gets command arguments.
        /// </summary>
        public String[] Arguments
        {
            get; private set;
        }

        #endregion Properties

        /// <summary>
        /// Parse string into command info.
        /// </summary>
        /// <param name="commandLine">String with command line.</param>
        /// <returns>Returns <c>CommandInfo</c> instance.</returns>
        public static CommandInfo Parse(String commandLine)
        {
            var allWords = regex.Matches(commandLine)
                .Cast<Match>()
                .Select(m => m.Value)
                .ToList();

            if (allWords.Count > 1)
            {
                var array = allWords.Skip(1).ToArray();

                var secondParameter = allWords[1];
                var actionName = String.Empty;
                if (secondParameter.StartsWith("-"))
                {
                    actionName = secondParameter;
                    array = array.ToArray();
                }


                return new CommandInfo(allWords[0], actionName, array);
            }

            return new CommandInfo(allWords[0], null, null);
        }
    }
}
