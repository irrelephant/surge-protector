using Irrelephant.SurgeProtector.Game;
using System;
using System.Linq;
using Irrelephant.SurgeProtector.Game.Data;
using Irrelephant.SurgeProtector.Game.Entities;
using UnityEngine;
using Irrelephant.SurgeProtector.UI.Hud.Chat;

namespace Irrelephant
{
    public class ListStuff : IConsoleCommand
    {
        private string[] names;
        public string[] Names
        {
            get
            {
                return names ?? (names = new[] { "list", "l" });
            }
        }

        public string HelpString
        {
            get
            {
                return "Lists stuff. Can 'list entities or l e', 'list instances or l i', 'list ships' or 'l s'";
            }
        }

        public string ToExecute(String action, params string[] args)
        {
            if (args.Length > 0)
            {
                switch (args[0])
                {
                    case "entities":
                    case "e":
                        return ListEntities();

                    case "ships":
                    case "s":
                        return ListShips();
                }
            }

            return "Somethign went wrong. Try 'man'ning the command first?".Color(Color.red);
        }


        private static string GetSpawnableType(GameObjectLibrary.LibraryEntry spawnable)
        {
            var entity = spawnable.Prefab.GetComponent<Entity>();
            if (entity == null)
            {
                return "Object";
            }
            if (entity is EntityShip)
            {
                return "Ship";
            }

            return "Entity";
        }

        private static string ListShips()
        {
            return "SHIPS: " + Environment.NewLine + string.Join(Environment.NewLine,
                       GameManager.Instance.CurrentInstance.Entities.Where(entity => entity is EntityShip).Select(entity => entity.Id + ": " + entity.gameObject.name + "(" + entity.ScriptableTag + ")").ToArray());
        }

        private static string ListEntities()
        {
            return "ENTITIES: " + Environment.NewLine + string.Join(Environment.NewLine,
                       GameManager.Instance.CurrentInstance.Entities.Select((entity) => entity.Id + ": " + entity.gameObject.name+ "(" + entity.ScriptableTag + ")").ToArray());
        }
    }
}
