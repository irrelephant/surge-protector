using System;
using System.Collections.Generic;
using System.Globalization;
using Irrelephant.SurgeProtector.Game;
using Irrelephant.SurgeProtector.Tooling.Debuggery;
using Irrelephant.SurgeProtector.UI.Hud.Chat;
using UnityEngine;

namespace Irrelephant
{
    public class ControlDebuggery : IConsoleCommand
    {
        private string[] names;
        public string[] Names
        {
            get
            {
                return names ?? (names = new[] { "dbg", "debuggery" });
            }
        }

        public string HelpString
        {
            get
            {
                return "Enables / disables Debuggery rendering, spawns debuging shapes." + Environment.NewLine + "Subcommands: 1/0; clear; color RRGGBB; line fromX fromY toX toY; box x y sizeX sizeY";
            }
        }

        private Color currentColor = Color.magenta;

        public string ToExecute(String actionName, params string[] args)
        {
            if (args.Length == 0)
            {
                Debuggery.Enabled = !Debuggery.Enabled;
                return "Showing Debuggery: " + Debuggery.Enabled;
            }

            int intAgument;
            if (int.TryParse(args[0], out intAgument))
            {
                Debuggery.Enabled = intAgument != 0;
                return "Showing Debuggery: " + Debuggery.Enabled;
            }

            if (args[0].ToLowerInvariant() == "clear")
            {
                Debuggery.AllLines = new List<DebuggeryLine>();
                Debuggery.AllBoxes = new List<DebuggeryBox>();
                return "OK!";
            }

            if (args.Length == 1 && args[0].ToLowerInvariant() == "color")
            {
                return "The color is <color=" + currentColor.AsHexString() + ">" + currentColor.AsHexString() + "</color>";
            }

            if (args[0].ToLowerInvariant() == "color")
            {
                var argb = int.Parse(args[1], NumberStyles.AllowHexSpecifier);
                currentColor = new Color(((argb & 0xff0000) >> 0x10) / 255f, ((argb & 0xff00) >> 0x8) / 255f, (argb & 0xff) / 255f);
                return "The new color is <color=" + currentColor.AsHexString() + ">" + currentColor.AsHexString() + "</color>";
            }

            if (args.Length == 5 && args[0].ToLowerInvariant() == "line")
            {
                Debuggery.Line(new Vector2(float.Parse(args[1]), float.Parse(args[2])),
                               new Vector2(float.Parse(args[3]), float.Parse(args[4])),
                               currentColor);

                return string.Format("Debuggery: Added {0}", "line".Color(currentColor));
            }

            if (args.Length == 5 && args[0].ToLowerInvariant() == "box")
            {
                Debuggery.Box(new Vector2(float.Parse(args[1]), float.Parse(args[2])),
                    new Vector2(float.Parse(args[3]), float.Parse(args[4])),
                    currentColor, true);

                return string.Format("Debuggery: Added {0}", "box".Color(currentColor));
            }

            return "<color=red>Something went wrong :C</color>";
        }
    }
}
