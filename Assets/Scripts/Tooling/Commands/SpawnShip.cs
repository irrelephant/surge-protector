using System;
using Irrelephant.SurgeProtector.Game;
using Irrelephant.SurgeProtector.Game.AI.Orders;
using Irrelephant.SurgeProtector.Game.Entities;
using UnityEngine;

namespace Irrelephant
{
    public class SpawnShip : IConsoleCommand
    {
        private string[] names;

        public string[] Names
        {
            get { return names ?? (names = new[] { "spawn" }); }
        }

        public string HelpString
        {
            get
            {
                return
                    "Spawns an obejct/entity or a ship. Ships can be 'warped in'";
            }
        }

        public string ToExecute(String actionName, params string[] args)
        {
            if (args.Length < 2)
            {
                return "example: spawn CommandCruiser --warpin or spawn MobileRefinery 10 10";
            }

            if (args.Length >= 2)
            {
                var spawned = SpawningManager.Instance.SpawnDynamicShipPrefab(args[0]);
                spawned.transform.SetParent(GameManager.Instance.CurrentInstance.Container);

                var entity = spawned.GetComponent<Entity>();
                var ship = entity as EntityShip;

                if (args[1].ToLowerInvariant() == "--warpin")
                {
                    if (ship != null)
                    {
                        ship.OrderManager.HaltAndGiveOrder(new OrderWarpTo(GameManager.Instance.CurrentInstance));
                    }
                }
                else
                {
                    var entry = new Vector3(float.Parse(args[1]), float.Parse(args[2]));
                    if (ship != null)
                    {
                        GameManager.Instance.CurrentInstance.JoinInstance(ship, entry);
                    } else if (entity != null)
                    {
                        entity.Unsuspend(entry);
                    }
                    else
                    {
                        spawned.transform.position = entry;
                    }
                }
            }
            return "OK";
        }
    }
}
