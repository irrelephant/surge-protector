using System;
using Irrelephant.SurgeProtector.Game;

namespace Irrelephant
{
    public class Connect : IConsoleCommand
    {
        private string[] names;
        public string[] Names
        {
            get
            {
                return names ?? (names = new[] { "connect", "join", "c", "j" });
            }
        }

        public string HelpString
        {
            get
            {
                return "Connects to a remote game";
            }
        }

        public string ToExecute(String actionName, params string[] args)
        {
            throw new NotImplementedException("Networking is scrapped for now.");
        }
    }
}
