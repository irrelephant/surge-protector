using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Irrelephant
{
    public class Editor : IConsoleCommand
    {
        private string[] names;
        public string[] Names
        {
            get
            {
                return names ?? (names = new[] { "editor" });
            }
        }

        public string HelpString
        {
            get
            {
                return "Loads the editor scene";
            }
        }

        public string ToExecute(String actionName, params string[] args)
        {
            SceneManager.LoadScene("edit-scenario");
            return "OK";
        }
    }
}
