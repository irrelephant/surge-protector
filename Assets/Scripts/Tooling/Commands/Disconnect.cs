using System;
using Irrelephant.SurgeProtector.Game;

namespace Irrelephant
{
    public class Disconect : IConsoleCommand
    {
        private string[] names;
        public string[] Names
        {
            get
            {
                return names ?? (names = new[] { "disconnect", "dc" });
            }
        }

        public string HelpString
        {
            get
            {
                return "Disconnects from the current game.";
            }
        }

        public string ToExecute(String actionName, params string[] args)
        {
            throw new NotImplementedException("Networking is scrapped for now.");
        }

    }
}
