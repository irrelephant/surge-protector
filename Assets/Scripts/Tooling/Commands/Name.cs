using System;

namespace Irrelephant
{
    public class Name : IConsoleCommand
    {
        private string[] names;
        public string[] Names
        {
            get
            {
                return names ?? (names = new[] { "name", "nickname" });
            }
        }

        public string HelpString
        {
            get
            {
                return "Sets up your player name";
            }
        }

        public string ToExecute(String actionName, params string[] args)
        {
            throw new NotImplementedException("Networking is scrapped for now.");
        }

    }
}
