﻿using System;

namespace Irrelephant
{
    /// <summary>
    /// Interface for in game console which every command must implement.
    /// </summary>
    public interface IConsoleCommand
    {
        /// <summary>
        /// Gets the command name wich will be used in in-game console.
        /// </summary>
        /// <returns>The command name.</returns>
        string[] Names { get; }

        /// <summary>
        /// Gets the help string to this command.
        /// </summary>
        /// <returns>The help.</returns>
        string HelpString { get; }

        /// <summary>
        /// Callback that will be executed when command triggered in console.
        /// </summary>
        /// <returns>The result string</returns>
        /// <param name="args">Arguments.</param>
        string ToExecute(String actionName, params string[] args);
    }
}