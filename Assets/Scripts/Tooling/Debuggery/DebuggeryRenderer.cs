﻿using UnityEngine;

namespace Irrelephant.SurgeProtector.Tooling.Debuggery
{
    public class DebuggeryRenderer : MonoBehaviour
    {
        static Material lineMaterial;
        static void CreateLineMaterial()
        {
            if (!lineMaterial)
            {
                var shader = Shader.Find("Hidden/Internal-Colored");
                lineMaterial = new Material(shader) { hideFlags = HideFlags.HideAndDontSave };
                lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
                lineMaterial.SetInt("_ZWrite", 0);
            }
        }

        public void OnRenderObject()
        {
            if (!Debuggery.Enabled) return;
            CreateLineMaterial();

            GL.PushMatrix();
            GL.MultMatrix(transform.localToWorldMatrix);

            for (int i = 0; i < Debuggery.AllLines.Count; i++)
            {
                var line = Debuggery.AllLines[i];
                DrawLine(line);
            }

            for (int i = 0; i < Debuggery.AllBoxes.Count; i++)
            {
                var box = Debuggery.AllBoxes[i];
                DrawBox(box);
            }

            for (int i = 0; i < Debuggery.AllCircles.Count; i++)
            {
                var circle = Debuggery.AllCircles[i];
                DrawCircle(circle);
            }

            GL.PopMatrix();
        }

        private static void DrawCircle(DebuggeryCircle circle) {
            const int CircleLod = 64;
            lineMaterial.SetPass(0);
            GL.Begin(GL.LINES);
            GL.Color(circle.Color);
            for (int i = 0; i < CircleLod; i++) {
                float theta = 2f * Mathf.PI * i / CircleLod;
                GL.Vertex3(circle.Pos.x + circle.Radius * Mathf.Cos(theta), circle.Pos.y + circle.Radius * Mathf.Sin(theta), -1);
            }
            GL.End();
        }

        private static void DrawLine(DebuggeryLine line)
        {
            lineMaterial.SetPass(0);
            GL.Begin(GL.LINES);
            GL.Color(line.Color);
            GL.Vertex3(line.From.x, line.From.y, -1);
            GL.Vertex3(line.To.x, line.To.y, -1);
            GL.End();
        }

        private static void DrawBox(DebuggeryBox box)
        {
            lineMaterial.SetPass(0);
            GL.Begin(GL.LINES);
            GL.Color(box.Color);

            GL.Vertex3(box.Pos.x - box.Size.x / 2, box.Pos.y - box.Size.y / 2, -1);
            GL.Vertex3(box.Pos.x - box.Size.x / 2, box.Pos.y + box.Size.y / 2, -1);

            GL.Vertex3(box.Pos.x - box.Size.x / 2, box.Pos.y + box.Size.y / 2, -1);
            GL.Vertex3(box.Pos.x + box.Size.x / 2, box.Pos.y + box.Size.y / 2, -1);

            GL.Vertex3(box.Pos.x + box.Size.x / 2, box.Pos.y + box.Size.y / 2, -1);
            GL.Vertex3(box.Pos.x + box.Size.x / 2, box.Pos.y - box.Size.y / 2, -1);

            GL.Vertex3(box.Pos.x + box.Size.x / 2, box.Pos.y - box.Size.y / 2, -1);
            GL.Vertex3(box.Pos.x - box.Size.x / 2, box.Pos.y - box.Size.y / 2, -1);

            if (box.Fill)
            {
                GL.Vertex3(box.Pos.x - box.Size.x / 2, box.Pos.y - box.Size.y / 2, -1);
                GL.Vertex3(box.Pos.x + box.Size.x / 2, box.Pos.y + box.Size.y / 2, -1);

                GL.Vertex3(box.Pos.x - box.Size.x / 2, box.Pos.y + box.Size.y / 2, -1);
                GL.Vertex3(box.Pos.x + box.Size.x / 2, box.Pos.y - box.Size.y / 2, -1);
            }

            GL.End();
        }
    }
}