﻿using System.Collections.Generic;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Tooling.Debuggery
{
    public static class Debuggery
    {
        public static List<DebuggeryLine> AllLines = new List<DebuggeryLine>();
        public static List<DebuggeryBox> AllBoxes = new List<DebuggeryBox>();
        public static List<DebuggeryCircle> AllCircles = new List<DebuggeryCircle>();


        public static Color[] DefaultColors = {
            Color.red,
            Color.green, 
            Color.blue,
            Color.magenta,
            Color.yellow,
            Color.clear,
            Color.white
        };

        public static bool Enabled = false;

        public static DebuggeryLine Line(Vector2 from, Vector2 to, Color color)
        {
            var line = new DebuggeryLine { From = from, To = to, Color = color };
            AllLines.Add(line);
            return line;
        }

        public static DebuggeryBox Box(Vector2 pos, Vector2 size, Color color, bool fill = false)
        {
            var box = new DebuggeryBox { Pos = pos, Size = size, Color = color, Fill = fill };
            AllBoxes.Add(box);
            return box;
        }

        public static DebuggeryCircle Circle(Vector2 pos, float radius, Color color) {
            var circle = new DebuggeryCircle { Pos = pos, Radius = radius, Color = color };
            AllCircles.Add(circle);
            return circle;
        }
        
    }
}