﻿using UnityEngine;

namespace Irrelephant.SurgeProtector.Tooling.Debuggery
{
    public class DebuggeryLine
    {
        public Vector2 From;
        public Vector2 To;
        public Color Color;
    }
}
