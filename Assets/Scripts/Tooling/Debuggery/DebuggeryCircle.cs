using UnityEngine;

namespace Irrelephant.SurgeProtector.Tooling.Debuggery
{
    public class DebuggeryCircle
    {
        public Vector2 Pos;
        public float Radius;
        public Color Color;
    }
}
