﻿using UnityEngine;

namespace Irrelephant.SurgeProtector.Tooling.Debuggery
{
    public class DebuggeryBox
    {
        public Vector2 Pos;
        public Vector2 Size;
        public Color Color;
        public bool Fill;
    }
}
