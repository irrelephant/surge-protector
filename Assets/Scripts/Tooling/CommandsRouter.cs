﻿using UnityEngine;
using System.Collections.Generic;

namespace Irrelephant
{
    /// <summary>
    /// Routes available console commands.
    /// </summary>
    public class CommandsRouter : MonoBehaviour
    {
        public static readonly List<IConsoleCommand> cmdList = new List<IConsoleCommand>
        {
            new Help(),
            new ListStuff(),
            new Kill(),
            new SpawnShip(),

            // Tooling
            new Editor(),
            new ControlDebuggery(),
            //=======

            // Networking commands are left in the list for now, but are not doing anything for now.
            new Connect(),
            new Host(),
            new Name(),
            new Disconect()
            //=======
        };

        public void Start()
        {
            var cons = ConsoleCommandsRepository.Instance;
            cmdList.ForEach(cmd =>
            {
                foreach (var alias in cmd.Names)
                {
                    cons.RegisterCommand(alias, cmd.ToExecute);
                }
            });
        }
    }
}
