﻿namespace Irrelephant.SurgeProtector.Tooling
{
    public enum EnumLogType
    {
        Plain, Info, Warning, Error, InstanceScript
    }
}
