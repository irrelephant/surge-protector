using UnityEngine;

public class ConsoleToggler : MonoBehaviour {
    private bool consoleEnabled;
    public ConsoleAction ConsoleOpenAction;
    public ConsoleAction ConsoleCloseAction;

    public void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ToggleConsole();
        }
    }

    private void ToggleConsole() {
        consoleEnabled = !consoleEnabled;
        if (consoleEnabled) {
            ConsoleOpenAction.Activate();
        } else {
            ConsoleCloseAction.Activate();
        }
    }
}
