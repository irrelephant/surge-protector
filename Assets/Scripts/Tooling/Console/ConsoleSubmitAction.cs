using System;
using Irrelephant;
using Irrelephant.SurgeProtector.Tooling;

public class ConsoleSubmitAction : ConsoleAction
{
    public ConsoleGUI consoleGUI;
    private ConsoleCommandsRepository consoleCommandsRepository;
    private ConsoleLog consoleLog;

    public void Start()
    {
        consoleCommandsRepository = ConsoleCommandsRepository.Instance;
        consoleLog = ConsoleLog.Instance;
    }

    public override void Activate()
    {
        var info = CommandInfo.Parse(consoleGUI.input);

        consoleLog.Log("> " + consoleGUI.input);
        if (consoleCommandsRepository.HasCommand(info.CommandName))
        {
            ExecCommandLoggingResult(info);
        }
        else
        {
            consoleLog.Log("Command " + info.CommandName + " not found", EnumLogType.Error);
        }
    }

    private void ExecCommandLoggingResult(CommandInfo info)
    {
        try
        {
            consoleLog.Log(consoleCommandsRepository.ExecuteCommand(info));
        }
        catch (Exception e)
        {
            consoleLog.Log(e.Message, EnumLogType.Error);
            throw;
        }
    }
}
