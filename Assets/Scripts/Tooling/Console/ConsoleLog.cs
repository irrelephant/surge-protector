using System;
using Irrelephant;
using Irrelephant.SurgeProtector.Tooling;
using Irrelephant.SurgeProtector.UI.Hud.Chat;
using UnityEngine;

public class ConsoleLog {
    private static ConsoleLog instance;
    public static ConsoleLog Instance {
        get {
            if (instance == null) {
                instance = new ConsoleLog();
            }
            return instance;
        }
    }

    public string log = "";

    private string GetPrefix(EnumLogType type)
    {
        switch (type)
        {
            case EnumLogType.Plain:
                return string.Empty;
            case EnumLogType.Info:
                return "[INFO] ".Color(Color.gray).Bold();
            case EnumLogType.Warning:
                return "[WARN] ".Color(Color.yellow).Bold();
            case EnumLogType.Error:
                return "[ERR]  ".Color(Color.red).Bold();
            case EnumLogType.InstanceScript:
                return "[LUA] ".Color(Color.blue).Bold();
            default:
                return null;
        }
    }

    public void Log(string message, EnumLogType type = EnumLogType.Plain)
    {
        log += GetPrefix(type) + message + Environment.NewLine;
    }

    public void LogFormat(string format, params object[] parameters)
    {
        LogFormat(format, EnumLogType.Plain, parameters);
    }

    public void LogFormat(string format, EnumLogType type, params object[] parameters)
    {
        Log(String.Format(format, parameters), type);
    }
}
