using UnityEngine;

public class ConsoleCloseAction : ConsoleAction {
    public GameObject ConsoleGui;

    private bool activated;
    public override void Activate() {
        activated = true;
    }

    public void Update()
    {
        if (activated)
        {
            ConsoleGui.SetActive(false);
        }
        activated = false;
    }
}
