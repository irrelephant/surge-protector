using System;
using System.Collections.Generic;
using Irrelephant;

public delegate string ConsoleCommandCallback(String actionName, params string[] args);

public class ConsoleCommandsRepository
{
    private static ConsoleCommandsRepository instance;
    private Dictionary<string, ConsoleCommandCallback> repository;

    public static ConsoleCommandsRepository Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new ConsoleCommandsRepository();
            }
            return instance;
        }
    }
    public ConsoleCommandsRepository()
    {
        repository = new Dictionary<string, ConsoleCommandCallback>();
    }

    public void RegisterCommand(string command, ConsoleCommandCallback callback)
    {
        repository[command] = callback;
    }

    public bool HasCommand(string command)
    {
        return repository.ContainsKey(command);
    }

    public string ExecuteCommand(CommandInfo info)
    {
        if (HasCommand(info.CommandName))
        {
            return repository[info.CommandName](info.ActionName, info.Arguments);
        }
        throw new ArgumentException("Command " + info.CommandName + " was not found");
    }
}
