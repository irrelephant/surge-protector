using System.Collections.Generic;
using Irrelephant.SurgeProtector.Tooling;
using Irrelephant.SurgeProtector.UI.Hud.Chat;
using Irrelephant.SurgeProtector.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Irrelephant.SurgeProtector.UI.Hud
{
    public enum ObjectiveState
    {
        New, Complete, Failed
    }

    public class Objective
    {
        public int Id;

        private string text;
        public string Text
        {
            get {
                return text;
            }
            set {
                text = value;
                if (ViewObject != null)
                {
                    UpdateView();
                }
            }
        }

        private ObjectiveState state;
        public ObjectiveState State
        {
            get { return state; }
            set
            {
                state = value;
                if (ViewObject != null)
                {
                    UpdateView();
                }
            }
        }

        private Text viewObject;

        public Text ViewObject
        {
            get { return viewObject; }
            set
            {
                viewObject = value;
                if (ViewObject != null)
                {
                    UpdateView();
                }
            }
        }

        private void UpdateView()
        {
            ViewObject.text = ToString();
        }

        private string GetStateMarker()
        {
            switch (State)
            {
                case ObjectiveState.New: return "   ".Size(8);
                case ObjectiveState.Complete: return "[v]".Color(Color.green).Size(8);
                case ObjectiveState.Failed: return "[x]".Color(Color.red).Size(8);
            }

            return null;
        }

        public override string ToString()
        {
            return (State != ObjectiveState.New ? Text.Color(new Color(0.7f, 0.7f, 0.7f)) : Text) + ' ' + GetStateMarker();
        }
    }

    public class ObjectiveDisplayController : MonoBehaviour
    {
        public GameObject ObjectiveLinePrefab;

        private readonly IDictionary<int, Objective> Objectives = new Dictionary<int, Objective>();

        public int AddObjective(string objective)
        {
            var objectiveView = Instantiate(ObjectiveLinePrefab, transform).GetComponent<Text>();
            var objectiveObj = new Objective
            {
                Id = IdGenerator.GetNextId(),
                Text = objective,
                State = ObjectiveState.New,
                ViewObject = objectiveView
            };

            Objectives[objectiveObj.Id] = objectiveObj;

            ConsoleLog.Instance.Log("new task ID " + objectiveObj.Id, EnumLogType.Info);

            return objectiveObj.Id;
        }

        public void CompleteObjective(int id)
        {
            var objectiveObj = Objectives[id];
            objectiveObj.State = ObjectiveState.Complete;
            ConsoleLog.Instance.Log("completed ID " + objectiveObj.Id, EnumLogType.Info);
        }

        public void FailObjective(int id)
        {
            var objectiveObj = Objectives[id];
            objectiveObj.State = ObjectiveState.Failed;
            ConsoleLog.Instance.Log("failed ID " + objectiveObj.Id, EnumLogType.Info);
        }

        public void EditObjective(int id, string newObjective)
        {
            var objective = FindById(id);
            if (objective != null)
            {
                objective.Text = newObjective;
            }
        }

        public Objective FindById(int id) 
        {
            return Objectives.ContainsKey(id)
                ? Objectives[id]
                : null;
        }
    }
}