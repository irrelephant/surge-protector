﻿using UnityEngine.EventSystems;

namespace Irrelephant.SurgeProtector.UI.Hud
{
    public class ScreenSpaceHoverCanvas : EventTrigger
    {
        public static bool OverUi
        {
            get { return hoverCount != 0; }
        }

        private static int hoverCount;

        public override void OnPointerEnter(PointerEventData eventData)
        {
            hoverCount++;
            base.OnPointerEnter(eventData);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            hoverCount--;
            base.OnPointerExit(eventData);
        }
    }
}