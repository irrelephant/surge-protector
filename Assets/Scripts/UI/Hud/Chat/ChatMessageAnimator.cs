﻿using System.Collections;
using UnityEngine;

namespace Irrelephant.SurgeProtector.UI.Hud.Chat
{
	public class ChatMessageAnimator : MonoBehaviour
	{
		private CanvasGroup canvasGroup;

		public void Awake() {
			canvasGroup = GetComponent<CanvasGroup>();
		}

		private IEnumerator FadeOutCoroutine(float waitFor) {
			yield return new WaitForSeconds(waitFor);
			
			var endOfFrame = new WaitForEndOfFrame();
			
			while (canvasGroup.alpha > 0) {
				yield return endOfFrame;
				canvasGroup.alpha -= 0.01f;
			}
		}

		public void FadeOutAfter(float seconds) {
			ShowImmediately();
			StartCoroutine(FadeOutCoroutine(seconds));
		}

		public void ShowImmediately() {
			StopAllCoroutines();
			canvasGroup.alpha = 1f;
		}
	}
}