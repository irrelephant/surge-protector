﻿using UnityEngine;

namespace Irrelephant.SurgeProtector.UI.Hud.Chat
{
    static class ChatExtensions
    {
        /// <summary>
        /// Template for the color tag construction
        /// </summary>
        private const string colorTagTemplate = "<color={0}>{1}</color>";

        /// <summary>
        /// Template for the hex color string
        /// </summary>
        private const string hexColorTemplate = "#{0}{1}{2}";

        /// <summary>
        /// Template for the bold tag construction
        /// </summary>
        private const string boldTagTemplate = "<b>{0}</b>";
        
        /// <summary>
        /// Template for the italic tag construction
        /// </summary>
        private const string italicTagTemplate = "<i>{0}</i>";

        /// <summary>
        /// Template for the size tag construction
        /// </summary>
        private const string sizeTagTemplate = "<size={0}>{1}</size>";

        /// <summary>
        /// Sets the color for the message
        /// </summary>
        /// <param name="message">Message to set color for</param>
        /// <param name="color">Color to use</param>
        /// <returns>Message wrapped in a color tag</returns>
        public static string Color(this string message, Color color)
        {
            return Color(message, color.AsHexString());
        }

        /// <summary>
        /// Sets the color for the message
        /// </summary>
        /// <param name="message">Message to set color for</param>
        /// <param name="color">Color to use</param>
        /// <returns>Message wrapped in a color tag</returns>
        public static string Color(this string message, string color)
        {
            return string.Format(colorTagTemplate, color, message);
        }

        /// <summary>
        /// Makes the text bold
        /// </summary>
        /// <param name="message">Message to make bold</param>
        /// <returns>Message wrapped in bold tag</returns>
        public static string Bold(this string message)
        {
            return string.Format(boldTagTemplate, message);
        }

        /// <summary>
        /// Makes the text italic
        /// </summary>
        /// <param name="message">Message to make italic</param>
        /// <returns>Message wrapped in italic tag</returns>
        public static string Italic(this string message)
        {
            return string.Format(italicTagTemplate, message);
        }

        /// <summary>
        /// Makes the text sized
        /// </summary>
        /// <param name="message">Message to make sized</param>
        /// <param name="size">Size to set</param>
        /// <returns>Message wrapped in size tag</returns>
        public static string Size(this string message, int size = 14)
        {
            return string.Format(sizeTagTemplate, size, message);
        }

        /// <summary>
        /// Builds a random color
        /// </summary>
        /// <param name="alpha">Alpha to use</param>
        /// <returns>Random color</returns>
        public static Color RandomColor(float alpha = 1f)
        {
            return new Color(Random.value, Random.value, Random.value, alpha);
        }

        /// <summary>
        /// Strikes through the text
        /// </summary>
        /// <param name="text">String to striketrough</param>
        /// <returns></returns>
        public static string StrikeThrough(this string text)
        {
            string strikethrough = "";
            foreach (char c in text)
            {
                strikethrough = strikethrough + c + '\u0336';
            }
            return strikethrough;
        }

        /// <summary>
        /// Clamps a float value between 0 and 1 into an int between 0 and 255
        /// </summary>
        /// <param name="val">Value to clamp</param>
        /// <returns>Value between 0 and 255</returns>
        private static int ClampToByte(float val)
        {
            return val == 1 ? 0xFF : (int)(0xFF * val);
        }

        /// <summary>
        /// Transforms the color value to its hex representation
        /// </summary>
        /// <param name="color">color to transform</param>
        /// <returns>Hex string in the following format: <b>#RRGGBBAA</b></returns>
        public static string AsHexString(this Color color)
        {
            const string doubleHexFormat = "X2";

            return string.Format(hexColorTemplate, ClampToByte(color.r).ToString(doubleHexFormat),
                                                   ClampToByte(color.g).ToString(doubleHexFormat),
                                                   ClampToByte(color.b).ToString(doubleHexFormat));
        }
    }
}
