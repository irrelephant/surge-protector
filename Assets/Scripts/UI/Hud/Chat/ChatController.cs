using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Irrelephant.SurgeProtector.UI.Hud.Chat
{
    /// <summary>
    /// Controls the chat window
    /// </summary>
    public class ChatController : MonoBehaviour
    {
        /// <summary>
        /// Reference to primary chat panel
        /// </summary>
        public RectTransform ChatPanel;

        /// <summary>
        /// Reference to message container scrolled and masked rect
        /// </summary>
        private RectTransform messageContainer;

        /// <summary>
        /// Chat message prefab
        /// </summary>
        public GameObject ChatMessagePrefab;

        /// <summary>
        /// Minimal amount of time it takes a message to start fading out 
        /// </summary>
        public float MessageBaseFadeoutTime = 5f;

        /// <summary>
        /// Amount of time each character in a message adds to fade-out timer
        /// </summary>
        public float MessageFadeoutPerChar = 0.05f;

        /// <summary>
        /// Amount of messages already created from the prefab
        /// </summary>
        private int messagesCreated = 0;

        /// <summary>
        /// Amount of messages to instantiate before reusing the existing ones
        /// </summary>
        public int MessagePoolSize = 3;

        /// <summary>
        /// Text generator used to recalaulate the container height
        /// </summary>
        private TextGenerator textGenerator;

        /// <summary>
        /// Initializes the component
        /// </summary>
        private void Start()
        {
            messageContainer = ChatPanel.GetChild(0).GetComponent<RectTransform>();
            textGenerator = new TextGenerator();
        }

        /// <summary>
        /// Instantiates new chat message object
        /// </summary>
        /// <param name="message">Text to show</param>
        /// <returns>Newly built text component</returns>
        private Text PrepareNewTextObject(string message)
        {
            var newChatMessage = GameObject.Instantiate(ChatMessagePrefab);
            var textComponent = newChatMessage.GetComponent<Text>();
            textComponent.transform.SetParent(messageContainer, true);
            textComponent.text = message;
            messagesCreated++;

            return textComponent;
        }

        /// <summary>
        /// Calculates the height of the text component
        /// </summary>
        /// <param name="textComponent">Text component to calculate the height for</param>
        /// <returns>Height of the component in pixels</returns>
        private float GetMessageHeight(Text textComponent)
        {
            return textGenerator.GetPreferredHeight(textComponent.text, textComponent.GetGenerationSettings(messageContainer.rect.size));
        }

        /// <summary>
        /// Reuse an existing Text component
        /// </summary>
        /// <param name="message">New text message to set</param>
        /// <param name="oldMessageSize">Height of an old message object</param>
        /// <returns>Reused text component</returns>
        private Text ReuseExistingText(string message, out float oldMessageSize)
        {
            var reusedChatMessage = messageContainer.GetChild(0);
            var textComponent = reusedChatMessage.GetComponent<Text>();
            oldMessageSize = GetMessageHeight(textComponent);
            textComponent.transform.SetAsLastSibling();
            textComponent.text = message;
            return textComponent;
        }

        /// <summary>
        /// Appends the message to the chat
        /// </summary>
        public void AppendMessage(string message)
        {
            Text textComponent;
            var newSize = messageContainer.sizeDelta;

            if (messagesCreated < MessagePoolSize)
            {
                textComponent = PrepareNewTextObject(message);
            }
            else
            {
                float oldHeight;
                textComponent = ReuseExistingText(message, out oldHeight);
                newSize.y -= oldHeight;
            }

            newSize.y += GetMessageHeight(textComponent);
            messageContainer.sizeDelta = newSize;
            
            var animator = textComponent.GetComponent<ChatMessageAnimator>();
            if (animator != null) {
                animator.FadeOutAfter(MessageBaseFadeoutTime + message.Length * MessageFadeoutPerChar);
            }
        }

        /// <summary>
        /// Appends formatted message to the chat
        /// </summary>
        /// <param name="format">Format string</param>
        /// <param name="arguments">Arguments</param>
        public void AppendMessageFormat(string format, params object[] arguments)
        {
            AppendMessage(string.Format(format, arguments));
        }
    }
}