﻿using System;
using System.Collections;
using UnityEngine;

namespace Irrelephant.SurgeProtector.UI.Hud {
    public class BlackoutOverlayController : MonoBehaviour
    {
        private CanvasGroup overlayCanvasGroup;

        public void Awake()
        {
            overlayCanvasGroup = GetComponent<CanvasGroup>();
        }

        private IEnumerator FadeRoutine(float seconds, bool fadeIn)
        {
            var endOfFrame = new WaitForEndOfFrame();

            while (true)
            {
                var deltaFade = (fadeIn ? 1 : -1) * Time.deltaTime / seconds;
                overlayCanvasGroup.alpha += deltaFade;

                if (fadeIn && 1f - Math.Abs(overlayCanvasGroup.alpha) < Mathf.Epsilon ||
                    !fadeIn && Mathf.Abs(overlayCanvasGroup.alpha) < Mathf.Epsilon) break;

                yield return endOfFrame;
            }
        }

        public void EnableOverlayOver(float seconds)
        {
            StartCoroutine(FadeRoutine(seconds, true));
            overlayCanvasGroup.blocksRaycasts = true;
        }

        public void DisableOverlayOver(float seconds)
        {
            StartCoroutine(FadeRoutine(seconds, false));
            overlayCanvasGroup.blocksRaycasts = false;
        }
    }
}