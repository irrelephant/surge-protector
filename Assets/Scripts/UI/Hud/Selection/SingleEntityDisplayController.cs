﻿using System;
using Irrelephant;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.UI.Hud.Chat;
using UnityEngine;
using UnityEngine.UI;

namespace Irrelephant.SurgeProtector.UI.Hud.Selection {
    
    public class SingleEntityDisplayController : MonoBehaviour
    {
        public Text HealthbarText;
        public Text HealthValue;
        public Text EntityTitle;
        public Color MainColor;
        public Color SecondaryColor;
        
        private EntityShip tracking;
        public EntityShip Tracking
        {
            set
            {
                if (tracking)
                {
                    tracking.OnDamageTaken -= OnTargetDamaged;
                }
                tracking = value;
                if (tracking)
                {
                    tracking.OnDamageTaken += OnTargetDamaged;
                    UpdateDisplay();
                    EntityTitle.text = value.ScriptableTag + "(" + value.Id + ")";
                }
            }
            get
            {
                return tracking;
            }
        }

        public const int PinsOnProgressBar = 25;

        private void OnTargetDamaged(float dmg)
        {
            UpdateDisplay();
        }

        public void UpdateDisplay()
        {
            var actualHealth = Mathf.Min(Tracking.MaxHealth, Tracking.Health);
            
            HealthValue.text = String.Format("[{0}/{1}]", actualHealth, Tracking.MaxHealth);
            var pinsCurrently = (int)(1f * (PinsOnProgressBar - 1) * actualHealth / Tracking.MaxHealth) + 1;
            HealthbarText.text = new String('I', pinsCurrently).Color(MainColor) + new String('I', PinsOnProgressBar - pinsCurrently).Color(SecondaryColor);
        }
    }
}