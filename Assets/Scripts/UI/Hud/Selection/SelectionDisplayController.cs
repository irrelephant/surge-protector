﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Irrelephant.SurgeProtector.Extensions;
using Irrelephant.SurgeProtector.Game.Entities;
using UnityEngine;

namespace Irrelephant.SurgeProtector.UI.Hud.Selection {

	public class SelectionDisplayController : MonoBehaviour {

		private IList<SingleEntityDisplayController> entityDisplays = new List<SingleEntityDisplayController>();

		public SingleEntityDisplayController EntityDisplayPrefab;

		public void Start() {
			UiManager.Instance.SelectionManager.OnSelectionChanged += OnSelectionChanged; 
		}

		private SingleEntityDisplayController SpawnNewDisplayForEntity(EntityShip selectedEntity)
		{
			var displayObj = GameObject.Instantiate(EntityDisplayPrefab.gameObject, Vector3.zero, Quaternion.identity, transform);
			var displayComponent = displayObj.GetComponent<SingleEntityDisplayController>();
			displayComponent.Tracking = selectedEntity;
			return displayComponent;
		}

		private void OnSelectionChanged(IList<EntityShip> newSelection)
		{
			entityDisplays.Apply(display => {
				display.Tracking = null;
				GameObject.Destroy(display.gameObject);
			});
			entityDisplays = newSelection.Select(SpawnNewDisplayForEntity).ToList();
		}
	}
}