﻿using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.Game.Allegiance;
using UnityEngine;
using UnityEngine.UI;

namespace Irrelephant.SurgeProtector.UI.Hud.Targeting
{
    public class EntityTargetingBracket : TargetingBracket
    {
        private Entity trackedEntity;
        public Entity TrackedEntity
        {
            get
            {
                return trackedEntity;
            }

            set
            {
                if (trackedEntity != null) {
                    var oldShip = trackedEntity as EntityShip;
                    if (oldShip != null)
                    {
                        oldShip.OnAllegianceChanged -= UpdateColor;
                        oldShip.OnShipDestroyed -= OnTrackedShipDestroyed;
                    }
                }
                trackedEntity = value;
                var newShip = trackedEntity as EntityShip;
                if (newShip != null) {
                    newShip.OnAllegianceChanged += UpdateColor;
                    newShip.OnShipDestroyed += OnTrackedShipDestroyed;
                    UpdateColor(null, newShip.Faction);
                }
            }
        }

        public override void Start() {
            base.Start();         
        }

        public void UpdateColor(Faction prev, Faction next)
        {
            var bracketImage = GetComponent<Image>();
            bracketImage.color = Faction.GetColorFromFaction(next, BracketController.Tracking.Faction);
        }

        private void OnTrackedShipDestroyed() {
            BracketController.DespawnBracketFor(TrackedEntity);
        }

        public override Vector3 GetTrackingPosition()
        {
            return UiManager.Instance.CameraManager.ControlledCamera.WorldToScreenPoint(TrackedEntity.transform.position);
        }
    }
}