﻿using UnityEngine;

namespace Irrelephant.SurgeProtector.UI.Hud.Targeting
{
    public abstract class TargetingBracket : MonoBehaviour
    {
        protected TargetingBracketController BracketController;

        private RectTransform rectTransform;

        private Vector3 screenSize;

        private Vector3 scalingVector;

        public void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            BracketController = GetComponentInParent<TargetingBracketController>();
        }

        public virtual void Start()
        {
            screenSize = new Vector3(Screen.width, Screen.height, 0f);
            scalingVector = new Vector3(1f / (Screen.width / 2f - BracketController.Padding.x),
                1f / (Screen.height / 2f - BracketController.Padding.y));
        }

        public void LateUpdate()
        {
            bool outOfBounds;
            var markerScreenPos = ProjectOntoScreenBounds(out outOfBounds);
            rectTransform.anchoredPosition = markerScreenPos;
        }

        public Vector3 ProjectOntoScreenBounds(out bool outOfBounds)
        {
            var screenPosition = GetTrackingPosition();

            var relativePosition = screenPosition - screenSize / 2f;
            var scaledPosition = Vector3.Scale(relativePosition, scalingVector);
            Vector3 markerPosition;

            if (Mathf.Abs(scaledPosition.x) < 1f && Mathf.Abs(scaledPosition.y) < 1f)
            {
                outOfBounds = false;
                return screenPosition;
            }

            if (Mathf.Abs(scaledPosition.x) > Mathf.Abs(scaledPosition.y))
            {
                markerPosition = relativePosition / Mathf.Abs(scaledPosition.x);
            }
            else
            {
                markerPosition = relativePosition / Mathf.Abs(scaledPosition.y);
            }

            outOfBounds = true;
            return markerPosition + screenSize / 2;
        }

        public abstract Vector3 GetTrackingPosition();
    }
}