﻿using System.Collections.Generic;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.Game.Allegiance;
using Irrelephant.SurgeProtector.Game.Instancing;
using UnityEngine;
using UnityEngine.UI;
using Irrelephant.SurgeProtector.Game;

namespace Irrelephant.SurgeProtector.UI.Hud.Targeting
{
    public class TargetingBracketController : MonoBehaviour
    {
        public Vector2 Padding;

        public GameObject TargetingBracketPrefab;
        public GameObject InstanceBracketPrefab;

        private ISensoryDataProvider currentSensor;
        private EntityShip tracking;
        public EntityShip Tracking
        {
            get { return tracking; }
            set
            {
                UnbindEvents(currentSensor);
                if (tracking != null) {
                    tracking.OnSensorChanged -= OnSensorUpdated;
                }
                if (value != null) {
                    currentSensor = value.CurrentSensor;
                    value.OnSensorChanged += OnSensorUpdated;
                }
                else
                {
                    currentSensor = null;
                }
                tracking = value;
                BindEvents(currentSensor);
                RebuildList();
            }
        }

        private void BindEvents(ISensoryDataProvider sensor)
        {
            if (sensor == null) return;
            sensor.OnEntityDetected += SpawnBracketFor;
            sensor.OnEntityLost += DespawnBracketFor;
        }

        private void UnbindEvents(ISensoryDataProvider sensor)
        {
            if (sensor == null) return;
            sensor.OnEntityDetected -= SpawnBracketFor;
            sensor.OnEntityLost -= DespawnBracketFor;
        }

        private void OnSensorUpdated(ISensoryDataProvider oldSensor, ISensoryDataProvider newSensor)
        {
            UnbindEvents(oldSensor);
            BindEvents(newSensor);
            currentSensor = newSensor;
            RebuildList();
        }

        private readonly IDictionary<int, TargetingBracket> bracketCache = new Dictionary<int, TargetingBracket>();

        public void RebuildList()
        {
            ClearChildren();
            foreach (var entity in currentSensor.AllVisibleEntities)
            {
                SpawnBracketFor(entity);
            }
        }

        public void SpawnBracketFor(Entity entity)
        {
            var bracket = Instantiate(TargetingBracketPrefab, transform)
                .GetComponent<EntityTargetingBracket>();

            bracket.TrackedEntity = entity;
            bracketCache[entity.Id] = bracket;
        }

        public void SpawnBracketFor(Instance instance)
        {
            var bracket = Instantiate(InstanceBracketPrefab, transform)
                .GetComponent<InstanceTargetingBracket>();
            bracket.TrackedInstance = instance;
            bracketCache[instance.InstanceId] = bracket;
        }

        public void DespawnBracketFor(Entity entity)
        {
            TargetingBracket bracket;
            if (bracketCache.TryGetValue(entity.Id, out bracket))
            {
                bracketCache.Remove(entity.Id);
                Destroy(bracket.gameObject);
            }
        }

        public void DespawnBracketFor(Instance instance)
        {
            TargetingBracket bracket;
            if (bracketCache.TryGetValue(instance.InstanceId, out bracket))
            {
                bracketCache.Remove(instance.InstanceId);
                Destroy(bracket.gameObject);
            }
        }

        private void ClearChildren()
        {
            foreach (Transform children in transform)
            {
                Destroy(children.gameObject);
                bracketCache.Clear();
            }
        }
    }
}