﻿using Irrelephant.SurgeProtector.Game;
using Irrelephant.SurgeProtector.Game.Instancing;
using UnityEngine;

namespace Irrelephant.SurgeProtector.UI.Hud.Targeting
{
    public class InstanceTargetingBracket : TargetingBracket
    {
        public Instance TrackedInstance;

        public override Vector3 GetTrackingPosition()
        {
            var controlledCam = UiManager.Instance.CameraManager.ControlledCamera;
            return controlledCam.WorldToScreenPoint(TrackedInstance.Position - GameManager.Instance.CurrentInstance.Position);
        }
    }
}