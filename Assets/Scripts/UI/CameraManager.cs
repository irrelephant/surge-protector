﻿using System.Runtime.InteropServices.ComTypes;
using Irrelephant.SurgeProtector.Game;
using Irrelephant.SurgeProtector.Game.AI.Utils;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.UI.Hud;
using Irrelephant.SurgeProtector.Utils;
using UnityEngine;

namespace Irrelephant.SurgeProtector.UI {
    public class CameraManager : MonoBehaviour
    {
        /// <summary>
        /// Camera obejct that is rendering the game view
        /// </summary>
        public Camera ControlledCamera;

        /// <summary>
        /// Max. ortho camera size 
        /// </summary>
        public float MaxZoom;

        /// <summary>
        /// Min. ortho camera size
        /// </summary>
        public float MinZoom;

        /// <summary>
        /// Speed at which the camera does the zooming (ortho size units per scrollwheel 'click')
        /// </summary>
        public float ZoomSpeed;

        /// <summary>
        /// Min speed for a single axis (on min zoom, single axis means actual max velocity of the camera is 2^1/2*MinCameraSpeed)
        /// </summary>
        public float MinCameraSpeed;

        /// <summary>
        /// Min speed for a single axis (on max zoom, single axis means actual max velocity of the camera is 2^1/2*MinCameraSpeed)
        /// </summary>
        public float MaxCameraSpeed;

        /// <summary>
        /// Pixel padding of the screen in which mouse starts panning the camera
        /// </summary>
        public Vector2 CameraMovementBoundaries;

        /// <summary>
        /// Box selection graphic
        /// </summary>
        public CanvasGroup SelectionBox;

        /// <summary>
        /// Camera position to lerp towards
        /// </summary>
        private Vector3 desiredCameraPosition;

        /// <summary>
        /// Current camera velocity
        /// </summary>
        private Vector3 cameraVelocity;

        /// <summary>
        /// Camera zoom to lerp towards
        /// </summary>
        private float desiredCameraZoom = 10f;

        /// <summary>
        /// Lerp factor of all camera lerp operations (movement, zoom).
        /// The greated this value is, the less smooth and more snappy the movement is going to be
        /// </summary>
        private const float cameraMovementLerpTime = 0.2f;

        /// <summary>
        /// Initializes the camera manager
        /// </summary>
        public void Awake() {
            desiredCameraPosition = ControlledCamera.transform.position;
        }

        /// <summary>
        /// Actually moves the camera
        /// </summary>
        public void LateUpdate() {
            ControlledCamera.transform.position = Vector3.Lerp(ControlledCamera.transform.position, desiredCameraPosition, cameraMovementLerpTime);
        }

        /// <summary>
        /// Reads all the inputs and does the input handling
        /// </summary>
        public void Update()
        {
            if (UiManager.Instance.InputManager.DoMousePanning)
            {
                UpdateCameraMovement();
            }

            UpdateCameraZoom();
        }

        /// <summary>
        /// Moves the camera in the desired direction
        /// </summary>
        /// <param name="direction">Normalized direction</param>
        public void PanCamera(Vector3 direction) {
            var actualSpeed = Mathf.Lerp(MinCameraSpeed, MaxCameraSpeed, desiredCameraZoom / MaxZoom);
            cameraVelocity = Vector3.Lerp(cameraVelocity, direction * actualSpeed, cameraMovementLerpTime);
            desiredCameraPosition += cameraVelocity;
        }

        private float GetAxisMovement(float position, float boundary, float screenDimension)
        {
            if (position <= boundary) return -1;
            if (position >= screenDimension - boundary) return +1;
            return 0;
        }

        private Vector3 GetCameraMovementFromMousePosition(Vector3 mousePosition)
        {
            return new Vector3(
                GetAxisMovement(mousePosition.x, CameraMovementBoundaries.x, Screen.width),
                GetAxisMovement(mousePosition.y, CameraMovementBoundaries.y, Screen.height)
            );
        }

        private void UpdateCameraMovement()
        {
            var cameraMovement = GetCameraMovementFromMousePosition(Input.mousePosition);
            PanCamera(cameraMovement);
        }

        private void AdjustCameraPositionForZoom(float newOrtho, float sign)
        {
            var mousePositionInWorld = ControlledCamera.ScreenToWorldPoint(Input.mousePosition);
            var deltaMovement = (mousePositionInWorld - ControlledCamera.transform.position) / newOrtho * ZoomSpeed / 2f;
            desiredCameraPosition += deltaMovement * sign;
        }

        private void UpdateCameraZoom()
        {
            var mouseScroll = -Input.mouseScrollDelta.y;
            desiredCameraZoom = Mathf.Clamp(desiredCameraZoom + mouseScroll * ZoomSpeed, MinZoom, MaxZoom);
            var oldOrthoSize = ControlledCamera.orthographicSize;
            if (mouseScroll != 0 && !(desiredCameraZoom - MinZoom < Mathf.Epsilon) && !(MaxZoom - desiredCameraZoom < Mathf.Epsilon))
            {
                AdjustCameraPositionForZoom(desiredCameraZoom, -Mathf.Sign(mouseScroll));
            }
            ControlledCamera.orthographicSize = Mathf.Lerp(oldOrthoSize, desiredCameraZoom, cameraMovementLerpTime);
        }

    }
}