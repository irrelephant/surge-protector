﻿using System;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.UI;
using Irrelephant.SurgeProtector.UI.Hud;
using Irrelephant.SurgeProtector.UI.Hud.Targeting;
using Irrelephant.SurgeProtector.UI.Hud.Chat;
using UnityEngine;

public class UiManager : MonoBehaviour
{
    public TargetingBracketController TargetingBrackets;
    public BlackoutOverlayController BlackoutOverlay;
    public ChatController ChatController;
    public ObjectiveDisplayController ObjectiveDisplayControler;
    public CameraManager CameraManager;
    public SelectionManager SelectionManager;
    public BoxSelector BoxSelector;
    public InputManager InputManager;

    public static UiManager Instance;

    private EntityShip controlledShip;

    /// <summary>
    /// Updates the controlled entity
    /// </summary>
    public EntityShip Controlled
    {
        get
        {
            return controlledShip;
        }
        set
        {
            controlledShip = value;
            TargetingBrackets.Tracking = value;
        }
    }

    public void Awake()
    {
        Instance = this;
        SelectionManager = new SelectionManager();
    }
}