﻿public static class UnitUtils
{
    public const float AuToKm = 149598000f;
    public const float KmToMThreshold = 5f;
    public const float KmHighToKmIntThreshold = 10f;
    public const float KmToMmThreshold = 500f;
    public const float MmToAuThreshold = 14959800f;

    public static string FormatSpeed(float units)
    {
        return FormatDistance(units) + "/s";
    }

    public static string FormatDistance(float unityUnits)
    {
        var km = unityUnits / 10f;

        if (km < KmToMThreshold)
        {
            return (km * 1000f).ToString("####") + " m";
        }

        if (km < KmHighToKmIntThreshold)
        {
            return km.ToString("##.#") + " km";
        }

        if (km < KmToMmThreshold)
        {
            return km.ToString("####") + " km";
        }

        if (km < MmToAuThreshold)
        {
            return (km / 1000f).ToString("##.#") + " Mm";
        }

        return (km / AuToKm).ToString("##.#") + " AU";
    }

    public static float ToAU(this float unityUnits)
    {
        return unityUnits * AuToKm;
    }
}