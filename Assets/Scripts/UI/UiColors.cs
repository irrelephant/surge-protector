﻿using UnityEngine;

namespace Irrelephant.SurgeProtector.UI {
	public static class UiColors {
        public static Color ColorAlliedMarker = new Color(0f, 0.5f, 1f);
        public static Color ColorFriendlyMarker = new Color(0f, 0.5f, 1f);
        public static Color ColorNeutralHostileMarker = new Color(1f, 0.5f, 0f);
        public static Color ColorHostileMarker = new Color(0.7f, 0f, 0f);
        public static Color ColorNeutralMarker = Color.white;
        public static Color ColorOwnMarker = new Color(0.1f, 1f, 0f);


	}
}