﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class RespawnTimerController : MonoBehaviour
{
    private float remaining;
    private CanvasGroup canvasGroup;
    private Text text;

    private bool faded;
    private bool fadeEnabled;

    public void StartCountdown(float seconds)
    {
        canvasGroup.alpha = 1;
        remaining = seconds;
    }

    public void StartCountdownWithFadeOutIn(float seconds)
    {
        StartCountdown(seconds);
        fadeEnabled = true;
        faded = false;
    }

    public void Update()
    {
        remaining -= Time.deltaTime;
        text.text = remaining < 1f ? "Respawning..." : String.Format("Respawning in... {0}", Mathf.CeilToInt(remaining));
        if (remaining < 1f && !faded && fadeEnabled)
        {
            UiManager.Instance.BlackoutOverlay.EnableOverlayOver(0.5f);
            faded = true;
        }
        if (remaining <= 0)
        {
            if (fadeEnabled && faded)
            {
                UiManager.Instance.BlackoutOverlay.DisableOverlayOver(0.5f);
                fadeEnabled = false;
                faded = false;
            }
            canvasGroup.alpha = 0;
        }
    }

    public void Start()
    {
        text = GetComponent<Text>();
        canvasGroup = GetComponent<CanvasGroup>();
        canvasGroup.alpha = 0;
    }
}