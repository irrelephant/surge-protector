﻿using Irrelephant.SurgeProtector.Game;
using Irrelephant.SurgeProtector.Game.AI.Utils;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.UI.Hud;
using Irrelephant.SurgeProtector.UI.Hud.Chat;
using Irrelephant.SurgeProtector.Utils;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public KeyCode PanCameraRightKey = KeyCode.D;
    public KeyCode PanCameraLeftKey = KeyCode.A;
    public KeyCode PanCameraUpKey = KeyCode.W;
    public KeyCode PanCameraDownKey = KeyCode.S;
    public KeyCode AppendOrToggleSelectionKey = KeyCode.LeftShift;
    public KeyCode QueueOrdersKey = KeyCode.LeftShift;

    public bool DoMousePanning;

    private void ControlKeyboardCameraPanning() {
        if (Input.GetKey(PanCameraUpKey)) {
            UiManager.Instance.CameraManager.PanCamera(Vector3.up);
        }

        if (Input.GetKey(PanCameraDownKey)) {
            UiManager.Instance.CameraManager.PanCamera(Vector3.down);
        }

        if (Input.GetKey(PanCameraRightKey)) {
            UiManager.Instance.CameraManager.PanCamera(Vector3.right);
        }

        if (Input.GetKey(PanCameraLeftKey)) {
            UiManager.Instance.CameraManager.PanCamera(Vector3.left);
        }
    }

    // <summary>
    /// Detects the box selection inputs and invokes the appropatiate calls on the BoxSelector.
    /// </summary>
    private void ProcessBoxSelection()
    {
        var boxSelector = UiManager.Instance.BoxSelector;
        if (Input.GetMouseButtonDown(0) && !ScreenSpaceHoverCanvas.OverUi) {
            UiManager.Instance.BoxSelector.StartBoxSelection();
        } else if (Input.GetMouseButtonUp(0)) {
            var clearSelection = !Input.GetKey(AppendOrToggleSelectionKey);
            if (boxSelector.BoxSelectionInProgress)
            {
                UiManager.Instance.BoxSelector.EndBoxSelection(clearSelection);
            }
            else
            {
                AttemptToSelectSingleUnit(clearSelection);
            }            				
        }

        if (Input.GetMouseButton(0)) {
            UiManager.Instance.BoxSelector.ProcessBoxSelection();
        }
    }

    private void AttemptToSelectSingleUnit(bool clearCurrentSelection) {
        var controlledCamera = UiManager.Instance.CameraManager.ControlledCamera;
        var worldPos = controlledCamera.ScreenToWorldPoint(Input.mousePosition);
        var clickedEntity = TargetingUtility.GetEntityAtPoint(worldPos, 1f);
        if (clickedEntity == null && clearCurrentSelection)
        {
            UiManager.Instance.SelectionManager.ClearSelection();
            return;
        }

        if (clickedEntity == null)
        {
            return;
        }

        if (clearCurrentSelection) 
        {
            UiManager.Instance.SelectionManager.SelectSingleUnit(clickedEntity);
        }
        else
        {
            UiManager.Instance.SelectionManager.ToggleSelectionForSingleUnit(clickedEntity);
        }
    }

    private void PerformRightClickAction() {
        if (!UiManager.Instance.SelectionManager.CanControlCurrentSelection())
        {
            UiManager.Instance.ChatController.AppendMessage("You have no control over these ships.".Color(Color.yellow));
            return;
        }

        var queueOrders = Input.GetKey(QueueOrdersKey);
        var controlledCamera = UiManager.Instance.CameraManager.ControlledCamera;
        var worldPos = controlledCamera.ScreenToWorldPoint(Input.mousePosition);
        var clickedEntity = TargetingUtility.GetEntityAtPoint(worldPos, 1f);

        foreach (var selected in UiManager.Instance.SelectionManager.SelectedEntities)
        {
            var contextOrder = clickedEntity == null
                ? ContextActionUtility.GetContextActionForPoint(selected, worldPos)
                : ContextActionUtility.GetContextActionForEntity(selected, clickedEntity);
            
            if (contextOrder != null) {
                if (queueOrders)
                {
                    selected.OrderManager.EnqueOrder(contextOrder);
                }
                else
                {
                    selected.OrderManager.HaltAndGiveOrder(contextOrder);
                }
            }
        }
    }

    private void HandleMouseButtons() {
        if (Input.GetMouseButtonDown(1)) {
            PerformRightClickAction();
        }
        
        ProcessBoxSelection();
    }

    public void Update()
    {
        ControlKeyboardCameraPanning();
        HandleMouseButtons();     
    }
}
