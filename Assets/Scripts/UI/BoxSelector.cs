using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.Utils;

namespace Irrelephant.SurgeProtector.UI {

	public class BoxSelector : MonoBehaviour {

		/// <summary>
		/// Determines whether box selction process in in progress.
		/// </summary>
		/// <value><c>true</c> if box selection in progress; otherwise, <c>false</c>.</value>
		public bool BoxSelectionInProgress { get; private set; }
		
		/// <summary>
		/// Box selection rectangle. Any unit owned by current player that is caught in this
		/// box while selecting is going to end up selected.
		/// </summary>
		/// <value>The box selection rectangle.</value>
		private Rect selectionRectangle;
		public Rect BoxSelectionRectangle {
			get { return selectionRectangle; }
		}
		
		/// <summary>
		/// Selection texture
		/// </summary>
		public Texture selectionHighlight;
		
		/// <summary>
		/// The selection origin - screen point to begin box selection at.
		/// </summary>
		private Vector2 selectionOrigin;
		
		/// <summary>
		/// Controls the minimal rectangle diagonal to start box selection.
		/// </summary>
		public float boxSelectionThreshold;

		/// <summary>
		/// Sets the box selection origin
		/// </summary>
		public void StartBoxSelection ()
		{
			selectionOrigin = Input.mousePosition;
		}

		/// <summary>
		/// Completes the box selection and sends the selection box message to visible selectable units.
		/// </summary>
		/// <param name="clearCurrentSelection">If set to <c>true</c> clear current selection.</param>
		public void EndBoxSelection(bool clearCurrentSelection) {
			BoxSelectionInProgress = false;
			FixRectangleInversion();
			var targetEntities = TargetingUtility.GetEntitiesInBox(selectionRectangle);
			if (clearCurrentSelection) 
			{
				UiManager.Instance.SelectionManager.SelectMultipleUnits(targetEntities);
			}
			else
			{
				UiManager.Instance.SelectionManager.AppendSelectionForMultipleUnits(targetEntities);
			}
		}

		/// <summary>
		/// Handles box selection.
		/// </summary>
		public void ProcessBoxSelection() {
			if (!BoxSelectionInProgress && (selectionOrigin - new Vector2(Input.mousePosition.x, Input.mousePosition.y)).magnitude > boxSelectionThreshold) {
				BoxSelectionInProgress = true;
			}
			

			selectionRectangle = new Rect(selectionOrigin.x,
			                              WorldSpaceUtility.WorldToScreenSpaceY(selectionOrigin.y),
			                              Input.mousePosition.x - selectionOrigin.x,
			                              WorldSpaceUtility.WorldToScreenSpaceY(Input.mousePosition.y) - WorldSpaceUtility.WorldToScreenSpaceY(selectionOrigin.y));
		}
		
		/// <summary>
		/// Draws the selection GUI if needed.
		/// </summary>
		void OnGUI() {
			if (BoxSelectionInProgress) {
				GUI.color = new Color(1f, 1f, 1f, 0.8f);
				GUI.DrawTexture(selectionRectangle, selectionHighlight);
			}
		}

		/// <summary>
		/// Fixes the selection rectangles which have either negative widths or negative heights
		/// </summary>
		private void FixRectangleInversion()
		{
			if (selectionRectangle.width < 0) {
				selectionRectangle.x += selectionRectangle.width;
				selectionRectangle.width = -selectionRectangle.width;
			}
			if (selectionRectangle.height < 0) {
				selectionRectangle.y += selectionRectangle.height;
				selectionRectangle.height = -selectionRectangle.height;
			}
		}
	}
}
