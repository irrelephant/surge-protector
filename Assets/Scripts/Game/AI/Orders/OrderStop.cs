﻿using System;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.Orders
{
    public class OrderStop : Order
    {
        public override bool IsMergeable
        {
            get { return true; }
        }

        public override bool AttemptMerge(Order order)
        {
            return true;
        }

        public override OrderSlot OrderSlot
        {
            get { return OrderSlot.Movement; }
        }

        public bool StopMovement()
        {
            var deltaSpeed = Executor.Propulsion.TopSpeed
                * Executor.Propulsion.Acceleration
                * MovementConstants.DecelerationBoostFactor 
                * Time.deltaTime;
            
            if (Executor.Velocity.magnitude < deltaSpeed) {
                Executor.Velocity = Vector3.zero;
                return true;
            }
            
            Executor.ApplyVelocity(-Executor.Velocity.normalized * deltaSpeed);
            return false;
        }

        public bool StopRotation()
        {
            Executor.ApplyAngularVelocity(0f);
            if (Executor.AngularVelocity < Executor.Propulsion.MaxAngularVelocity * MovementConstants.AngularVelocityLerpFactor)
            {
                Executor.AngularVelocity = 0f;
                return true;
            }

            return false;
        }

        public override bool ExecuteOrderUpdateTick()
        {
            var rotationStopped = StopRotation();
            var movementStopped = StopMovement();
            return rotationStopped && movementStopped;
        }
    }
}