﻿using Irrelephant.SurgeProtector.Game.AI.Movement;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.Orders
{
    public class OrderAlignToPoint : Order
    {
        public Vector3 TargetPoint;

        private float targetRotation;

        private EntityRotator rotator;

        public OrderAlignToPoint(Vector3 targetPoint)
        {
            TargetPoint = targetPoint;
        }

        public override OrderSlot OrderSlot
        {
            get { return OrderSlot.Movement; }
        }

        public override void WhenOrderStarted()
        {
            targetRotation = Quaternion
                .FromToRotation(Vector3.up, TargetPoint - Executor.transform.position)
                .eulerAngles.z;
        }

        public override bool ExecuteOrderUpdateTick()
        {
            rotator.RotateTowards(targetRotation);   
            return rotator.IsDone(targetRotation);
        }
    }
}