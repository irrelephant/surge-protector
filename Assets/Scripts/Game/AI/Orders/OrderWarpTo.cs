﻿using System;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.Game.Enum;
using Irrelephant.SurgeProtector.Game.Instancing;
using Irrelephant.SurgeProtector.UI;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.Orders
{
    public class OrderWarpTo : Order
    {
        public Instance WarpTarget;

        public Vector3 WarpInPoint;
        
        private EntityShip executor;

        public override OrderSlot OrderSlot
        {
            get { return OrderSlot.Warp | OrderSlot.Movement; }
        }

        public override EntityShip Executor
        {
            get { return executor; }
            set
            {
                executor = value;
            }
        }

        public OrderWarpTo(Instance target, Vector3 warpInPoint)
        {
            WarpTarget = target;
            WarpInPoint = warpInPoint;
        }

        public OrderWarpTo(Instance target) : this(target, target.DefaultWarpin)
        {
        }

        public override bool ExecuteOrderUpdateTick()
        {
            return true;
        }
    }
}