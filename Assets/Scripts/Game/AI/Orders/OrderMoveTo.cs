﻿using System.Linq;
using Irrelephant.SurgeProtector.Extensions;
using Irrelephant.SurgeProtector.Game.AI.Movement;
using Irrelephant.SurgeProtector.Tooling.Debuggery;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.Orders
{
    public class OrderMoveTo : Order
    {
        public Vector2 Target;

        public override bool IsMergeable {
            get 
            {
                return true;
            }
        }

        public override OrderSlot OrderSlot
        {
            get { return OrderSlot.Movement; }
        }

        private EntityPathFollower pathFollower;

        public OrderMoveTo(Vector2 target)
        {
            Target = target;
        }

        public override void WhenOrderStarted()
        {
            base.WhenOrderStarted();
            var path = GameManager.Instance.CurrentInstance.InstancePathdinder
                .GetPath(Executor.transform.position, Target);
            path.ApplyToPairs((curr, next) => Debuggery.Line(curr, next, Color.magenta));

            pathFollower = new EntityPathFollower(Executor, path);
        }

        public override bool ExecuteOrderUpdateTick()
        {
            if (pathFollower.IsDone())
            {
                return true;
            }

            pathFollower.FollowPath();
            return false;
        }

        public override bool AttemptMerge(Order otherOrder)
        {
            var otherMoveToOrder = otherOrder as OrderMoveTo;
            if (otherMoveToOrder == null) return false;
            
            var addedPath = GameManager.Instance.CurrentInstance.InstancePathdinder
                .GetPath(otherMoveToOrder.pathFollower.currentPath.LastOrDefault(), Target);

            addedPath.ApplyToPairs((curr, next) => Debuggery.Line(curr, next, Color.magenta));

            foreach (var point in addedPath.Skip(1)) {
                otherMoveToOrder.pathFollower.currentPath.Add(point);
            }

            ConsoleLog.Instance.LogFormat("Succesfully merged in {0} waypoints: {1}", addedPath.Count - 1, string.Join(";", addedPath.Select(p => p.ToString()).ToArray()));

            return true;
        }

        public override void WhenOrderComplete()
        {
            base.WhenOrderComplete();
            Executor.OrderManager.EnqueOrder(new OrderStop());
        }

        public override void WhenOrderCancelled()
        {
            base.WhenOrderCancelled();
            Executor.OrderManager.EnqueOrder(new OrderStop());
        }

    }
}