﻿using System;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.Orders
{
    public class OrderWarpOut : Order
    {
        public override OrderSlot OrderSlot
        {
            get { return OrderSlot.Warp | OrderSlot.Movement; }
        }

        public override bool ExecuteOrderUpdateTick()
        {
            GameManager.Instance.CurrentInstance.LeaveInstance(Executor);
            GameObject.Destroy(Executor, 1);
            return true;
        }
    }
}