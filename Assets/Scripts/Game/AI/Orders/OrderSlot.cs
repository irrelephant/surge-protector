﻿using System;

namespace Irrelephant.SurgeProtector.Game.AI.Orders
{
    [Flags]
    public enum OrderSlot
    {
        Movement          = 0x01,
        Warp              = 0x02,
        TurretAttack      = 0x04,
        StarboardAttack   = 0x08,
        PrortsideAttack   = 0x10
    }
}