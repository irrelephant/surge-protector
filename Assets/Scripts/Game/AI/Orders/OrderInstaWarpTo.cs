﻿using System;
using Irrelephant.SurgeProtector.Game.Enum;
using Irrelephant.SurgeProtector.Game.Instancing;
using Irrelephant.SurgeProtector.UI;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.Orders
{
    public class OrderInstaWarpTo : Order
    {
        public Instance WarpTarget;
        public Vector3 WarpFromPoint;
        public Vector3 WarpInPoint;

        public OrderInstaWarpTo(Instance target, Vector3 warpFromPoint, Vector3 warpInPoint)
        {
            WarpTarget = target;
            WarpFromPoint = warpFromPoint;
            WarpInPoint = warpInPoint;
        }

        public override OrderSlot OrderSlot
        {
            get { return OrderSlot.Warp | OrderSlot.Movement; }
        }

        public OrderInstaWarpTo(Instance target) : this(target, default(Vector3), target.DefaultWarpin)
        {
        }


        public override void WhenOrderComplete()
        {
            base.WhenOrderComplete();
            Executor.OrderManager.EnqueOrder(new OrderStop());
        }

        public override void WhenOrderCancelled()
        {
            base.WhenOrderCancelled();
            Executor.OrderManager.EnqueOrder(new OrderStop());
        }
        
        public override bool ExecuteOrderUpdateTick()
        {
            Executor.CurrentState = ShipState.Sublight;
            WarpTarget.JoinInstance(Executor, WarpInPoint);
            Executor.AngularVelocity = 0;
            Executor.Velocity = 10f * Executor.Propulsion.TopSpeed * (WarpInPoint - WarpFromPoint).normalized;
            Executor.transform.rotation =
                Quaternion.Euler(0, 0, Vector3.Angle(Vector3.up, WarpInPoint - WarpFromPoint));
            return true;
        }
    }
}