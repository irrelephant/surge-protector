﻿using Irrelephant.SurgeProtector.Game.Entities;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.Orders.Behaviours
{
    public abstract class BehaviourOrder : Order
    {
        public override OrderSlot OrderSlot { get { return 0; } }

        public override EntityShip Executor { get; set; }

        private readonly int updateFrequency;

        private int leftBeforeUpdate;

        protected BehaviourOrder(int updateFrequency)
        {
            this.updateFrequency = updateFrequency;
            leftBeforeUpdate = Random.Range(0, updateFrequency);
        }

        public new void OnOrderQueued() { }

        public new void OnOrderStarted() { }

        public new void OnOrderCancelled() { }

        public new void OnOrderComplete() { }

        public override bool ExecuteOrderUpdateTick()
        {
            leftBeforeUpdate--;

            if (leftBeforeUpdate <= 0)
            {
                leftBeforeUpdate = updateFrequency;
                UpdateBehaviour();
            }

            return false;
        }

        public abstract void UpdateBehaviour();
    }
}