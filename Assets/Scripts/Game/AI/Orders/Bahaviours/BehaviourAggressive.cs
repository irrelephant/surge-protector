﻿using System.Linq;
using Irrelephant.SurgeProtector.Game.Allegiance;
using Irrelephant.SurgeProtector.Game.Entities;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.Orders.Behaviours
{
    public class BehaviourAgressive : BehaviourOrder
    {
        public BehaviourAgressive() : base(120)
        {
        }

        private EntityShip pickedTarget;

        private OrderTurretAttack turretAttack;

        public override void UpdateBehaviour()
        {
            if (pickedTarget == null || pickedTarget.IsDestroyed)
            {
                PickNewTarget();
                if (pickedTarget == null)
                {
                    ConsoleLog.Instance.LogFormat("ID {0} didn't find a target.", Executor.Id);
                }
                else
                {
                    ConsoleLog.Instance.LogFormat("ID {0} has picked ID {1} as target.", Executor.Id, pickedTarget.Id);
                }
            }

            if (pickedTarget != null && (turretAttack == null || turretAttack.IsCompleteOrCancelled()))
            {
                turretAttack = new OrderTurretAttack(pickedTarget);
                Executor.OrderManager.EnqueOrder(turretAttack);
            }
        }

        private void PickNewTarget()
        {
            pickedTarget = Executor.CurrentSensor.AllVisibleEntities
                .Where(e => e is EntityShip)
                .Cast<EntityShip>()
                .Where(e => !e.IsDestroyed && e.IsHostile(Executor))
                .OrderBy(e => Vector2.Distance(e.transform.position, Executor.transform.position))
                .FirstOrDefault();
        }
    }
}