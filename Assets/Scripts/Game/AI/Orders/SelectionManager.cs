﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Irrelephant.SurgeProtector.Game;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.Game.Allegiance;

namespace Irrelephant.SurgeProtector.UI {
	public class SelectionManager {
		private IList<EntityShip> selectedEntities = new List<EntityShip>();

		public IEnumerable<EntityShip> SelectedEntities {
			get {
				return selectedEntities;
			}
		}

		public event Action<IList<EntityShip>> OnSelectionChanged;

		public void SelectSingleUnit(EntityShip unit)
		{
			if (unit == null) {
				return;
			}

			selectedEntities.Clear();
			selectedEntities.Add(unit);
			NotifySelectionChanged();
		}

		public void SelectMultipleUnits(IList<EntityShip> units)
		{
			if (!units.Any()) {
				return;
			}

			var ownUnits = units.Where(AllegianceExtensions.IsPlayerFaction).ToList();
			if (ownUnits.Count == 0)
			{
				selectedEntities = units;
			}
			else
			{
				selectedEntities = ownUnits;	
			}

			NotifySelectionChanged();
		}

		public void AppendSelectionForMultipleUnits(IList<EntityShip> units) {
			if (!units.Any()) {
				return;
			}

			if (selectedEntities.Any((unit) => !unit.IsPlayerFaction()))
			{
				selectedEntities = selectedEntities.Union(units.Where((unit) => !unit.IsPlayerFaction())).ToList();
			}
			else
			{
				selectedEntities = selectedEntities.Union(units.Where(AllegianceExtensions.IsPlayerFaction)).ToList();
			}

			NotifySelectionChanged();
		}

		public void ToggleSelectionForSingleUnit(EntityShip singleUnit)
		{
			if (selectedEntities.Contains(singleUnit))
			{
				DeselectSingleUnit(singleUnit);
				return;
			}
			
			AttemptToSelectSingleUnit(singleUnit);
		}

		public void ClearSelection() {
			selectedEntities.Clear();
			NotifySelectionChanged();
		}

		private void NotifySelectionChanged() {
			if (OnSelectionChanged != null)
			{
				OnSelectionChanged(selectedEntities);
			}
		}

		public bool CanControlCurrentSelection() {
			return !selectedEntities.Any() || selectedEntities.Any((unit) => unit.IsPlayerFaction());
		}

		private void DeselectSingleUnit(EntityShip singleUnit)
		{
			selectedEntities.Remove(singleUnit);
			NotifySelectionChanged();
		}

		private void AttemptToSelectSingleUnit(EntityShip singleUnit)
		{
			var unitIsPlayerFaction = singleUnit.IsPlayerFaction();
			if (selectedEntities.Any((unit) => !unit.IsPlayerFaction()))
			{
				if (!unitIsPlayerFaction)
				{
					selectedEntities.Add(singleUnit);
					NotifySelectionChanged();
				}
			}
			else
			{
				if (unitIsPlayerFaction)
				{
					selectedEntities.Add(singleUnit);
					NotifySelectionChanged();
				}
			}
		}
	}
}