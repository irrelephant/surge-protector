﻿namespace Irrelephant.SurgeProtector.Game.AI.Orders
{ 
    public enum OrderStatus
    {
        Dunno,
        Enqueued,
        Started,
        Completed,
        Cancelled
    }
}