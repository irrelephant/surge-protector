﻿using System.Collections.Generic;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI
{
    public interface IPathfindable
    {
        /// <summary>
        /// Returnes a list of points (quadtree nodes) that connect the "from" point to "to" point.
        /// </summary>
        /// <param name="from">Point to build the path from</param>
        /// <param name="to">Point to build the path to</param>
        /// <returns>List of intermediate path points or null if such path doesn't exist.</returns>
        List<Vector2> GetPath(Vector2 from, Vector2 to);
    }
}