﻿using System;
using System.Linq;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.Game.Entities.Ship.Subsystems;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.Orders
{
    public class OrderTurretAttack : Order
    {
        public override OrderSlot OrderSlot
        {
            get { return OrderSlot.TurretAttack; }
        }

        private readonly EntityShip target;

        private SubsystemWeaponGroup[] turretGroups;

        public OrderTurretAttack(EntityShip target)
        {
            this.target = target;
        }

        public override void WhenOrderStarted()
        {
            base.WhenOrderStarted();
            turretGroups = Executor.WeaponGroups.Where(group => group.IsTurret).ToArray();
        }

        private bool IsTargetEligibleForAttack()
        {
            return !(target.IsDestroyed || target.IsDespawning);
        }

        public override bool ExecuteOrderUpdateTick()
        {
            if (IsTargetEligibleForAttack())
            {
                FireAtTarget();
                return false;
            }

            StopFiring();
            return true;
        }

        private void FireAtTarget()
        {
            for (int i = 0; i < turretGroups.Length; i++)
            {
                var turretGroup = turretGroups[i];
                turretGroup.UpdateLookPosition(AimAtTarget(turretGroup));
                turretGroup.IsFiring = true;
            }
        }

        private Vector3 AimAtTarget(SubsystemWeaponGroup turretGroup)
        {
            var targetPosition = target.transform.position;
            var distanceToTarget = targetPosition - Executor.transform.position;
            var projectileTravelTime = distanceToTarget.magnitude / turretGroup.GetWeapon().ShotSpeed;
            return targetPosition + (target.Velocity - Executor.Velocity) * projectileTravelTime;
        }

        private void StopFiring()
        {
            for (int i = 0; i < turretGroups.Length; i++)
            {
                turretGroups[i].IsFiring = false;
            }
        }
    }
}