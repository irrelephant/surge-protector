﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Irrelephant.SurgeProtector.Extensions;
using Irrelephant.SurgeProtector.Game.AI.Orders.Behaviours;
using Irrelephant.SurgeProtector.Game.Entities;

namespace Irrelephant.SurgeProtector.Game.AI.Orders
{
    public class OrderManager : MonoBehaviour
    {
        public EntityShip ParentEntityShip;

        private readonly List<Order> activeOrders = new List<Order>();
        private readonly List<Order> queuedOrders = new List<Order>();

        private readonly List<Order> ordersToSetActive = new List<Order>();
        private readonly List<Order> ordersToRemove = new List<Order>();

        public Order BehaviourOrder { get; private set; }

        private bool AttemptToMergeIn(Order order) {
            var mergeable = activeOrders.FirstOrDefault(active => active.GetType().IsAssignableFrom(order.GetType()));
            if (mergeable == null) {
                return false;
            }
            return order.AttemptMerge(mergeable);
        }

        public void EnqueOrder(Order order)
        {
            bool canEnqueueThis = (order.OrderSlot & ParentEntityShip.EligibleOrderSlots) != 0;
            if (canEnqueueThis)
            {
                order.Executor = ParentEntityShip;
                if (order.IsMergeable && AttemptToMergeIn(order)) {
                    return; 
                }

                order.WhenOrderQueued();
                if (activeOrders.Union(ordersToSetActive).Any(active => (active.OrderSlot & order.OrderSlot) != 0))
                {
                    queuedOrders.Add(order);
                }
                else
                {
                    order.WhenOrderStarted();
                    ordersToSetActive.Add(order);
                }
            }
        }

        public void Update()
        {
            ProcessOrderBuffering();

            ProcessAiBehaviour();

            foreach (var order in activeOrders)
            {
                if (order.ExecuteOrderUpdateTick())
                {
                    order.WhenOrderComplete();
                    ordersToRemove.Add(order);
                    CheckQueueForNext(order);
                }
            }
        }

        private void ProcessAiBehaviour()
        {
            if (BehaviourOrder != null)
            {
                BehaviourOrder.ExecuteOrderUpdateTick();
            }
        }

        private void ProcessOrderBuffering()
        {
            activeOrders.AddRange(ordersToSetActive);
            ordersToSetActive.Clear();
            ordersToRemove.Apply(activeOrders.Remove);
            ordersToRemove.Clear();
        }

        private void CheckQueueForNext(Order justCompleted)
        {
            var nextQueuedOfSameSlot =
                queuedOrders.FirstOrDefault(queuedOrder => (queuedOrder.OrderSlot & justCompleted.OrderSlot) != 0);
            if (nextQueuedOfSameSlot != null)
            {
                nextQueuedOfSameSlot.WhenOrderStarted();
                queuedOrders.Remove(nextQueuedOfSameSlot);
                ordersToSetActive.Add(nextQueuedOfSameSlot);
            }
        }

        private void CancelOrdersFromCollection(IList<Order> orderCollection, Order slotOrder) {
            var activeOrdersToClear = orderCollection.Where(order => (order.OrderSlot & slotOrder.OrderSlot) != 0).ToList();
            activeOrdersToClear.Apply(orderToClear => {
                orderCollection.Remove(orderToClear);
            });
            activeOrdersToClear.Apply(orderToClear => {
                orderToClear.WhenOrderCancelled();
            });
        }

        public void HaltOrdersOfSameSlot(Order order)
        {
            CancelOrdersFromCollection(activeOrders, order);
            CancelOrdersFromCollection(queuedOrders, order);
        }

        public void HaltAndGiveOrder(Order order)
        {
            HaltOrdersOfSameSlot(order);
            EnqueOrder(order);
        }

        public void SetBehaviourOrder(BehaviourOrder order)
        {
            order.Executor = ParentEntityShip;
            BehaviourOrder = order;
        }
    }
}