﻿using System;
using Irrelephant.SurgeProtector.Game.Entities;

namespace Irrelephant.SurgeProtector.Game.AI.Orders
{
    public abstract class Order
    {
        public abstract OrderSlot OrderSlot { get; }

        public virtual bool IsMergeable {
            get {
                return false;
            }
        }
        
        public OrderStatus CurrentStatus { get; private set; }

        public virtual EntityShip Executor { get; set; }

        public event Action OnOrderQueued;

        public event Action OnOrderStarted;

        public event Action OnOrderCancelled;

        public event Action OnOrderComplete;

        public virtual void WhenOrderQueued()
        {
            CurrentStatus = OrderStatus.Enqueued;
            if (OnOrderQueued != null) OnOrderQueued();
        }

        public virtual void WhenOrderStarted()
        {
            CurrentStatus = OrderStatus.Started;
            if (OnOrderStarted != null) OnOrderStarted();
        }

        public virtual void WhenOrderCancelled()
        {
            CurrentStatus = OrderStatus.Cancelled;
            if (OnOrderCancelled != null) OnOrderCancelled();
        }

        public virtual void WhenOrderComplete()
        {
            CurrentStatus = OrderStatus.Completed;
            if (OnOrderComplete != null) OnOrderComplete();
        }

        public bool IsCompleteOrCancelled()
        {
            return CurrentStatus == OrderStatus.Completed || CurrentStatus == OrderStatus.Cancelled;
        }

        public virtual bool AttemptMerge(Order order)
        {
            return order != null;
        }

        public abstract bool ExecuteOrderUpdateTick();
    }
}