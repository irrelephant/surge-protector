﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Irrelephant.SurgeProtector.Game.AI.DataStructures
{
    public class PriorityQueue<T> : IEnumerable<T> where T : IComparable<T>
    {
        private readonly List<T> items;
        
        public PriorityQueue()
        {
            items = new List<T>();
        }

        public PriorityQueue(IEnumerable<T> priorityItems)
        {
            AddRange(priorityItems);
        }

        public int Count
        {
            get { return items.Count; }
        }
        
        public IEnumerator<T> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            Insert(item);
        }

        public void AddRange(IEnumerable<T> itemsToAdd)
        {
            if (itemsToAdd == null)
            {
                return;
            }

            foreach (var item in itemsToAdd)
            {
                Insert(item);
            }
        }

        public void Clear()
        {
            items.Clear();
        }

        public int Clear(int startIndex)
        {
            int numberOfItems = items.Count - 1 - startIndex;
            items.RemoveRange(startIndex, numberOfItems);
            return numberOfItems;
        }

        public void Clear(int startIndex, int count)
        {
            items.RemoveRange(startIndex, count);
        }

        public int ClearWhere(Func<T, bool> predicateFunction)
        {
            return items.RemoveAll(predicateFunction.Invoke);
        }

        public T PopFront()
        {
            if (items.Count == 0)
            {
                throw new InvalidOperationException("No elements exist in the queue");
            }

            var item = items[0];
            items.RemoveAt(0);
            return item;
        }

        public T PopBack()
        {
            if (items.Count == 0)
            {
                throw new InvalidOperationException("No elements exist in the queue");
            }

            int tail = items.Count - 1;
            var item = items[tail];
            items.RemoveAt(tail);
            return item;
        }

        public T PeekFront()
        {
            if (items.Count == 0)
            {
                throw new InvalidOperationException("No elements exist in the queue");
            }

            return items[0];
        }

        public T PeekBack()
        {
            if (items.Count == 0)
            {
                throw new InvalidOperationException("No elements exist in the queue");
            }

            return items[items.Count - 1];
        }

        public IEnumerable<T> PopFront(int numberToPop)
        {
            if (numberToPop > items.Count)
            {
                throw new ArgumentException(@"The numberToPop exceeds the number 
                                              of elements in the queue", "numberToPop");
            }

            var poppedItems = new List<T>();
            while (poppedItems.Count < numberToPop)
            {
                poppedItems.Add(PopFront());
            }

            return poppedItems;
        }

        public IEnumerable<T> PopBack(int numberToPop)
        {
            if (numberToPop > items.Count)
            {
                throw new ArgumentException(@"The numberToPop exceeds the number 
                                              of elements in the queue", "numberToPop");
            }

            var poppedItems = new List<T>();
            while (poppedItems.Count < numberToPop)
            {
                poppedItems.Add(PopBack());
            }

            return poppedItems;
        }

        public bool IsEmpty()
        {
            return items.Count == 0;
        }
        
        private void Insert(T item)
        {
            if (items.Count == 0)
            {
                items.Add(item);
            }
            else
            {
                InsertDescending(item);
            }
        }
        
        private void InsertDescending(T item)
        {
            var tail = items[items.Count - 1];
            int comparedToTail = item.CompareTo(tail);

            if (comparedToTail >= 0)
            {
                items.Add(item);
            }
            else if (items.Count == 1)
            {
                items.Insert(0, item);
            }
            else
            {
                FindIndexAndInsertItemDescending(item);
            }
        }
        
        private void FindIndexAndInsertItemDescending(T item)
        {
            int lowerBoundIndex = 0;
            int upperBoundIndex = items.Count - 1;
            int currentMedianIndex = upperBoundIndex / 2;

            while (true)
            {
                int comparisonResult = item.CompareTo(items[currentMedianIndex]);
                switch (comparisonResult)
                {
                    case 1:
                        lowerBoundIndex = currentMedianIndex;
                        break;
                    case -1:
                        upperBoundIndex = currentMedianIndex;
                        break;
                    default:
                        FindIndexAndInsertItem(item, currentMedianIndex);
                        return;
                }

                if (AreEndConditionsMet(item, lowerBoundIndex, upperBoundIndex, ref currentMedianIndex))
                {
                    break;
                }
            }
        }

        private void FindIndexAndInsertItem(T item, int currentIndex)
        {
            int currentPosition = currentIndex;
            int condition = -1;
            bool isLastElement = false;
            while (item.CompareTo(items[currentPosition]) != condition)
            {
                currentPosition++;
                if (currentPosition < items.Count) // Make sure the index does not go out of range
                {
                    continue;
                }

                isLastElement = true;
                break;
            }

            if (isLastElement)
            {
                items.Add(item);
            }
            else
            {
                items.Insert(currentPosition, item);
            }
        }

        private bool AreEndConditionsMet(T item, int lowerBoundIndex,
            int upperBoundIndex, ref int currentMedianIndex)
        {
            if (upperBoundIndex == 0)
            {
                items.Insert(0, item);
                return true;
            }

            if (lowerBoundIndex == items.Count - 1)
            {
                items.Add(item);
                return true;
            }

            int newMedianIndex = (upperBoundIndex + lowerBoundIndex) / 2;
            if (currentMedianIndex == newMedianIndex)
            {
                items.Insert(currentMedianIndex + 1, item);
                return true;
            }

            currentMedianIndex = newMedianIndex;
            return false;
        }
    }
}
