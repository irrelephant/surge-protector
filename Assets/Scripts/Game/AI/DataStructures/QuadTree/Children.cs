﻿using System.Collections;
using System.Collections.Generic;

namespace Irrelephant.SurgeProtector.Game.AI.DataStructures.QuadTree
{
    public class Children : IEnumerable<Node>
    {
        public Children(Node parent)
        {
            Parent = parent;
            
            TopLeft = new Node(parent, NodePosition.TopLeft);
            TopRight = new Node(parent, NodePosition.TopRight);
            BottomLeft = new Node(parent, NodePosition.BottomLeft);
            BottomRight = new Node(parent, NodePosition.BottomRight);
        }

        public Node Parent { get; private set; }

        public Node TopLeft { get; private set; }

        public Node TopRight { get; private set; }

        public Node BottomLeft { get; private set; }

        public Node BottomRight { get; private set; }

        public List<Node> ToList()
        {
            return new List<Node>
            {
                TopLeft,
                TopRight,
                BottomLeft,
                BottomRight,
            };
        }

        public IEnumerator<Node> GetEnumerator()
        {
            return ToList().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}