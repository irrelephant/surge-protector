﻿namespace Irrelephant.SurgeProtector.Game.AI.DataStructures.QuadTree
{
    public enum NodeState
    {
        Unknown,
        Passable,
        Impassable
    }
}
