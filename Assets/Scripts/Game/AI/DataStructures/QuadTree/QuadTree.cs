﻿using System.Linq;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.DataStructures.QuadTree
{
    public class QuadTree
    {
        public QuadTree(Vector2 origin, Vector2 extents)
        {
            RootNode = new Node(origin, extents);
        }

        public Node RootNode { get; private set; }

        public Node FindNodeForPoint(Vector2 point)
        {
            var currentNode = RootNode;
            while (!currentNode.IsLeaf)
            {
                currentNode = currentNode.Children.FirstOrDefault(x => x.ContainsPoint(point)) 
                              ?? currentNode.Children.First();
            }

            return currentNode;
        }
    }
}
