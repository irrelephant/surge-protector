﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.DataStructures.QuadTree
{
    public static class QuadTreeHelper
    {
        public static IEnumerable<Node> GetAdjacentNodes(Node fromNode)
        {
            var estimatedNeighbors = CreateEstimatedNeighbors(fromNode);
            FindShallowNodes(estimatedNeighbors, fromNode);
            RemoveImpossibleNodes(estimatedNeighbors);
            return estimatedNeighbors.SelectMany(GetAllNeighbors).Distinct();
        }

        private static List<EstimatedNeighborInfo> CreateEstimatedNeighbors(Node fromNode)
        {
            return System.Enum.GetValues(typeof(NeighborDirection))
                .Cast<NeighborDirection>()
                .Select(x => new EstimatedNeighborInfo
                {
                    Direction = x,
                    EstimatedOrigin = fromNode.Origin
                                      + Vector2.Scale(new Vector2(fromNode.Extents.x, fromNode.Extents.y), ToNormalVector(x))
                }).ToList();
        }

        private static void FindShallowNodes(List<EstimatedNeighborInfo> estimatedNeighbors, Node fromNode)
        {
            var parentNode = fromNode.Parent;
            int relativeDepth = 0;
            while (parentNode != null && estimatedNeighbors.Select(x => x.Value).Any(x => x == null))
            {
                foreach (var node in estimatedNeighbors.Where(x => x.Value == null))
                {
                    var newNode = parentNode.Children.FirstOrDefault(x => x.ContainsPoint(node.EstimatedOrigin));
                    if (newNode != null)
                    {
                        node.Value = newNode;
                        node.RelativeDepth = relativeDepth;
                    }
                }

                parentNode = parentNode.Parent;
                relativeDepth++;
            }
        }

        private static void RemoveImpossibleNodes(List<EstimatedNeighborInfo> estimatedNeighbors)
        {
            estimatedNeighbors.RemoveAll(x => x.Value == null);
        }

        private static IEnumerable<Node> GetAllNeighbors(EstimatedNeighborInfo estimatedNeighborInfo)
        {
            return GetAllNeighbors(estimatedNeighborInfo.Value, estimatedNeighborInfo.RelativeDepth,
                estimatedNeighborInfo.EstimatedOrigin, estimatedNeighborInfo.Direction);
        }

        private static IEnumerable<Node> GetAllNeighbors(Node value, int depth,
            Vector2 estimatedOrigin, NeighborDirection neighborDirection)
        {
            if (value.IsLeaf)
            {
                return value.State == NodeState.Passable 
                    ? Enumerable.Repeat(value, 1) 
                    : Enumerable.Empty<Node>();
            }

            if (depth > 0)
            {
                return GetAllNeighbors(value.Children.Single(x => x.ContainsPoint(estimatedOrigin)),
                    depth - 1, estimatedOrigin, neighborDirection);
            }

            return GetNeighborsForDirection(value.Children, neighborDirection)
                .SelectMany(x => GetAllNeighbors(x, depth - 1, estimatedOrigin, neighborDirection));
        }

        private static IEnumerable<Node> GetNeighborsForDirection(Children children, NeighborDirection direction)
        {
            switch (direction)
            {
                case NeighborDirection.Left:
                    yield return children.TopRight;
                    yield return children.BottomRight;
                    break;
                case NeighborDirection.LeftTop:
                    yield return children.BottomRight;
                    break;
                case NeighborDirection.Top:
                    yield return children.BottomLeft;
                    yield return children.BottomRight;
                    break;
                case NeighborDirection.RightTop:
                    yield return children.BottomLeft;
                    break;
                case NeighborDirection.Rigth:
                    yield return children.BottomLeft;
                    yield return children.TopLeft;
                    break;
                case NeighborDirection.RightBot:
                    yield return children.TopLeft;
                    break;
                case NeighborDirection.Bot:
                    yield return children.TopRight;
                    yield return children.TopLeft;
                    break;
                case NeighborDirection.LeftBot:
                    yield return children.TopRight;
                    break;
            }
        }

        private static Vector2 ToNormalVector(NeighborDirection direction)
        {
            switch (direction)
            {
                case NeighborDirection.Left:
                    return new Vector2(-1, 0);
                case NeighborDirection.LeftTop:
                    return new Vector2(-1, -1);
                case NeighborDirection.Top:
                    return new Vector2(0, -1);
                case NeighborDirection.RightTop:
                    return new Vector2(+1, -1);
                case NeighborDirection.Rigth:
                    return new Vector2(+1, 0);
                case NeighborDirection.RightBot:
                    return new Vector2(+1, +1);
                case NeighborDirection.Bot:
                    return new Vector2(0, +1);
                case NeighborDirection.LeftBot:
                    return new Vector2(-1, +1);
                default:
                    throw new NotImplementedException();
            }
        }

        private enum NeighborDirection
        {
            Left,
            LeftTop,
            Top,
            RightTop,
            Rigth,
            RightBot,
            Bot,
            LeftBot
        }

        private class EstimatedNeighborInfo
        {
            public Vector2 EstimatedOrigin { get; set; }

            public NeighborDirection Direction { get; set; }

            public int RelativeDepth { get; set; }

            public Node Value { get; set; }
        }
    }
}
