﻿using System;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.DataStructures.QuadTree
{
    public class Node
    {
        public Node(Vector2 origin, Vector2 extents)
        {
            Origin = origin;
            Extents = extents;
        }

        public Node(Node parent, NodePosition position)
        {
            Parent = parent;
            Origin = CalculateOrigin(parent, position);
            Extents = parent.Extents / 2;
        }

        public NodeState State { get; set; }

        public Vector2 Origin { get; private set; }

        public Vector2 Extents { get; private set; }

        public Node Parent { get; private set; }
        
        public Children Children { get; private set; }

        public bool IsLeaf
        {
            get { return Children == null; }
        }

        public float Area
        {
            get { return Extents.x * Extents.y; }
        }

        public void SplitIntoChildNodes()
        {
            Children = new Children(this);
        }

        public bool ContainsPoint(Vector2 point)
        {
            float halfWidth = Extents.x / 2;
            float halfHeight = Extents.y / 2;

            return Origin.x - halfWidth < point.x && point.x < Origin.x + halfWidth
                && Origin.y - halfHeight < point.y && point.y < Origin.y + halfHeight;
        } 

        private static Vector2 CalculateOrigin(Node parent, NodePosition position)
        {
            float parentFourthWidth = parent.Extents.x / 4;
            float parentFourthHeight = parent.Extents.y / 4;
            switch (position)
            {
                case NodePosition.TopLeft:
                    return parent.Origin + new Vector2(-parentFourthWidth, -parentFourthHeight);
                case NodePosition.TopRight:
                    return parent.Origin + new Vector2(+parentFourthWidth, -parentFourthHeight);
                case NodePosition.BottomLeft:
                    return parent.Origin + new Vector2(-parentFourthWidth, +parentFourthHeight);
                case NodePosition.BottomRight:
                    return parent.Origin + new Vector2(+parentFourthWidth, +parentFourthHeight);
                default:
                    throw new NotImplementedException();
            }
        }
    }
}