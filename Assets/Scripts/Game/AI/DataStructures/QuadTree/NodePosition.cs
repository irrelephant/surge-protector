﻿namespace Irrelephant.SurgeProtector.Game.AI.DataStructures.QuadTree
{
    public enum NodePosition
    {
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight
    }
}
