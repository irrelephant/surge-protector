﻿using System.Collections;
using System.Collections.Generic;
using Irrelephant.SurgeProtector.Game.AI.Orders;
using Irrelephant.SurgeProtector.Game.Allegiance;
using Irrelephant.SurgeProtector.Game.Entities;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.Utils {
	public class ContextActionUtility {
		public static Order GetContextActionForEntity(EntityShip executor, EntityShip target) {
			if (executor.IsHostile(target)) {
				return new OrderTurretAttack(target);
			} 
			return new OrderTurretAttack(target);
		}

		public static Order GetContextActionForPoint(EntityShip executor, Vector3 point) {
			return new OrderMoveTo(point);
		}
	}
}