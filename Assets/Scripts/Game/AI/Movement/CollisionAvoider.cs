using System;
using System.Linq;
using Irrelephant.SurgeProtector.Game.Entities;
using  Irrelephant.SurgeProtector.Utils;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.Movement
{
    public class CollisionAvoider
    {
        private readonly EntityShip ship;
        private readonly RaycastHit2D[] intersectionResults = new RaycastHit2D[10];

        public CollisionAvoider(EntityShip ship)
        {
            this.ship = ship;
        }

        public Vector3? CheckForCollision()
        {
            var closesObstacle = GetClosestObstacle();
            if (closesObstacle.HasValue)
            {
                var distance = closesObstacle.Value.transform.position - ship.transform.position;
                float sign = Math.Sign(Vector2.Dot(ship.Velocity, distance));
                return distance.normalized.Rotate(sign * Mathf.Rad2Deg * Mathf.Tan(1.5f / distance.magnitude));
            }

            return null;
        }

        private RaycastHit2D? GetClosestObstacle()
        {
            var ahead = CalculateAhead();
            int obstaclesCount = Physics2D.LinecastNonAlloc(ship.transform.position, ship.transform.position + ahead, 
                intersectionResults, LayerConstants.MaskFor(LayerConstants.StaticGeometryLayer));
            if (obstaclesCount == 0)
            {
                return null;
            }

            return intersectionResults
                .Take(obstaclesCount)
                .Aggregate((a, b) => DistanceTo(a) < DistanceTo(b) ? a : b);
        }

        private Vector3 CalculateAhead()
        {
            return ship.SignatureRadius * ship.Velocity * 2f;
        }

        private float DistanceTo(RaycastHit2D raycastHit2D)
        {
            return (ship.transform.position - raycastHit2D.transform.position).magnitude;
        }
    }
}
