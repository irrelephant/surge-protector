﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.Game.Entities.Ship.Subsystems;

namespace Irrelephant.SurgeProtector.Game.AI.Movement
{
    public class EntityRotator
    {
        /// <summary>
        /// Allowed distance error determining looking-at.
        /// Maximum distance is 2 (Units are looking in opposite directions).
        /// </summary>
        private const float lookingDistance = 0.1f;

        /// <summary>
        /// Ship that does the navigation.
        /// </summary>
        private Entity navigator;

        /// <summary>
        /// Propulsion subsystem of the navigator
        /// </summary>
        private SubsystemPropulsion propulsion;

        public EntityRotator(EntityShip ship) {
            navigator = ship;
            propulsion = ship.Propulsion;
        }

        /// <summary>
        /// Determines whether unit is looking in certain direction with certain margin for error.
        /// </summary>
        /// <param name="direction">Direction to detect.</param>
        /// <param name="lookingDistance">Max vector distance between actual and desired directions</param>
        /// <returns></returns>
        public bool IsAlmostLookingAt(Vector3 direction, float lookingDistance)
        {
            return (navigator.transform.up - direction).magnitude < lookingDistance;
        }

        /// <summary>
        /// Determines whether unit is looking in certain direction with certain margin for error.
        /// </summary>
        /// <param name="direction">Direction to detect.</param>
        /// <returns></returns>
        public bool IsAlmostLookingAt(Vector3 direction)
        {
            return IsAlmostLookingAt(direction, lookingDistance);
        }

        /// <summary>
        /// Rotates the unit towards the certain direction.
        /// </summary>
        /// <param name="direction">Direction to rotate towards.</param>
        public void RotateTowards(Vector3 direction)
        {
            var targetAngle = Quaternion
                .FromToRotation(Vector3.up, direction)
                .eulerAngles.z;
            RotateTowards(targetAngle);
        }

        /// <summary>
        /// Rotates the unit towards the certain angle.
        /// </summary>
        /// <param name="angle">Angle to rotate towards.</param>
        public void RotateTowards(float angle) {
            var normalizedError = Mathf.DeltaAngle(navigator.transform.rotation.eulerAngles.z, angle);
            // Play with the values, I guess.
            // The higher this value is, the more "linear" the acceleration is going to feel
            var actualRotationPerSec = Mathf.Clamp(normalizedError * MovementConstants.RotationAcceleration, -propulsion.MaxAngularVelocity, propulsion.MaxAngularVelocity);
            navigator.ApplyAngularVelocity(actualRotationPerSec);
        }

        /// <summary>
        /// Shows if the ship is looking in the necessary direction and is stable
        /// </summary>
        public bool IsDone(Vector3 direction) {
            return IsDone(Quaternion
                .FromToRotation(Vector3.up, direction)
                .eulerAngles.z);
        }

        /// <summary>
        /// Shows if the ship is rotated to the necessary angle and is stable
        /// </summary>
        public bool IsDone(float angle) {
            return Mathf.DeltaAngle(navigator.transform.rotation.eulerAngles.z, angle) < Mathf.Epsilon
                && navigator.AngularVelocity < propulsion.MaxAngularVelocity * Time.deltaTime;
        }
    }
}