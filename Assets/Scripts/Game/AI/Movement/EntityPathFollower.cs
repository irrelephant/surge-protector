﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.Game.Entities.Ship.Subsystems;
using Irrelephant.SurgeProtector.Tooling.Debuggery;

namespace Irrelephant.SurgeProtector.Game.AI.Movement
{
    public class EntityPathFollower
    {

        /// <summary>
        /// Current path that this gameobject is travelling along.
        /// </summary>
        public IList<Vector2> currentPath;

        /// <summary>
        /// Current waypoint index to go for.
        /// </summary>
        private int currentWaypoint;

        /// <summary>
        /// Ship that does the navigation.
        /// </summary>
        private Entity navigator;

        /// <summary>
        /// Propulsion subsystem of the navigator
        /// </summary>
        private SubsystemPropulsion propulsion;

        /// <summary>
        /// Rotator that does the rotation towards target
        /// </summary>
        private EntityRotator rotator;

        /// <summary>
        /// Collision avoider that rotates the ship according to the incoming obstacles
        /// </summary>
        private CollisionAvoider collisionAvoider;

        public EntityPathFollower(EntityShip ship, IList<Vector2> path) {
            currentPath = path;
            navigator = ship;
            propulsion = ship.Propulsion;
            rotator = new EntityRotator(ship);
            collisionAvoider = new CollisionAvoider(ship);
        }

        /// <summary>
        /// Accelerates the unit forward.
        /// </summary>
        private void AccelerateTowardsWaypoint(Vector3 direction)
        {
            navigator.ApplyVelocity(direction * propulsion.Acceleration * propulsion.TopSpeed * Time.deltaTime);
        }

        /// <summary>
        /// Decelerates the unit.
        /// </summary>
        private void Decelerate()
        {
            /// Notice the hack - making the deceleration more effective then the acceleration. 
            var velocityToApply = -navigator.Velocity.normalized * MovementConstants.DecelerationBoostFactor * propulsion.Acceleration * propulsion.TopSpeed * Time.deltaTime;
            if (velocityToApply.sqrMagnitude > navigator.Velocity.sqrMagnitude)
            {
                navigator.ApplyVelocity(-navigator.Velocity);
            }
            else
            {
                navigator.ApplyVelocity(velocityToApply);
            }
        }

        private bool IsVelocityAlignedWithWaypoint(Vector3 waypointDirection)
        {
            var normVelocity = Vector3.Normalize(navigator.Velocity);
            var deltaVelocity = normVelocity - waypointDirection;
            return navigator.Velocity == Vector3.zero 
                ? true
                : deltaVelocity.magnitude < 0.1f;
        }


        /// <summary>
        /// Make a movement step.
        /// </summary>
        /// <param name="waypoint"></param>
        private void SeekCurrentWaypoint(Vector3 waypointDirection)
        {
            Vector3? possibleCollisionEscapeVector = collisionAvoider.CheckForCollision();           
            if (possibleCollisionEscapeVector == null 
                && rotator.IsAlmostLookingAt(waypointDirection, propulsion.ManeuverabilityFactor)
                && IsVelocityAlignedWithWaypoint(possibleCollisionEscapeVector ?? waypointDirection))
            {
                
                AccelerateTowardsWaypoint(waypointDirection);
            }
            else
            {
                Decelerate();
            }
            
            rotator.RotateTowards(waypointDirection);
        }

        /// <summary>
        /// Checks if unit has reached the given waypoint.
        /// </summary>
        /// <param name="waypoint"></param>
        /// <returns></returns>
        private bool IsReadyForNextWaypoint(Vector3 currentWaypoint, Vector3 waypointDirection)
        {
            if (currentWaypoint == navigator.transform.position) return true;

            if (navigator.Velocity == Vector3.zero || (waypointDirection - navigator.Velocity.normalized).magnitude < 0.1f)
            {
                var timeToWaypoint = (currentWaypoint - navigator.transform.position).magnitude / navigator.Velocity.magnitude;
                var canStopIn = (navigator.Velocity.magnitude / propulsion.TopSpeed)
                    / propulsion.Acceleration
                    / MovementConstants.DecelerationBoostFactor / MovementConstants.DecelerationBoostFactor;
                return timeToWaypoint <= canStopIn;
            }

            return false;
        }

        /// <summary>
        /// Applies necessary velocity changes to follow along the path
        /// </summary>
        public void FollowPath()
        {
            if (IsDone())
            {
                return;
            }

            if (currentWaypoint >= currentPath.Count)
            {
                Stop();
                return;
            }

            Vector3 currentWaypointPos = currentPath[currentWaypoint];
            var waypointDirection = (currentWaypointPos - navigator.transform.position).normalized;

            if (IsReadyForNextWaypoint(currentWaypointPos, waypointDirection))
            {
                Debuggery.Box(navigator.transform.position, 0.3f * Vector2.one, Color.red);
                currentWaypoint++;
            }
            else
            {
                SeekCurrentWaypoint(waypointDirection);
            }
        }

        /// <summary>
        /// Stops the unit movement
        /// </summary>
        public void Stop()
        {
            if (currentPath == null)
            {
                return;
            }

            currentPath = null;
        }

        /// <summary>
        /// Shows if the current follower is done with the path following
        /// </summary>
        public bool IsDone()
        {
            return currentPath == null;
        }
    }
}