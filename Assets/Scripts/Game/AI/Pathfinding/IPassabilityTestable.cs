﻿using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.Pathfinding
{
    public interface IPassabilityTestable
    {
        /// <summary>
        /// Checks whether or not the given AABB intersects an obstacle.
        /// </summary>
        /// <param name="origin">Center of the AABB to hittest</param>
        /// <param name="extents">x, y sizes of the AABB</param>
        /// <returns>true of the given AABB doesn't intersect an obstacle. false otherwise.</returns>
        bool TestBoxPassiblity(Vector2 origin, Vector2 extents);

        /// <summary>
        /// Checks whether or not the given line intersects an obstacle.
        /// </summary>
        /// <param name="start">Start point of the line</param>
        /// <param name="end">End point of the line</param>
        /// <returns>true of the given line doesn't intersect an obstacle. false otherwise.</returns>
        bool TestLinePassiblity(Vector2 start, Vector2 end);
    }
}