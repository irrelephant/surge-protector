﻿using System;
using System.Collections.Generic;
using System.Linq;
using Irrelephant.SurgeProtector.Game.AI.DataStructures;

namespace Irrelephant.SurgeProtector.Game.AI.Pathfinding
{
    public abstract class AStarPathfinder<T>
    {
        private readonly Dictionary<T, NodeInfo> nodeInfos = new Dictionary<T, NodeInfo>();
        
        protected abstract float GetTraversalCost(T firstNode, T secondNode);

        protected abstract List<T> GetAdjacentNodes(T fromNode);

        private NodeInfo GetOrCreateInfo(T node)
        {
            if (nodeInfos.ContainsKey(node))
            {
                return nodeInfos[node];
            }

            var nodeInfo = new NodeInfo(node);
            nodeInfos[node] = nodeInfo;
            return nodeInfo;
        }

        public List<T> FindPath(T start, T end)
        {
            nodeInfos.Clear();
            var startNode = GetOrCreateInfo(start);
            var endNode = GetOrCreateInfo(end);
            var path = new List<T>();

            bool success = Search(startNode, endNode);
            if (success)
            {
                var node = endNode;
                while (node.ParentInfo != null)
                {
                    path.Add(node.Value);
                    node = node.ParentInfo;
                }

                path.Add(node.Value);
                path.Reverse();
            }

            return path;
        }

        private bool Search(NodeInfo startNode, NodeInfo endNode)
        {
            startNode.State = NodeState.Closed;
            var openNodes = new PriorityQueue<NodeInfo>();
            openNodes.AddRange(GetSuccessors(startNode, endNode));

            while (openNodes.Any())
            {
                var nextNode = openNodes.PeekFront();
                if (Equals(nextNode.Value, endNode.Value))
                {
                    endNode.ParentInfo = nextNode.ParentInfo;
                    return true;
                }

                nextNode.State = NodeState.Closed;
                openNodes.PopFront();
                var nextOpenNodes = GetSuccessors(nextNode, endNode);
                openNodes.AddRange(nextOpenNodes);
            }

            return false;
        }

        private List<NodeInfo> GetSuccessors(NodeInfo fromNode, NodeInfo endNode)
        {
            var successors = new List<NodeInfo>();
            var adjacentNodes = GetAdjacentNodes(fromNode.Value).Select(GetOrCreateInfo);

            foreach (var adjacentNode in adjacentNodes.Where(x => x.State != NodeState.Closed))
            {
                if (adjacentNode.State == NodeState.Open)
                {
                    double gTemp = CalculateG(fromNode, adjacentNode);
                    if (gTemp < adjacentNode.G)
                    {
                        adjacentNode.ParentInfo = fromNode;
                        adjacentNode.G = CalculateG(adjacentNode.ParentInfo, adjacentNode);
                    }
                }
                else
                {
                    adjacentNode.ParentInfo = fromNode;
                    adjacentNode.H = CalculateH(adjacentNode, endNode);
                    adjacentNode.G = CalculateG(adjacentNode.ParentInfo, adjacentNode);
                    adjacentNode.State = NodeState.Open;
                    successors.Add(adjacentNode);
                }
            }

            return successors;
        }

        private float CalculateG(NodeInfo parentNode, NodeInfo currentNode)
        {
            return parentNode.G + GetTraversalCost(parentNode.Value, currentNode.Value);
        }

        private float CalculateH(NodeInfo currentNode, NodeInfo endNode)
        {
            return GetTraversalCost(currentNode.Value, endNode.Value);
        }
        
        private class NodeInfo : IComparable<NodeInfo>
        {
            public NodeInfo(T node)
            {
                Value = node;
            }

            public T Value { get; private set; }

            public NodeState State { get; set; }

            public NodeInfo ParentInfo { get; set; }

            public float F
            {
                get { return G + H; }
            }

            public float G { get; set; }

            public float H { get; set; }

            public int CompareTo(NodeInfo other)
            {
                return F.CompareTo(other.F);
            }
        }

        private enum NodeState
        {
            New,
            Open,
            Closed
        }
    }
}
