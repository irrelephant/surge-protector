﻿using System.Collections.Generic;
using System.Linq;
using Irrelephant.SurgeProtector.Game.AI.DataStructures.QuadTree;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.Pathfinding
{
    public class QuadTreePathFinder : AStarPathfinder<Node>
    {
        protected override float GetTraversalCost(Node firstNode, Node secondNode)
        {
            return Vector2.Distance(firstNode.Origin, secondNode.Origin);
        }

        protected override List<Node> GetAdjacentNodes(Node fromNode)
        {
            return QuadTreeHelper.GetAdjacentNodes(fromNode).ToList();
        }
    }
}
