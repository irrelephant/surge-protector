﻿using System.Collections.Generic;
using System.Linq;
using Irrelephant.SurgeProtector.Extensions;
using Irrelephant.SurgeProtector.Game.AI.DataStructures.QuadTree;
using Irrelephant.SurgeProtector.Tooling.Debuggery;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.Pathfinding
{
    public class Pathfinder : IPathfindable
    {
        private readonly QuadTreePathFinder quadTreePathFinder = new QuadTreePathFinder();

        private readonly QuadTree pathdindingData;
        private readonly IPassabilityTestable passabilityTestable;

        public Pathfinder(IPassabilityTestable passabilityTestable, Vector2 extents)
        {
            this.passabilityTestable = passabilityTestable;
            var boxExtents = new Vector2(Mathf.Max(extents.x, extents.y), Mathf.Max(extents.x, extents.y));
            pathdindingData = new QuadTreeGenerator(passabilityTestable)
                .GenerateOnArea(Vector2.zero, boxExtents);

            //RenderDebuggeryQuadtreeRecursively(pathdindingData.RootNode);
        }

        private void RenderDebuggeryQuadtreeRecursively(Node node)
        {           
            if (node.IsLeaf)
            {
                Debuggery.Box(node.Origin, node.Extents, node.State == NodeState.Passable ? Color.green : Color.red);
            }
            else
            {
                node.Children.Apply(RenderDebuggeryQuadtreeRecursively);
            }
        }

        public List<Vector2> GetPath(Vector2 from, Vector2 to)
        {
            var startNode = pathdindingData.FindNodeForPoint(from);
            var endNode = pathdindingData.FindNodeForPoint(to);

            if (startNode == endNode) {
                return new List<Vector2> { from, to };
            }

            var path = quadTreePathFinder
                .FindPath(pathdindingData.FindNodeForPoint(from), pathdindingData.FindNodeForPoint(to))
                .Select(x => x.Origin)
                .ToList();

            if (path.Any())
            {
                path.Insert(0, from);
                path.Add(to);

                MakeStraightLines(path);
            }

            return path;
        }

        private void MakeStraightLines(List<Vector2> path)
        {
            int currentIndex = 0;
            while (currentIndex < path.Count - 2)
            {
                if (passabilityTestable.TestLinePassiblity(path[currentIndex], path[currentIndex + 2]))
                {
                    path.RemoveAt(currentIndex + 1);
                }
                else
                {
                    currentIndex++;
                }
            }
        }
    }
}
