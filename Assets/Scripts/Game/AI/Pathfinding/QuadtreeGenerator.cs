﻿using System.Collections.Generic;
using System.Linq;
using Irrelephant.SurgeProtector.Extensions;
using Irrelephant.SurgeProtector.Game.AI.DataStructures.QuadTree;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.AI.Pathfinding
{
    public class QuadTreeGenerator
    {
        private const float MinimumImpassableArea = 10f;

        private readonly IPassabilityTestable passabilityTestable;

        public QuadTreeGenerator(IPassabilityTestable passabilityTestable)
        {
            this.passabilityTestable = passabilityTestable;
        }

        public QuadTree GenerateOnArea(Vector2 areaOrigin, Vector2 areaExtents)
        {
            var quadTree = new QuadTree(areaOrigin, areaExtents);
            SplitQuadTree(quadTree);
            return quadTree;
        }

        private void SplitQuadTree(QuadTree quadTree)
        {
            var lastAddedQuadNodes = new List<Node> { quadTree.RootNode };
            var impassableNodes = GetImpassableNodes(lastAddedQuadNodes).ToList();
            while (impassableNodes.Any())
            {
                lastAddedQuadNodes = impassableNodes
                    .Each(x => x.SplitIntoChildNodes())
                    .SelectMany(x => x.Children)
                    .ToList();
                impassableNodes = GetImpassableNodes(lastAddedQuadNodes).ToList();
            }
        }

        private IEnumerable<Node> GetImpassableNodes(IEnumerable<Node> nodes)
        {
            foreach (var node in nodes)
            {
                if (passabilityTestable.TestBoxPassiblity(node.Origin, node.Extents))
                {
                    node.State = NodeState.Passable;
                }
                else
                {
                    if (node.Area < MinimumImpassableArea)
                    {
                        node.State = NodeState.Impassable;
                    }
                    else
                    {
                        yield return node;
                    }
                }
            }
        }
    }
}
