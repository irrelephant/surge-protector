using System;
using System.Collections.Generic;
using Irrelephant.SurgeProtector.Game.Entities;

namespace Irrelephant.SurgeProtector.Game.Instancing
{
    public class VisibilityEntry
    {
        public int Count;
        public Entity Entity;

        public VisibilityEntry(Entity e) {
            Entity = e;
            Count = 1;
        }
    }

    public class IntelligenceCache : ISensoryDataProvider
    {
        private IDictionary<int, VisibilityEntry> VisibilityCount;

        public IList<Entity> AllVisibleEntities { get; private set; }

        public IntelligenceCache() 
        {
            VisibilityCount = new Dictionary<int, VisibilityEntry>();
            AllVisibleEntities = new List<Entity>();
        }

        public void Subscribe(EntityShip e)
        {
            foreach (var visible in e.Navigation.AllVisibleEntities) {
                OnSubscriberDetectedEntity(visible);
            }

            e.Navigation.OnEntityDetected += OnSubscriberDetectedEntity;
            e.Navigation.OnEntityLost += OnSubscriberLostEntity;

            // We should always see ourselves
            OnSubscriberDetectedEntity(e);
        }

        public void Unsubscribe(EntityShip e)
        {
            foreach (var visible in e.Navigation.AllVisibleEntities)
            {
                OnSubscriberLostEntity(visible);
            }

            e.Navigation.OnEntityDetected -= OnSubscriberDetectedEntity;
            e.Navigation.OnEntityLost -= OnSubscriberLostEntity;

            // We remove visibility on self when unsubbing
            OnSubscriberLostEntity(e);
        }

        public event Action<Entity> OnEntityDetected;

        public event Action<Entity> OnEntityLost;

        public void OnSubscriberDetectedEntity(Entity e)
        {
            if (VisibilityCount.ContainsKey(e.Id))
            {
                var entry = VisibilityCount[e.Id];
                entry.Count++;
            }
            else
            {
                VisibilityCount[e.Id] = new VisibilityEntry(e);
                AllVisibleEntities.Add(e);
                if (OnEntityDetected != null) OnEntityDetected(e);
            }
        }

        public void OnSubscriberLostEntity(Entity e)
        {
            var entry = VisibilityCount[e.Id];
            entry.Count--;
            if (entry.Count == 0)
            {
                VisibilityCount.Remove(e.Id);
                AllVisibleEntities.Remove(e);
                if (OnEntityLost != null) OnEntityLost(e);
            }
        }

    }
}