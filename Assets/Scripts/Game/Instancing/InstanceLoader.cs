﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using UnityEngine;
using MoonSharp.Interpreter;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.Game.Persistance;
using Irrelephant.SurgeProtector.Game.Persistance.Schema;
using Irrelephant.SurgeProtector.Game.Interop;
using Irrelephant.SurgeProtector.Tooling;
using FileMode = System.IO.FileMode;
using Random = UnityEngine.Random;
using Irrelephant.SurgeProtector.Game.Allegiance;
using Irrelephant.SurgeProtector.UI.Hud.Chat;

namespace Irrelephant.SurgeProtector.Game.Instancing
{
    public class InstanceLoader
    {
        private static Script InitLuaRuntime(Instance loadingInstance, InstanceScriptManifest manifestScriptEntryPoint, string scenarioPath)
        {
            if (manifestScriptEntryPoint == null)
            {
                return null;
            }

            using (var entryScriptStream = new FileStream(Path.Combine(scenarioPath, manifestScriptEntryPoint.File),
                FileMode.Open))
            {
                var script = new Script();
                SetupScriptGlobals(loadingInstance, script);
                script.DoStream(entryScriptStream);
                script.Call(script.Globals[manifestScriptEntryPoint.Function]);
                return script;
            }
        }

        private static void SetupScriptGlobals(Instance loadingInstance, Script script)
        {
            script.Globals["objectiveManager"] = new ObjectivesManager();
            script.Globals["unitManager"] = new UnitManager(loadingInstance);
            script.Globals["timer"] = new LuaTimer();
            script.Globals["debug"] = new LuaDebug(loadingInstance);
            script.Globals["chat"] = new LuaChatManager();
        }

        public static Instance LoadInstanceByName(string instanceName, Transform container = null, bool disableScripts = false, int? id = null)
        {
            var manifest = ManifestRepository.ReadInstanceManifest(instanceName);
            return LoadInstanceByManifest(manifest, container, disableScripts, id);
        }

        public static Instance LoadInstanceByManifest(InstanceManifest manifest, Transform container = null, bool disableScripts = false, int? id = null)
        {
            var instance = id == null ? new Instance() : new Instance(id.Value);
            instance.Container = container ?? new GameObject(string.Format("_Instance_{0}", manifest.ScenarioId)).transform;
            instance.Position = new Vector3(manifest.OffsetX == 0 ? Random.value : manifest.OffsetX,
                                            manifest.OffsetY == 0 ? Random.value : manifest.OffsetY) * 1f.ToAU();
            instance.DefaultWarpin = new Vector3(manifest.DefaultWarpinX, manifest.DefaultWarpinY);
            BuildInstanceAllegianceData(manifest, instance);
            InitInstanceObject(manifest, instance, disableScripts);
            instance.GeneratePathfindingData();
            ConsoleLog.Instance.LogFormat("Loaded instance: {0} with extents: {1}. Total objects: {2}. Total entities: {3}", EnumLogType.Info,
                                    instance.Name.Color(Color.green), instance.GeometryExtents, instance.Container.childCount, instance.Entities.Count);
            return instance;
        }

        private static void BuildInstanceAllegianceData(InstanceManifest manifest, Instance instance)
        {
            if (manifest.LocalAllegianceManifest != null)
            {
                instance.LocalFactions = AllegianceRepository.BuildAllegianceDataFromManifest(manifest.LocalAllegianceManifest);
            }
            else
            {
                instance.LocalFactions = new Dictionary<string, Faction>();
            }
        }

        private static void InitInstanceObject(InstanceManifest manifest, Instance instance, bool disableScripts)
        {
            instance.Name = manifest.ScenarioName;
            SpawnInstanceObjects(manifest.Objects, instance);
            instance.Entities = manifest.Entities.Select(om => SpawnAndPrepEntityObject(om, instance)).ToList();
            instance.ScenarioScript = disableScripts
                ? null
                : InitLuaRuntime(instance, manifest.ScriptEntryPoint, ManifestRepository.BuildScenarioPath(manifest.ScenarioId));
            instance.Loaded = true;
        }

        public static void LoadLazyInstance(Instance instance, Transform container = null, bool disableScripts = false)
        {
            var manifest = ManifestRepository.ReadInstanceManifest(instance.Name);
            instance.Container = container ?? new GameObject(string.Format("_Instance_{0}", manifest.ScenarioId)).transform;
            InitInstanceObject(manifest, instance, disableScripts);
            instance.GeneratePathfindingData();
            instance.Loaded = true;
        }

        private static void SpawnInstanceObjects(IList<ObjectManifest> objectManifests, Instance targetInstance)
        {
            foreach (var manifest in objectManifests)
            {
                var instanceObj = SpawnAndPrepObject(manifest, targetInstance);
                targetInstance.NotifyObjectAdded(instanceObj);
            }
        }

        private static Entity SpawnAndPrepEntityObject(EntityManifest manifest, Instance instance)
        {
            var obj = SpawnAndPrepObject(manifest, instance);
            var entity = obj.GetComponent<Entity>();
            if (!string.IsNullOrEmpty(manifest.Tag))
            {
                entity.ScriptableTag = manifest.Tag;
            }
            if (!string.IsNullOrEmpty(manifest.Faction)) {
                var ship = entity as EntityShip;
                if (ship != null)
                {
                    ship.Faction = instance.GetFactionById(manifest.Faction);
                }
            }
            return entity;
        }

        private static GameObject SpawnAndPrepObject(ObjectManifest manifest, Instance instance)
        {
            var obj = SpawningManager.Instance.SpawnByName(manifest.PrefabName);
            obj.transform.SetParent(instance.Container);
            obj.transform.position = new Vector3(manifest.X, manifest.Y);
            obj.transform.rotation = Quaternion.Euler(0f, 0f,
                manifest.RandomRotation ? Random.Range(-180f, 180f) : manifest.Rotation);
            obj.gameObject.name = manifest.PrefabName;
            return obj;
        }
    }
}