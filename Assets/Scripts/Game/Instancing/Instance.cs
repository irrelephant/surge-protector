﻿using System.Collections.Generic;
using System.Linq;
using Irrelephant.SurgeProtector.Game.AI;
using Irrelephant.SurgeProtector.Game.AI.Pathfinding;
using Irrelephant.SurgeProtector.Game.Allegiance;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.Utils;
using MoonSharp.Interpreter;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.Instancing
{
    public class Instance : IPassabilityTestable
    {
        public string Name;

        public int InstanceId { get; private set; }

        public List<Entity> Entities;

        public Vector3 Position;

        public Vector3 DefaultWarpin;

        public Transform Container;

        public Script ScenarioScript;

        public IPathfindable InstancePathdinder { get; private set; }

        public bool IsListed;

        public Instance(): this(IdGenerator.GetNextId()) { }

        public bool Loaded = false;

        private readonly RaycastHit2D[] intersectionResults = new RaycastHit2D[1];
        private readonly Collider2D[] overlapResults = new Collider2D[1];
        
        public IDictionary<string, Faction> LocalFactions;

        private IDictionary<Faction, IntelligenceCache> IntelligenceCaches = new Dictionary<Faction, IntelligenceCache>();

        private Vector2 geometryMaxAbsoluteCoordinates;
        private static readonly Vector2 InstanceBoundaryPadding = new Vector2(10f, 10f);
        
        public Vector2 GeometryExtents
        {
            get { return geometryMaxAbsoluteCoordinates * 2f + InstanceBoundaryPadding; }
        }

        public Instance(int id)
        {
            Entities = new List<Entity>();
            InstanceId = id;
        }

        public void GeneratePathfindingData()
        {
            InstancePathdinder = new Pathfinder(this, GeometryExtents);
        }

        public void NotifyObjectAdded(GameObject instanceObject)
        {
            var objectAbsoluteX = Mathf.Abs(instanceObject.transform.position.x);
            if (objectAbsoluteX > geometryMaxAbsoluteCoordinates.x)
            {
                geometryMaxAbsoluteCoordinates.x = objectAbsoluteX;
            }

            var objectAbsoluteY = Mathf.Abs(instanceObject.transform.position.y);
            if (objectAbsoluteY > geometryMaxAbsoluteCoordinates.y)
            {
                geometryMaxAbsoluteCoordinates.y = objectAbsoluteY;
            }
        }

        public void Suspend(bool sleep)
        {
            if (Container != null)
            {
                Container.position = Vector3.one * 10000000 * InstanceId;
                if (sleep)
                {
                    Container.gameObject.SetActive(false);
                }
            }
        }

        public virtual void OnStartWarpTo()
        {

        }

        public void JoinInstance(EntityShip ship)
        {
            JoinInstance(ship, DefaultWarpin);
        }

        public void JoinInstance(EntityShip ship, Vector3 entryPoint)
        {
            Entities.Add(ship);
            ship.transform.position = entryPoint;

            if (ship == GameManager.Instance.PlayerShip)
            {
                if (IsListed)
                {
                    UiManager.Instance.TargetingBrackets.DespawnBracketFor(this);
                }
            }
            else
            {
                ship.Unsuspend(entryPoint);
            }
        }

        public void LeaveInstance(EntityShip ship)
        {
            Entities.Remove(ship);

            if (ship == GameManager.Instance.PlayerShip)
            {
                Suspend(false);
                if (IsListed)
                {
                    UiManager.Instance.TargetingBrackets.SpawnBracketFor(this);
                }
            }
            else
            {
                ship.Despawn();
            }
        }

        public IntelligenceCache GetIntelligenceCache(Faction f)
        {
            if (f == null)
            {
                return null;
            }
            
            if (!IntelligenceCaches.ContainsKey(f))
            {
                var existingCache = IntelligenceCaches
                    .FirstOrDefault(kvp => Faction.GetRelations(kvp.Key, f) == EnumRelationsStatus.Allied)
                    .Value;
                if (existingCache != null) {
                    IntelligenceCaches[f] = existingCache;
                } else {
                    IntelligenceCaches[f] = new IntelligenceCache();
                }
            }

            return IntelligenceCaches[f];
        }
        
        public bool TestBoxPassiblity(Vector2 origin, Vector2 extents)
        {
            return Physics2D.OverlapAreaNonAlloc(origin - extents / 2f, origin + extents / 2f, overlapResults,
                LayerConstants.MaskFor(LayerConstants.StaticGeometryLayer)) == 0;
        }

        public bool TestLinePassiblity(Vector2 start, Vector2 end)
        {
            return Physics2D.LinecastNonAlloc(start, end, intersectionResults, 
                LayerConstants.MaskFor(LayerConstants.StaticGeometryLayer)) == 0;
        }

        public Faction GetFactionById(string id)
        {
            if (LocalFactions.ContainsKey(id))
            {
                return LocalFactions[id];
            }

            if (GameManager.Instance.GlobalFactions.ContainsKey(id))
            {
                return GameManager.Instance.GlobalFactions[id];
            }

            return null;
        }
    }
}
