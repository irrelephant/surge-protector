﻿namespace Irrelephant.SurgeProtector.Game.Instancing
{
    public class LazyInstance : Instance
    {
        private bool loadingStarted;

        public void LazyLoadInstance()
        {
            if (!loadingStarted && !Loaded)
            {
                loadingStarted = true;
                InstanceLoader.LoadLazyInstance(this);
                Suspend(false);
            }
        }

        public override void OnStartWarpTo()
        {
            LazyLoadInstance();
        }
    }
}
