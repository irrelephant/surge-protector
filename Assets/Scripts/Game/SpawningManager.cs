﻿using System.IO;
using Irrelephant.SurgeProtector.Game.AI.Orders;
using Irrelephant.SurgeProtector.Game.Data;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.Game.Entities.Ship.Modules;
using Irrelephant.SurgeProtector.Game.Entities.Ship.Subsystems;
using Irrelephant.SurgeProtector.Game.Persistance;
using Irrelephant.SurgeProtector.Game.Persistance.Schema;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game
{
    public class SpawningManager : MonoBehaviour
    {
        public const string HullPrefabsFolder = "Hulls";
        public const string ObjectPrefabsFolder = "Objects";
        public const string AmmoPrefabsFolder = "Ammo";

        public static SpawningManager Instance;

        public EquipmentManifest Equipment;
        
        public void Awake()
        {
            Instance = this;
            Equipment = ManifestRepository.ReadGlobalEquipmentManifests();
        }

        /// <summary>
        /// Spawns the static instance element from the "Objects" prefab folder
        /// </summary>
        /// <param name="spawnableName">Object name</param>
        /// <returns></returns>
        public GameObject SpawnByName(string spawnableName)
        {
            return Instantiate(LoadDynamicScenePrefab(spawnableName));
        }

        /// <summary>
        /// Spawns the ship by the manifest name
        /// </summary>
        /// <param name="spawnableName"></param>
        /// <returns></returns>
        public EntityShip SpawnDynamicShipPrefab(string spawnableName)
        {
            if (Equipment.Ships.ContainsKey(spawnableName)) {
                var manifest = Equipment.Ships[spawnableName];
                if (Equipment.Hulls.ContainsKey(manifest.HullType)) {
                    var hullManifest = Equipment.Hulls[manifest.HullType];
                    var hullPrefab = LoadDynamicShipPrefab(hullManifest.Prefab);
                    var spawnedShip = Instantiate(hullPrefab);
                    var entity = InstantiateShipComponents(spawnedShip, hullManifest, manifest);
                    spawnedShip.name = string.Format("[{0}]-{1}", entity.Id, spawnableName);
                    return entity;
                }

                throw new FileNotFoundException("No such hull type defined in the manifests: " + manifest.HullType);
            }

            throw new FileNotFoundException("No such spawnable defined in the manifests: " + spawnableName);
        }

        private GameObject LoadDynamicScenePrefab(string hullName) {
            var spawnedHull = Path.Combine(ObjectPrefabsFolder, hullName);
            var hullPrefab = Resources.Load<GameObject>(spawnedHull);
            if (hullPrefab == null) throw new FileNotFoundException(string.Format("Unable to find scene prefab: {0}.", hullName));
            return hullPrefab;
        }

        private GameObject LoadDynamicShipPrefab(string hullName) {
            var spawnedHull = Path.Combine(HullPrefabsFolder, hullName);
            var hullPrefab = Resources.Load<GameObject>(spawnedHull);
            if (hullPrefab == null) throw new FileNotFoundException(string.Format("Unable to find hull prefab: {0}.", hullName));
            return hullPrefab;
        }

        private EntityShip InstantiateShipComponents(GameObject hull, HullManifest hullManifest, ShipManifest manifest) {
            var shipSignature = InitializeSignature(hull, manifest);
            var shipSensor = InitializeSensor(shipSignature, hull);

            var nav = hull.AddComponent<SubsystemNavigation>();
            nav.MaxSensorRange = manifest.SensorRadius;
            InitializeShipNavigation(nav, shipSensor, manifest);

            var prop = hull.AddComponent<SubsystemPropulsion>();
            InitializeShipPropulsion(prop, manifest);

            var weapons = hull.AddComponent<SubsystemWeaponGroup>();
            InitializeShipWeapons(weapons, hullManifest, manifest);

            var orderManager = hull.AddComponent<OrderManager>();

            var entity = hull.AddComponent<EntityShip>();

            entity.Navigation = nav;
            entity.Propulsion = prop;
            entity.WeaponGroups = new [] { weapons };
            entity.OrderManager = orderManager;
            entity.Health = entity.MaxHealth = manifest.MaxHealth;
            entity.ArmorThickness = manifest.ArmorThickness;
            entity.RecalculateOrderSlots();

            return entity;
        }

        private GameObject InitializeSignature(GameObject hull, ShipManifest manifest)
        {
            var shipSignature = Instantiate(Resources.Load<GameObject>("System/Signature"), hull.transform.position, hull.transform.rotation, hull.transform);
            shipSignature.name = "_Signature";
            var sigCollider = shipSignature.GetComponent<CircleCollider2D>();
            sigCollider.radius = manifest.SignatureRadius;
            return shipSignature;
        }

        private GameObject InitializeSensor(GameObject signature, GameObject hull) {
            var sensor = Instantiate(Resources.Load<GameObject>("System/Sensor"), hull.transform.position, hull.transform.rotation, hull.transform);
            sensor.name = "_Sensor";
            var sensorController = sensor.GetComponent<SensorColliderController>();
            sensorController.OwnSignature = signature.GetComponent<Collider2D>();
            return sensor;
        }

        private void InitializeShipNavigation(SubsystemNavigation nav, GameObject sensor, ShipManifest manifest) {
            nav.MaxSensorRange = manifest.SensorRadius;
            nav.SensorColliderController = sensor.GetComponent<SensorColliderController>();
        }

        private void InitializeShipPropulsion(SubsystemPropulsion prop, ShipManifest manifest) {
            prop.MaxAngularVelocity = manifest.MaxAngularVelocity;
            prop.TopSpeed = manifest.TopSpeed;
            prop.Acceleration = manifest.Acceleration;
            prop.ManeuverabilityFactor = manifest.Maneuverability;
        }
        
        private void InitializeShipWeapons(SubsystemWeaponGroup weapons, HullManifest hull, ShipManifest ship) {
            //TODO: FIX THE NON-TURRET BEHAVIOUR
            weapons.IsTurret = true;

            var slotIndex = 0;
            foreach (var weapon in ship.Weapons) {
                if (slotIndex >= hull.Hardpoints.Length) break;
                var hardpointManifest = hull.Hardpoints[slotIndex++];
                if (weapon != null) {
                    var weaponManifest = Equipment.Items.Weapons[weapon.WeaponType];
                    BuildWeaponObject(weapon, hardpointManifest, weaponManifest, weapons);
                }
            }
            weapons.DetectWeapons();
        }

        private GameObject BuildWeaponObject(WeaponInstanceManifest weapon, HardpointManifest hardpointManifest, WeaponManifest weaponManifest, SubsystemWeaponGroup weaponSubsystem) {
            var weaponObject = Instantiate(
                Resources.Load<GameObject>("Weapons/Hardpoint"),
                new Vector3(weaponSubsystem.transform.position.x + hardpointManifest.X, weaponSubsystem.transform.position.y + hardpointManifest.Y),
                weaponSubsystem.transform.rotation,
                weaponSubsystem.transform
            );
            weaponObject.name = weapon.WeaponType;

            var ammoManifest = Equipment.Items.Ammo[weapon.Ammo];
            var ammoPrefabPath = Path.Combine(AmmoPrefabsFolder, ammoManifest.Prefab);
            var ammoPrefab = Resources.Load<GameObject>(ammoPrefabPath);
            ammoPrefab.name = weapon.Ammo;
            var ammoComponent = ammoPrefab.GetComponent<EntityProjectile>();

            var weaponComponent = weaponObject.GetComponent<ModuleProjectileGun>();
            weaponComponent.WeaponFireArc = weaponManifest.WeaponFireArc;

            weaponComponent.ProjectileLifetime = weaponManifest.ProjectileLifetime;
            weaponComponent.ShotWinddown = weaponManifest.ShotWinddown;
            weaponComponent.MaxShotDelay = weaponManifest.MaxShotDelay;
            weaponComponent.ReloadTime = weaponManifest.ReloadTime;
            weaponComponent.AmmoMax = weaponComponent.AmmoCurrent = weaponManifest.AmmoMax;
            weaponComponent.WeaponLookDirection = Quaternion.AngleAxis(hardpointManifest.Rotation, Vector3.forward) * Vector3.up;
            weaponComponent.ControllingGroup = weaponSubsystem;
            weaponComponent.AmmoColor = new Color(ammoManifest.ColorR, ammoManifest.ColorG, ammoManifest.ColorB);
            weaponComponent.Caliber = weaponManifest.Caliber;

            ammoComponent.ShotDamage = weaponManifest.BaseDamage * ammoManifest.DamageMultiplier;
            weaponComponent.ShotSpeed = weaponManifest.ShotSpeed * ammoManifest.VelocityMultiplier;
            weaponComponent.ShotDeviationDegrees = weaponManifest.ShotDeviation * ammoManifest.DeviationMultiplier;
            weaponComponent.ProjectilePrefab = ammoPrefab;

            return weaponObject;
        }
    }
}