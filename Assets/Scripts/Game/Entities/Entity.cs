﻿using Irrelephant.SurgeProtector.Game.Instancing;
using Irrelephant.SurgeProtector.Utils;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.Entities
{
    public class Entity : MonoBehaviour
    {
        private readonly int id = IdGenerator.GetNextId();

        /// <summary>
        /// Unique id of the current entity used to supply target to scenario scripts.
        /// </summary>
        public int Id
        {
            get { return id; }
        }

        /// <summary>
        /// Lua-scripting tag that is used to target scenario scripts at a certain entity
        /// </summary>
        public string ScriptableTag;

        /// <summary>
        /// Local sublight velocity of the ship.
        /// </summary>
        public Vector3 Velocity;

        /// <summary>
        /// Sublight angular velocity.
        /// </summary>
        public float AngularVelocity;

        public bool IsDespawning
        {
            get; private set;
        }

        /// <summary>
        /// Unsuspends an entity when joining an instance (moves it back to playspace)
        /// </summary>
        public void Unsuspend(Vector3 position)
        {
            transform.position = position;
        }

        public virtual void Awake() { }

        public virtual void ApplyAngularVelocity(float angularVelocity)
        {
            AngularVelocity = angularVelocity;
        }

        public virtual void ApplyVelocity(Vector3 velocity) {
            Velocity += velocity;
        }

        public virtual void Update()
        {
            transform.Translate(Velocity * Time.deltaTime, Space.World);
            transform.Rotate(Vector3.forward * AngularVelocity * Time.deltaTime);
        }

        public override string ToString()
        {
            return ScriptableTag;
        }

        public virtual void Despawn() {
            IsDespawning = true;
            Destroy(gameObject, 1f);
        }
    }
}