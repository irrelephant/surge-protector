﻿using System.Collections;
using Irrelephant.SurgeProtector.Game.Entities.Ship.Subsystems;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.Entities.Ship.Modules
{
    public class ModuleProjectileGun : MonoBehaviour
    {
        public Vector3 WeaponLookDirection;
        public float WeaponFireArc;
        public GameObject ProjectilePrefab;

        public float ShotSpeed;
        public float ShotDeviationDegrees;
        public float ProjectileLifetime;

        public float ShotWinddown;
        public float MaxShotDelay;
        public float ReloadTime;

        private float lastShot = float.MinValue;
        private float reloadStarted = float.MinValue;

        public int AmmoMax;
        public int AmmoCurrent;

        public int Caliber;

        public Color AmmoColor;

        public SubsystemWeaponGroup ControllingGroup;

        public bool CanShoot()
        {
            return Time.time - lastShot > ShotWinddown && Time.time - reloadStarted > ReloadTime;
        }

        public void Shoot(Vector3 lookDirection)
        {
            lookDirection.z = 0;

            var shotDirection = Quaternion.Euler(0f, 0f, (Random.value - 0.5f) * ShotDeviationDegrees) * lookDirection;

            var actualLookDirection = transform.rotation * WeaponLookDirection;
            if (Vector3.Angle(actualLookDirection, shotDirection) > WeaponFireArc)
            {
                return;
            }

            if (CanShoot())
            {
                var deviation = Random.value * Mathf.Abs(MaxShotDelay);
                lastShot = Time.time;
                AmmoCurrent--;
                if (AmmoCurrent == 0)
                {
                    Reload();
                }
                StartCoroutine(Shoot(shotDirection, deviation));
            }
        }

        private void Reload()
        {
            reloadStarted = Time.time;
            AmmoCurrent = AmmoMax;
        }

        private IEnumerator Shoot(Vector3 shotDirection, float deviation)
        {
            yield return new WaitForSeconds(deviation);

            var proj = Instantiate(ProjectilePrefab, transform.position, Quaternion.identity).GetComponent<EntityProjectile>();
            proj.Caliber = Caliber;
            proj.GetComponent<LineRenderer>().endColor = AmmoColor;

            var totalVelocity = GetProjectileVelocity(shotDirection);
            proj.Shoot(ControllingGroup.ParentShip, totalVelocity, ProjectileLifetime);
        }

        private Vector3 GetProjectileVelocity(Vector3 shotDirection)
        {
            var totalVelocity = shotDirection.normalized * ShotSpeed;
            totalVelocity.x += ControllingGroup.ParentShip.Velocity.x;
            totalVelocity.y += ControllingGroup.ParentShip.Velocity.y;
            return totalVelocity;
        }
    }
}
