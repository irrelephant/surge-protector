﻿using UnityEngine;
using Irrelephant.SurgeProtector.Game.Entities.Ship.Modules;

namespace Irrelephant.SurgeProtector.Game.Entities.Ship.Subsystems
{
    public class SubsystemWeaponGroup : Subsystem
    {
        public ModuleProjectileGun[] Weapons;

        public bool IsTurret;
        public bool IsFiring;
        private Vector3 lookPosition;

        public override void Awake()
        {
            base.Awake();
            DetectWeapons();
        }

        public void DetectWeapons() {
            Weapons = GetComponentsInChildren<ModuleProjectileGun>();
        }

        public void UpdateLookPosition(Vector3 updatedLookPosition)
        {
            lookPosition = updatedLookPosition;
        }

        private Vector3 GetLookAtDirection(Vector3 targetPosition, Vector3 objPosition)
        {
            var lookDirection = targetPosition - objPosition;
            lookDirection.z = 0;
            return lookDirection.normalized;
        }

        public ModuleProjectileGun GetWeapon() {
            return Weapons[0];
        }

        public void Update()
        {
            if (IsFiring)
            {
                for (var i = 0; i < Weapons.Length; i++)
                {
                    Weapons[i].Shoot(GetLookAtDirection(lookPosition, IsTurret ? Weapons[i].transform.position : transform.position));
                }
            }
        }
    }
}