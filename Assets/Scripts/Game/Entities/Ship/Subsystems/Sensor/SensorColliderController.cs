﻿using System.Collections;
using System.Collections.Generic;
using Irrelephant.SurgeProtector.Game.Entities;
using UnityEngine;

public class SensorColliderController : MonoBehaviour
{
    private Collider2D ownSignature;
    public Collider2D OwnSignature {
        get { return ownSignature; }
        set { 
            ownSignature = value;
            Physics2D.IgnoreCollision(ownSignature, GetComponent<Collider2D>());
        }
    }


    public delegate void SensorRangeEvent(Entity ship);
    public event SensorRangeEvent OnSignatureEnteringSensorRange;
	public event SensorRangeEvent OnSignatureLeavingSensorRange;

	public void OnTriggerEnter2D(Collider2D signature) {
        var enteringShip = signature.transform.GetComponentInParent<Entity>();
        if (enteringShip != null)
        {
            if (OnSignatureEnteringSensorRange != null)
            {
                OnSignatureEnteringSensorRange(enteringShip);
	        }
        }
    }

    public void OnTriggerExit2D(Collider2D signature)
    {
        var leavingShip = signature.transform.GetComponentInParent<Entity>();
        if (leavingShip != null)
        {
            if (OnSignatureLeavingSensorRange != null)
            {
                OnSignatureLeavingSensorRange(leavingShip);
            }
        }
    }
}
