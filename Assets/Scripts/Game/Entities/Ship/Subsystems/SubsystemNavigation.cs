﻿using System;
using System.Collections;
using System.Collections.Generic;
using Irrelephant.SurgeProtector.Tooling.Debuggery;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.Entities.Ship.Subsystems
{
    public class SubsystemNavigation : Subsystem, ISensoryDataProvider
    {
        private SensorColliderController sensorColliderController;
        public SensorColliderController SensorColliderController
        {
            get {
                return sensorColliderController;
            }
            set {
                if (sensorColliderController != null) {
                    sensorColliderController.OnSignatureEnteringSensorRange -= Detect;
                    sensorColliderController.OnSignatureLeavingSensorRange -= Lose;
                }

                sensorColliderController = value;
                sensorColliderController.OnSignatureEnteringSensorRange += Detect;
                sensorColliderController.OnSignatureLeavingSensorRange += Lose;
            }
        }

        public float MaxSensorRange = 50f;

        public IList<Entity> AllVisibleEntities 
        {
            get; private set;
        }

        public SubsystemNavigation() {
            AllVisibleEntities = new List<Entity>();
        }

        private DebuggeryCircle sensorRangeDbg;

        public event Action<Entity> OnEntityDetected;
        public event Action<Entity> OnEntityLost;

        public void Detect(Entity e)
        {
            AllVisibleEntities.Add(e);
            if (OnEntityDetected != null) OnEntityDetected(e);

            var entityShip = e as EntityShip;
            if (entityShip != null) {
                entityShip.NotifyEntityDetected(ParentShip);
            }
        }

        public void Lose(Entity e)
        {
            AllVisibleEntities.Remove(e);
            if (OnEntityLost != null) OnEntityLost(e);
        }

        public void Subscribe(EntityShip ship)
        {
            // Navigation subsystem is always active
        }

        public void Unsubscribe(EntityShip ship)
        {
            // Navigation subsystem is always active
        }
    }
}