﻿using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.Entities.Ship.Subsystems
{
    public class SubsystemPropulsion : Subsystem
    {
        /// <summary>
        /// Maximum angular velocity in degrees per second
        /// </summary>
        public float MaxAngularVelocity = 10f;
        
        /// <summary>
        /// Warp speed in unity per second
        /// </summary>
        /// <returns></returns>
        public float WarpSpeed = 0.1f.ToAU();

        /// <summary>
        /// Top speed in units per second
        /// </summary>
        public float TopSpeed = 5f;

        /// <summary>
        /// Acceleration per second (in percent of top speed per second.)
        /// </summary>
        public float Acceleration = 0.05f;

        /// <summary>
        /// Maximum "rotational distance" allowed when accelerating towards the target
        /// </summary>
        public float ManeuverabilityFactor = 0.5f;
    }
}