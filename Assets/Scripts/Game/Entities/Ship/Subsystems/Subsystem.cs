﻿using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.Entities.Ship.Subsystems
{
    public class Subsystem : MonoBehaviour
    {
        [HideInInspector]
        public EntityShip ParentShip;

        public virtual void Awake()
        {
            ParentShip = GetComponentInParent<EntityShip>();
        }
    }
}