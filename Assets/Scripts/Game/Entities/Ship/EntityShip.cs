﻿using System;
using System.Linq;
using UnityEngine;

using Irrelephant.SurgeProtector.Game.AI.Orders;
using Irrelephant.SurgeProtector.Game.Entities.Ship.Subsystems;
using Irrelephant.SurgeProtector.Game.Enum;
using Irrelephant.SurgeProtector.Game.Allegiance;
using Irrelephant.SurgeProtector.Game.Instancing;

namespace Irrelephant.SurgeProtector.Game.Entities
{
    public class EntityShip : Entity
    {
        /// <summary>
        /// Reference to a <see cref="SubsystemPropulsion"/> subsystem instance.
        /// </summary>
        private SubsystemPropulsion propulsion;
        public SubsystemPropulsion Propulsion {
            get {
                return propulsion;
            }
            set {
                propulsion = value;
                propulsion.ParentShip = this;
            }
        }

        /// <summary>
        /// Reference to a <see cref="SubsystemNavigation"/> subsystem instance.
        /// </summary>
        private SubsystemNavigation navigation;
        public SubsystemNavigation Navigation
        {
            get {
                return navigation;
            }
            set {
                navigation = value;
                navigation.ParentShip = this;
                CurrentSensor = navigation;
            }
        }

        /// <summary>
        /// References to all the ship's <see cref="SubsystemWeaponGroup"/> subsystem instances.
        /// </summary>
        [HideInInspector]
        private SubsystemWeaponGroup[] weaponGroups;
        public SubsystemWeaponGroup[] WeaponGroups {
            get {
                return weaponGroups;
            }
            set {
                weaponGroups = value;
                foreach (var weaponGroup in weaponGroups) {
                    weaponGroup.ParentShip = this;
                }
            }
        }

        public float SignatureRadius = 3f;

        /// <summary>
        /// Explosion prefab
        /// </summary>
        public GameObject ExpliosonFx;

        private Faction faction;

        /// <summary>
        /// Faction that the current entity belongs to
        /// </summary>
        public Faction Faction { 
            get 
            {
                return faction;
            }
            set
            {
                faction = value;
                if (faction == null) {
                    CurrentSensor = Navigation;
                }
                else 
                {
                    CurrentSensor = GameManager.Instance.CurrentInstance.GetIntelligenceCache(faction);
                }
            }
        }

        private ISensoryDataProvider sensor;
        public ISensoryDataProvider CurrentSensor {
            get {
                return sensor;
            }
            private set {
                if (sensor != value) {
                    if (OnSensorChanged != null) OnSensorChanged(sensor, value);
                    if (sensor != null) sensor.Unsubscribe(this);
                    value.Subscribe(this);
                    sensor = value;
                }
            }
        }

        /// <summary>
        /// Current ship's state of movement.
        /// </summary>
        public ShipState CurrentState = ShipState.Sublight;

        /// <summary>
        /// An enum mask of order types that can be given to the entity.
        /// </summary>
        [EnumFlag(rowCount: 2)]
        public OrderSlot EligibleOrderSlots;

        /// <summary>
        /// Current hull's health value. Ship is considered destroyed when it reaches zero.
        /// </summary>
        public float Health;

        /// <summary>
        /// Hull's maximum health.
        /// </summary>
        public float MaxHealth;

        /// <summary>
        /// Armor thickness in millimeters
        /// </summary>
        public int ArmorThickness;

        /// <summary>
        /// Whether or not the hull is considered destroyed.
        /// </summary>
        public bool IsDestroyed;

        /// <summary>
        /// All the colliders that make up the extends of the ship.
        /// </summary>
        public Collider2D[] ShipGeometry
        {
            get
            {
                return shipGeometry ?? (shipGeometry = GetComponentsInChildren<Collider2D>());
            }
        }

        /// <summary>
        /// Order manager that controls order execution of the current ship
        /// </summary>
        private OrderManager orderManager;
        public OrderManager OrderManager {
            get {
                return orderManager;
            }
            set {
                orderManager = value;
                orderManager.ParentEntityShip = this;
            }
        }

        /// <summary>
        /// Geometry cache. Use <see cref="ShipGeometry"/> property instead.
        /// </summary>
        private Collider2D[] shipGeometry;

        public override void Despawn()
        {
            OnAllegianceChanged = null;
            OnSensorChanged = null;
            OnDamageTaken = null;
            OnShipDestroyed = null;
            sensor.Unsubscribe(this);
            base.Despawn();
        }

        /// <summary>
        /// Triggered when entity changes allegiance. Arguments are (old faction, new faction)
        /// </summary>
        public event Action<Faction, Faction> OnAllegianceChanged;

        /// <summary>
        /// Triggered when entity changes current sensor. Arguments are (old sensor, new sensor)
        /// </summary>
        public event Action<ISensoryDataProvider, ISensoryDataProvider> OnSensorChanged;

        /// <summary>
        /// Triggered when taking damage. Argument is amount.
        /// </summary>
        public event Action<float> OnDamageTaken;

        /// <summary>
        /// Triggered when entity is destroyed.
        /// </summary>
        public event Action OnShipDestroyed;

        /// <summary>
        /// Triggered when the current entity is detected. An argument is detector entity
        /// </summary>
        public event Action<EntityShip> OnDetected;

        public override void ApplyAngularVelocity(float theta) {
            AngularVelocity = Mathf.LerpAngle(AngularVelocity,
                theta,
                MovementConstants.AngularVelocityLerpFactor);
        }

        public override void ApplyVelocity(Vector3 dv) {
            Velocity = Velocity + dv;
        }

        public override void Update()
        {
            ProcessMovement();
            ClampMovement();
        }

        private void ClampMovement() {
            if (Velocity.magnitude > Propulsion.TopSpeed)
            {
                Velocity *= 0.99f;
            }

            if (AngularVelocity > Propulsion.MaxAngularVelocity) {
                AngularVelocity *= 0.99f;
            }
        }

        private void ProcessMovement()
        {
            switch (CurrentState)
            {
                case ShipState.Sublight:
                    base.Update();
                    break;
                case ShipState.InWarp:
                    break;
            }
        }

        public void TakeDamage(float dmg)
        {
            if (IsDestroyed || IsDespawning) return;
            Health = Mathf.Max(0, Health - dmg);
            if (OnDamageTaken != null) OnDamageTaken(dmg);
            if (Health == 0)
            {
                IsDestroyed = true;
                if (OnShipDestroyed != null) OnShipDestroyed();
                if (ExpliosonFx != null) {
                    var explosion = Instantiate(ExpliosonFx, transform.position, Quaternion.identity);
                    Destroy(explosion, 10f);
                }
                Despawn();
            }
        }

        public void NotifyEntityDetected(EntityShip detector)
        {
            if (OnDetected != null) {
                OnDetected(detector);
            }
        }

        public void RecalculateOrderSlots() {
            if (Propulsion.TopSpeed > 0) {
                EligibleOrderSlots |= OrderSlot.Movement;
            }

            if (Propulsion.WarpSpeed > 0) {
                EligibleOrderSlots |= OrderSlot.Warp;
            }

            if (WeaponGroups.Any(weaponGroup => weaponGroup.Weapons.Any())) {
                EligibleOrderSlots |= OrderSlot.TurretAttack;
            }
        }
    }
}