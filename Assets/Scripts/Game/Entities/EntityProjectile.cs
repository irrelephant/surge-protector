﻿using System.Linq;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.Entities
{
    public class EntityProjectile : Entity
    {
        public int Caliber;
     
        public float ShotDamage;

        private Vector3 velocity;

        private EntityShip shooterShip;

        private const int RaycastBufferSize = 20;
        protected static RaycastHit2D[] RaycastResultBuffer = new RaycastHit2D[RaycastBufferSize];

        const float PenetrationAdjustmentFactor = 0.05095f;

        public override void Update()
        {
            var hitCount = Physics2D.RaycastNonAlloc(
                transform.position,
                velocity,
                RaycastResultBuffer,
                velocity.magnitude * Time.deltaTime,
                LayerConstants.MaskFor(LayerConstants.StaticGeometryLayer, LayerConstants.Default));
                
            for (var hitIndex = 0; hitIndex < hitCount; hitIndex++)
            {
                var hit = RaycastResultBuffer[hitIndex];
                if (!shooterShip.ShipGeometry.Contains(hit.collider))
                {
                    Destroy(gameObject);
                    PlayHitFx(hit);

                    var hitShip = hit.collider.GetComponentInParent<EntityShip>();
                    if (hitShip != null)
                    {
                        var appliedDamage = CalculateAppliedDamage(hit, hitShip);
                        hitShip.TakeDamage(appliedDamage);
                    }
                }
            }

            transform.Translate(velocity * Time.deltaTime, Space.World);
        }

        private float CalculateAppliedDamage(RaycastHit2D hit, EntityShip ship) {
            var angle = Vector2.Angle(-velocity.normalized, hit.normal);
            var actualArmorThickness = ship.ArmorThickness / Mathf.Cos(angle * Mathf.Deg2Rad);
            var penetrationScore = Mathf.Sqrt(velocity.magnitude) * 10 * Caliber / actualArmorThickness;
            var armorPenChance = Mathf.Pow(penetrationScore, 0.666f) * PenetrationAdjustmentFactor;
            var isArmorPenetrated = Random.value < armorPenChance;
            if (isArmorPenetrated) {
                return ShotDamage;
            }
            return ShotDamage * 0.3f;
        }

        private static void PlayHitFx(RaycastHit2D hit)
        {
            ParticleLibrary.Instance.ProjectileHitFx.Emit(new ParticleSystem.EmitParams
            {
                position = hit.point
            }, 1);
        }

        public void Shoot(EntityShip shooter, Vector3 vel, float ttl)
        {
            velocity = vel;
            Destroy(gameObject, ttl);
            shooterShip = shooter;
        }
    }
}