﻿using System.Collections.Generic;
using System.Linq;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.Game.Instancing;
using Irrelephant.SurgeProtector.Game.Allegiance;
using Irrelephant.SurgeProtector.Tooling.Debuggery;
using MoonSharp.Interpreter;
using UnityEngine;
using System;
using Irrelephant.SurgeProtector.Extensions;
using Irrelephant.SurgeProtector.Game.Persistance;

namespace Irrelephant.SurgeProtector.Game
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;

        public GameObject ShipPrefab;

        public Instance CurrentInstance;

        public EntityShip PlayerShip;

        public Faction PlayerFaction;

        public IDictionary<string, Faction> GlobalFactions;        
    
        public void Awake()
        {
            UserData.RegisterAssembly();
            Instance = this;
        }

        public void Start()
        {
            GlobalFactions = AllegianceRepository.BuildGlobalAllegianceData();
            SpawnDefaultInstance();
            SpawnPlayer();
        }

        private void SpawnPlayer()
        {
            PlayerShip = SpawningManager.Instance.SpawnDynamicShipPrefab("CommandCruiser");
            UiManager.Instance.Controlled = PlayerShip;
            UiManager.Instance.BlackoutOverlay.DisableOverlayOver(0.5f);
            PlayerFaction = GlobalFactions.First().Value;
            PlayerShip.Faction = PlayerFaction;
            PlayerShip.ScriptableTag = "PLAYER";
            CurrentInstance.JoinInstance(PlayerShip);
        }

        private void SpawnDefaultInstance()
        {
            if (CurrentInstance == null)
            {
                CurrentInstance = InstanceLoader.LoadInstanceByName("default");
                CurrentInstance.IsListed = true;
            }
        }
    }
}