﻿using System;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.Data
{
    [CreateAssetMenu]
    public class GameObjectLibrary : ScriptableObject
    {
        [Serializable]
        public class LibraryEntry
        {
            public string Name;
            public GameObject Prefab;
        }

        public LibraryEntry[] Spawnables;
    
        public LibraryEntry[] Hulls;

        public GameObject SignaturePrefab;
        public GameObject SensorPrefab;
    }
}