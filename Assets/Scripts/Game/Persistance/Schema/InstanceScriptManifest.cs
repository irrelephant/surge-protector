﻿using System.Runtime.Serialization;

namespace Irrelephant.SurgeProtector.Game.Persistance.Schema
{
    [DataContract]
    public class InstanceScriptManifest
    {
        [DataMember(Name = "file")]
        public string File;

        [DataMember(Name = "function")]
        public string Function;
    }
}