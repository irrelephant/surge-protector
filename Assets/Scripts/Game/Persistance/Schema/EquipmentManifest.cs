using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Irrelephant.SurgeProtector.Game.Persistance.Schema
{
    [DataContract]
    public class EquipmentManifest
    {
        [DataMember(Name = "ships")]
        public IDictionary<string, ShipManifest> Ships;

        [DataMember(Name = "hulls")]
        public IDictionary<string, HullManifest> Hulls;

        [DataMember(Name = "items")]
        public ItemsManifest Items;
    }
}