﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Irrelephant.SurgeProtector.Game.Persistance.Schema
{
    [DataContract]
    public class AllegianceManifest
    {
        [DataMember(Name = "factions")]
        public IList<FactionManifest> Factions;
    }
}