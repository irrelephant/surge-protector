﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Irrelephant.SurgeProtector.Game.Persistance.Schema
{
    [DataContract]
    public class InstanceManifest
    {
        public string ScenarioId;

        [DataMember(Name = "name")]
        public string ScenarioName;

        [DataMember(Name = "by")]
        public string Author;

        [DataMember(Name = "x")]
        public float OffsetX;

        [DataMember(Name = "y")]
        public float OffsetY;

        [DataMember(Name = "warpinX")]
        public float DefaultWarpinX;

        [DataMember(Name = "warpinY")]
        public float DefaultWarpinY;

        [DataMember(Name = "scriptEntryPoint")]
        public InstanceScriptManifest ScriptEntryPoint;

        [DataMember(Name = "objects")]
        public IList<ObjectManifest> Objects;

        [DataMember(Name = "entities")]
        public IList<EntityManifest> Entities;

        [DataMember(Name = "allegiance")]
        public AllegianceManifest LocalAllegianceManifest;
    }
}