﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Irrelephant.SurgeProtector.Game.Persistance.Schema
{
    [DataContract]
    public class FactionManifest
    {
        [DataMember(Name = "name")]
        public string Name;

        [DataMember(Name = "id")]
        public string Id;

        [DataMember(Name = "relations")]
        public IDictionary<string, string> Relations;

        [DataMember(Name = "defaultRelations")]
        public string DefaultRelations;
    }
}