using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Irrelephant.SurgeProtector.Game.Persistance.Schema
{
    [DataContract]
    public class ItemsManifest {
        
        [DataMember(Name = "weapons")]
        public IDictionary<string, WeaponManifest> Weapons;

        [DataMember(Name = "ammo")]
        public IDictionary<string, AmmoManifest> Ammo;
    }
}