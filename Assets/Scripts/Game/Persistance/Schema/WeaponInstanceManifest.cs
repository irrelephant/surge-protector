using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Irrelephant.SurgeProtector.Game.Persistance.Schema
{
    [DataContract]
    public class WeaponInstanceManifest {
        
        [DataMember(Name = "weaponType", IsRequired = true)]
        public string WeaponType;

        [DataMember(Name = "ammo", IsRequired = true)]
        public string Ammo;
    }
}