using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Irrelephant.SurgeProtector.Game.Persistance.Schema
{
    [DataContract]
    public class AmmoManifest {
        
        [DataMember(Name = "prefab", IsRequired = true)]
        public string Prefab;

        [DataMember(Name = "caliber", IsRequired = true)]
        public int Caliber;

        [DataMember(Name = "damageMultiplier", IsRequired = true)]
        public float DamageMultiplier;

        [DataMember(Name = "velocityMultiplier", IsRequired = true)]
        public float VelocityMultiplier;

        [DataMember(Name = "deviationMultiplier", IsRequired = true)]
        public float DeviationMultiplier;

        [DataMember(Name = "colorR", IsRequired = false)]
        public float ColorR = 1f;

        [DataMember(Name = "colorG", IsRequired = false)]
        public float ColorG = 1f;

        [DataMember(Name = "colorB", IsRequired = false)]
        public float ColorB = 1f;
    }
}