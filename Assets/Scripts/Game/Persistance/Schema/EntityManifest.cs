﻿using System.Runtime.Serialization;

namespace Irrelephant.SurgeProtector.Game.Persistance.Schema
{
    [DataContract]
    public class EntityManifest : ObjectManifest
    {
        [DataMember(Name = "tag")]
        public string Tag;

        [DataMember(Name = "faction")]
        public string Faction;
    }
}