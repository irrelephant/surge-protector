using UnityEngine;
using System.Runtime.Serialization;

namespace Irrelephant.SurgeProtector.Game.Persistance.Schema
{
    [DataContract]
    public class ShipManifest
    {
        [DataMember(Name = "hullType", IsRequired = true)]
        public string HullType;

        [DataMember(Name = "signatureRadius", IsRequired = true)]
        public int SignatureRadius;

        [DataMember(Name = "sensorRadius", IsRequired = true)]
        public int SensorRadius;

        [DataMember(Name = "maxHealth", IsRequired = true)]
        public int MaxHealth;

        [DataMember(Name = "maxAngularVelocity", IsRequired = true)]
        public float MaxAngularVelocity;

        [DataMember(Name = "topSpeed", IsRequired = true)]
        public float TopSpeed;

        [DataMember(Name = "acceleration", IsRequired = true)]
        public float Acceleration;

        [DataMember(Name = "maneuverability", IsRequired = true)]
        public float Maneuverability;

        [DataMember(Name = "armorThickness", IsRequired = true)]
        public int ArmorThickness;

        [DataMember(Name = "weapons", IsRequired = true)]
        public WeaponInstanceManifest[] Weapons;
    }
}