using System.Runtime.Serialization;

namespace Irrelephant.SurgeProtector.Game.Persistance.Schema
{
    [DataContract]
    public class HardpointManifest {
        
        [DataMember(Name = "x")]
        public float X;

        [DataMember(Name = "y")]
        public float Y;

        [DataMember(Name = "rotation")]
        public float Rotation;
    }
}