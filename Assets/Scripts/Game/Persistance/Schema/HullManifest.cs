using UnityEngine;
using System.Runtime.Serialization;

namespace Irrelephant.SurgeProtector.Game.Persistance.Schema
{
    [DataContract]
    public class HullManifest
    {
        [DataMember(Name = "hardpoints", IsRequired = true)]
        public HardpointManifest[] Hardpoints;

        [DataMember(Name = "prefab", IsRequired = true)]
        public string Prefab;
    }
}