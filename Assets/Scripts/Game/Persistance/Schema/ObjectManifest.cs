﻿using System.Runtime.Serialization;

namespace Irrelephant.SurgeProtector.Game.Persistance.Schema
{
    [DataContract]
    public class ObjectManifest
    {
        [DataMember(Name = "prefabName", IsRequired = true)]
        public string PrefabName;

        [DataMember(Name = "x", IsRequired = true)]
        public float X;

        [DataMember(Name = "y", IsRequired = true)]
        public float Y;

        [DataMember(Name = "rotation")]
        public float Rotation;

        [DataMember(Name = "randomRotation")]
        public bool RandomRotation;
    }
}