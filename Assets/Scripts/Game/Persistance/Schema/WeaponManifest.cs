using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Irrelephant.SurgeProtector.Game.Persistance.Schema
{
    [DataContract]
    public class WeaponManifest {
        
        [DataMember(Name = "weaponFireArc", IsRequired = true)]
        public float WeaponFireArc;

        [DataMember(Name = "shotSpeed", IsRequired = true)]
        public float ShotSpeed;

        [DataMember(Name = "shotDeviation", IsRequired = true)]
        public float ShotDeviation;

        [DataMember(Name = "projectileLifetime", IsRequired = true)]
        public float ProjectileLifetime;

        [DataMember(Name = "shotWinddown", IsRequired = true)]
        public float ShotWinddown;

        [DataMember(Name = "maxShotDelay", IsRequired = true)]
        public float MaxShotDelay;

        [DataMember(Name = "reloadTime", IsRequired = true)]
        public float ReloadTime;

        [DataMember(Name = "ammoMax", IsRequired = true)]
        public int AmmoMax;

        [DataMember(Name = "baseDamage", IsRequired = true)]
        public int BaseDamage;

        [DataMember(Name = "caliber", IsRequired = true)]
        public int Caliber;
    }
}