﻿using System.IO;
using System.Linq;
using Irrelephant.SurgeProtector.Utils;
using Irrelephant.SurgeProtector.Game.Persistance.Schema;
using Newtonsoft.Json;
using UnityEngine;
using Newtonsoft.Json.Linq;

namespace Irrelephant.SurgeProtector.Game.Persistance
{
    public static class ManifestRepository
    {
        public static string StreamingAssetsPath;

        static ManifestRepository()
        {
            StreamingAssetsPath = Application.streamingAssetsPath;
        }

        public static InstanceManifest ReadInstanceManifest(string instanceName)
        {
            var sanitizedName = PathSanitizer.SanitizeFilename(instanceName, '_');
            var scenarioPath = BuildScenarioPath(sanitizedName);
            if (Directory.Exists(scenarioPath))
            {
                using (var instanceManifestText = new StreamReader(Path.Combine(scenarioPath, "Manifest.json")))
                {
                    var manifest = JsonConvert.DeserializeObject<InstanceManifest>(instanceManifestText.ReadToEnd());
                    manifest.ScenarioId = instanceName;
                    return manifest;
                }
            }

            throw new FileNotFoundException("Scenario folder not found - " + instanceName);
        }

        public static AllegianceManifest ReadGlobalAllegianceManifest()
        {
            using (var instanceManifestText = new StreamReader(Path.Combine(StreamingAssetsPath, "Allegiance.json")))
            {
                return JsonConvert.DeserializeObject<AllegianceManifest>(instanceManifestText.ReadToEnd());
            }
        }

        public static EquipmentManifest ReadGlobalEquipmentManifests()
        {
            return Directory
                .GetFiles(StreamingAssetsPath, "*.Items.json", SearchOption.AllDirectories)
                .Select(file => {
                    using (var instanceManifestText = new StreamReader(file))
                    {
                        return JContainer.Parse(instanceManifestText.ReadToEnd());
                    }
                })
                .Aggregate<JToken, JContainer>((JContainer)JToken.FromObject(new object()), (acc, val) => {
                    acc.Merge(val);
                    return acc;
                }).ToObject<EquipmentManifest>();
        }

        public static void StoreInstanceManifest(InstanceManifest manifest)
        {
            var sanitizedName = PathSanitizer.SanitizeFilename(manifest.ScenarioId, '_');
            var scenarioPath = BuildScenarioPath(sanitizedName);
            if (!Directory.Exists(scenarioPath))
            {
                Directory.CreateDirectory(scenarioPath);
            }

            using (var manifestWriter = new StreamWriter(Path.Combine(scenarioPath, "Manifest.json")))
            {
                manifestWriter.Write(JsonConvert.SerializeObject(manifest, Formatting.Indented));
            }
        }

        public static string BuildScenarioPath(string instanceName)
        {
            return Path.Combine(Path.Combine(StreamingAssetsPath, "Scenarios"), instanceName);
        }
    }
}