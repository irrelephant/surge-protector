using System.Collections.Generic;
using System.Linq;
using Irrelephant.SurgeProtector.Extensions;
using Irrelephant.SurgeProtector.Game.Allegiance;
using Irrelephant.SurgeProtector.Game.Persistance.Schema;

namespace Irrelephant.SurgeProtector.Game.Persistance
{
    public static class AllegianceRepository
    {
        public static IDictionary<string, Faction> BuildGlobalAllegianceData()
        {
            return BuildAllegianceDataFromManifest(ManifestRepository.ReadGlobalAllegianceManifest());
        }

        public static IDictionary<string, Faction> BuildAllegianceDataFromManifest(AllegianceManifest manifest)
        {
            var globalFactions = manifest
                .Factions
                .ToDictionary(fm => fm.Id, fm => new Faction
                {
                    Id = fm.Id,
                    Name = fm.Name,
                    DefaultRelations = ParseRelations(fm.DefaultRelations)
                });

            globalFactions.Apply((id, fact) =>
            {
                fact.Relations = BuildRelations(globalFactions,
                    manifest.Factions
                    .First(factManifest => factManifest.Id == id)
                    .Relations);
            });

            return globalFactions;
        }

        private static IDictionary<Faction, EnumRelationsStatus> BuildRelations(IDictionary<string, Faction> readFactions, IDictionary<string, string> rawRelations)
        {
            if (rawRelations == null)
            {
                return new Dictionary<Faction, EnumRelationsStatus>();
            }

            return rawRelations.ToDictionary(
                manifestRelations => readFactions[manifestRelations.Key],
                manifestRelations => ParseRelations(manifestRelations.Value));
        }

        private static EnumRelationsStatus ParseRelations(string relations)
        {
            if (string.IsNullOrEmpty(relations))
            {
                return EnumRelationsStatus.NeutralHostile;
            }
            else
            {
                return (EnumRelationsStatus)System.Enum.Parse(typeof(EnumRelationsStatus), relations);
            }
        }

    }

}