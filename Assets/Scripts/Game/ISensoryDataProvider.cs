using System;
using System.Collections.Generic;
using Irrelephant.SurgeProtector.Game.Entities;


namespace Irrelephant.SurgeProtector.Game
{
    public interface ISensoryDataProvider
    {
        event Action<Entity> OnEntityDetected;

        event Action<Entity> OnEntityLost;

        void Subscribe(EntityShip ship);

        void Unsubscribe(EntityShip ship);

        IList<Entity> AllVisibleEntities { get; }

    }
}