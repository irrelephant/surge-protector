﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game {
	public static class MovementConstants {
		public const float RotationAcceleration = 2f;
		public const float AngularVelocityLerpFactor = 0.05f;
		public const float DecelerationBoostFactor = 2f;
		public const float WaypointReachThreshold = 0.1f;
		
	}
}