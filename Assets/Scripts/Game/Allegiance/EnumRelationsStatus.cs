﻿namespace Irrelephant.SurgeProtector.Game.Allegiance
{
    public enum EnumRelationsStatus
    {
        Hostile,
        NeutralHostile,
        Neutral,
        Friendly,
        Allied
    }
}
