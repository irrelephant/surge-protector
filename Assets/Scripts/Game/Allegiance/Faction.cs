﻿using System.Collections.Generic;
using Irrelephant.SurgeProtector.UI;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.Allegiance
{
    public class Faction
    {
        public string Name { get; set; }

        public string Id { get; set; }

        public EnumRelationsStatus DefaultRelations { get; set; }

        public IDictionary<Faction, EnumRelationsStatus> Relations { get; set; }

        private static EnumRelationsStatus GetRelationTowards(Faction a, Faction b) {
            if (a == null)
            {
                return EnumRelationsStatus.NeutralHostile;
            }

            if (b == null) {
                return a.DefaultRelations;
            }

            return a.Relations.ContainsKey(b) ? a.Relations[b] : a.DefaultRelations;
        }

        public static EnumRelationsStatus GetRelations(Faction a, Faction b)
        {
            if (a == b)
            {
                return EnumRelationsStatus.Allied;
            }
            
            return Worst(GetRelationTowards(a, b), GetRelationTowards(b, a));
        }

        public static EnumRelationsStatus Worst(EnumRelationsStatus a, EnumRelationsStatus b) {
            return (int)a - (int)b < 0 ? a : b; 
        }

        public static Color GetColorFromRelations(EnumRelationsStatus relationsStatus)
        {
            switch (relationsStatus)
            {
                case EnumRelationsStatus.Allied: return UiColors.ColorAlliedMarker;
                case EnumRelationsStatus.Friendly: return UiColors.ColorFriendlyMarker;
                case EnumRelationsStatus.NeutralHostile: return UiColors.ColorNeutralHostileMarker;
                case EnumRelationsStatus.Hostile: return UiColors.ColorHostileMarker;
                default: return UiColors.ColorNeutralMarker;
            }
        }

        public static Color GetColorFromFaction(Faction forWhom, Faction who) {
            if (forWhom == who) {
                return UiColors.ColorOwnMarker;
            }

            return GetColorFromRelations(Faction.GetRelations(forWhom, who));
        }
    }
}
