using System;
using System.Collections.Generic;
using System.Linq;
using Irrelephant.SurgeProtector.Game.Entities;

namespace Irrelephant.SurgeProtector.Game.Allegiance
{
    public static class AllegianceExtensions
    {
        public static bool IsHostile(this EntityShip ship, EntityShip other)
        {
            return Faction.GetRelations(ship.Faction, other.Faction) < EnumRelationsStatus.Neutral;
        }

        public static bool IsPlayerFaction(this EntityShip ship)
        {
            return ship.Faction == GameManager.Instance.PlayerFaction;
        }
    }
}