﻿using System.Collections.Generic;

namespace Irrelephant.SurgeProtector.Game.Interop.OrderFactories
{
    public class OrderFactoryRegistry
    {
        private static readonly IDictionary<string, IOrderFactory> Factories = new Dictionary<string, IOrderFactory>
        {
            /* Orders */
            {
                "ORDER_MOVE_TO",
                new OrderMoveToFactory()
            },
            {
                "ORDER_STOP",
                new OrderStopFactory()
            },
            {
                "ORDER_WARP_OUT",
                new OrderWarpOutFactory()
            },

            /* Behaviours */
            {
                "BEHAVIOUR_AGGRESSIVE",
                new BehaviourAggressiveFactory()
            }
        };

        public static IOrderFactory Find(string orderName)
        {
            return Factories[orderName];
        }
    }
}