﻿using System;
using MoonSharp.Interpreter;
using UnityEngine;
using Irrelephant.SurgeProtector.Game.AI.Orders;

namespace Irrelephant.SurgeProtector.Game.Interop.OrderFactories
{
    public class OrderMoveToFactory : IOrderFactory
    {
        public Order Assemble(params DynValue[] luaValues)
        {
            if (luaValues.Length == 2)
            {
                if (luaValues[0].Type == DataType.Number && luaValues[1].Type == DataType.Number)
                {
                    return new OrderMoveTo(new Vector2((float)luaValues[0].Number, (float)luaValues[1].Number));
                }

            }

            throw new ArgumentException("Invalid argument count. Expecting 2 and got " + luaValues.Length);
        }
    }
}