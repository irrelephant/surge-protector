﻿using MoonSharp.Interpreter;
using Irrelephant.SurgeProtector.Game.AI.Orders;

namespace Irrelephant.SurgeProtector.Game.Interop.OrderFactories
{
    public interface IOrderFactory
    {
        Order Assemble(params DynValue[] luaValues);
    }
}