﻿using MoonSharp.Interpreter;
using Irrelephant.SurgeProtector.Game.AI.Orders;

namespace Irrelephant.SurgeProtector.Game.Interop.OrderFactories
{
    public class OrderWarpOutFactory : IOrderFactory
    {
        public Order Assemble(params DynValue[] luaValues)
        {
            return new OrderWarpOut();
        }
    }
}