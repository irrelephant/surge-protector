﻿using MoonSharp.Interpreter;
using Irrelephant.SurgeProtector.Game.AI.Orders;
using Irrelephant.SurgeProtector.Game.AI.Orders.Behaviours;

namespace Irrelephant.SurgeProtector.Game.Interop.OrderFactories
{
    public class BehaviourAggressiveFactory : IOrderFactory
    {
        public Order Assemble(params DynValue[] luaValues)
        {
            return new BehaviourAgressive();
        }
    }
}