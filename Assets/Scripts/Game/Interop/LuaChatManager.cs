﻿using System.Diagnostics.CodeAnalysis;
using MoonSharp.Interpreter;
using Irrelephant.SurgeProtector.UI.Hud.Chat;

namespace Irrelephant.SurgeProtector.Game.Interop
{
    [MoonSharpUserData]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class LuaChatManager
    {
        public void addMessage(string message)
        {
			UiManager.Instance.ChatController.AppendMessage(message);
        }

        public string color(string message, string hexColor) {
            return ChatExtensions.Color(message, hexColor);
        }

        public string bold(string message) {
            return ChatExtensions.Bold(message);
        }

        public string italic(string message) {
            return ChatExtensions.Italic(message);
        }

        public string size(string message, int size) {
            return ChatExtensions.Size(message, size);
        }
    }
}