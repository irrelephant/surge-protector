﻿using System.Diagnostics.CodeAnalysis;
using Irrelephant.SurgeProtector.Game.AI.Orders;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.Game.Instancing;
using MoonSharp.Interpreter;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.Interop
{
    [MoonSharpUserData]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class UnitManager
    {
        public readonly Instance CurrentInstance;
        private UnitHandle playerHandle;
        
        public UnitManager(Instance currentInstance)
        {
            CurrentInstance = currentInstance;
        }

        public UnitHandle getPlayerUnit()
        {
            return playerHandle ?? (playerHandle = new UnitHandle(GameManager.Instance.PlayerShip, this));
        }

        public UnitHandle getUnitByTag(string tag)
        {
            return new UnitHandle(FindByTag(tag), this);
        }

        public UnitHandle getUnitById(int id)
        {
            return new UnitHandle(FindById(id), this);
        }

        public UnitHandle spawnByType(string spawnableName, float x, float y)
        {
            var spawned = SpawningManager.Instance.SpawnDynamicShipPrefab(spawnableName);
            spawned.transform.SetParent(CurrentInstance.Container);
            CurrentInstance.JoinInstance(spawned, new Vector3(x, y));
            return new UnitHandle(spawned, this);
        }

        public UnitHandle warpInByType(string spawnableName, float x, float y)
        {
            var spawned = SpawningManager.Instance.SpawnDynamicShipPrefab(spawnableName);
            spawned.transform.SetParent(CurrentInstance.Container);
            spawned.OrderManager.HaltAndGiveOrder(new OrderInstaWarpTo(CurrentInstance, new Vector3(x, y) * 2, new Vector3(x, y)));
            spawned.OrderManager.EnqueOrder(new OrderStop());
            return new UnitHandle(spawned, this);
        }

        private EntityShip FindByTag(string tag)
        {
            return CurrentInstance.Entities.Find(e => e.ScriptableTag.Equals(tag)) as EntityShip;
        }

        private EntityShip FindById(int id)
        {
            return CurrentInstance.Entities.Find(e => e.Id == id) as EntityShip;
        }
    }
}