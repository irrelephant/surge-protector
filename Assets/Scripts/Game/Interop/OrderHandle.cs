﻿using System;
using System.Diagnostics.CodeAnalysis;
using Irrelephant.SurgeProtector.Game.AI.Orders;
using MoonSharp.Interpreter;

namespace Irrelephant.SurgeProtector.Game.Interop
{
    [MoonSharpUserData]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class OrderHandle
    {
        private Order order;

        internal OrderHandle(Order order)
        {
            this.order = order;
        }

        public OrderHandle onCompleted(DynValue fn)
        {
            if (fn.Type == DataType.Function)
            {
                order.OnOrderComplete += () => fn.Function.Call();
                return this;
            }

            throw new ArgumentException("Argument is not a function.");
        }

        public OrderHandle onCancelled(DynValue fn)
        {
            if (fn.Type == DataType.Function)
            {
                order.OnOrderCancelled += () => fn.Function.Call();
                return this;
            }

            throw new ArgumentException("Argument is not a function.");
        }

        public OrderHandle onQueued(DynValue fn)
        {
            if (fn.Type == DataType.Function)
            {
                order.OnOrderQueued += () => fn.Function.Call();
                return this;
            }

            throw new ArgumentException("Argument is not a function.");
        }

        public OrderHandle onStarted(DynValue fn)
        {
            if (fn.Type == DataType.Function)
            {
                order.OnOrderStarted += () => fn.Function.Call();
                return this;
            }

            throw new ArgumentException("Argument is not a function.");
        }
    }
}