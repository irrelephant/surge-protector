﻿using System.Diagnostics.CodeAnalysis;
using Irrelephant.SurgeProtector.Game.Instancing;
using Irrelephant.SurgeProtector.Tooling;
using MoonSharp.Interpreter;

namespace Irrelephant.SurgeProtector.Game.Interop
{
    [MoonSharpUserData]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class LuaDebug
    {
        private readonly Instance currentInstance;

        public LuaDebug(Instance currentInstance)
        {
            this.currentInstance = currentInstance;
        }

        public void log(string message)
        {
            ConsoleLog.Instance.LogFormat("[@{0}] {1}", EnumLogType.InstanceScript, currentInstance.InstanceId, message);
        }
    }
}