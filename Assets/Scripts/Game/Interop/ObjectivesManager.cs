﻿using System.Diagnostics.CodeAnalysis;
using MoonSharp.Interpreter;

namespace Irrelephant.SurgeProtector.Game.Interop
{
    [MoonSharpUserData]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class ObjectivesManager
    {
        public int addObjective(string objective)
        {
            return UiManager.Instance.ObjectiveDisplayControler.AddObjective(objective);
        }

        public void failObjective(int id)
        {
            UiManager.Instance.ObjectiveDisplayControler.FailObjective(id);
        }

        public void completeObjective(int id)
        {
            UiManager.Instance.ObjectiveDisplayControler.CompleteObjective(id);
        }

        public void editObjective(int id, string newObjective)
        {
            UiManager.Instance.ObjectiveDisplayControler.EditObjective(id, newObjective);
        }
    }
}
