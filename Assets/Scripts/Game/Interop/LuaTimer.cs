﻿using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using MoonSharp.Interpreter;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game.Interop
{
    [MoonSharpUserData]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class LuaTimer
    {
        private IEnumerator CallAfterCoroutine(float sec, Closure fn)
        {
            yield return new WaitForSeconds(sec);
            fn.Call();
        }

        private IEnumerator CallEveryCoroutine(float sec, Closure fn)
        {
            while (true)
            {
                yield return new WaitForSeconds(sec);
                fn.Call();
            }
        }

        public void callAfter(int ms, DynValue fn)
        {
            if (fn.Type == DataType.Function)
            {
                CoroutineRunner.Instance.AttachCoroutine(CallAfterCoroutine(ms / 1000f, fn.Function));
            }
            else
            {
                throw new ArgumentException("Second argument is not a function.");
            }
        }

        public void callEvery(int ms, DynValue fn) 
        {
            if (fn.Type == DataType.Function)
            {
                CoroutineRunner.Instance.AttachCoroutine(CallEveryCoroutine(ms / 1000f, fn.Function));
            }
            else
            {
                throw new ArgumentException("Second argument is not a function.");
            }
        }
    }
}
