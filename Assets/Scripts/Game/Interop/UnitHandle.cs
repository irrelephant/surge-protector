﻿using System;
using System.Diagnostics.CodeAnalysis;
using Irrelephant.SurgeProtector.Game.AI.Orders.Behaviours;
using Irrelephant.SurgeProtector.Game.Allegiance;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.Game.Interop.OrderFactories;
using MoonSharp.Interpreter;

namespace Irrelephant.SurgeProtector.Game.Interop
{
    [MoonSharpUserData]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class UnitHandle
    {
        public int unitId
        {
            get { return unit.Id; }
        }

        public string tag
        {
            get { return unit.ScriptableTag; }
        }

        private readonly EntityShip unit;

        private readonly UnitManager unitManager;

        internal UnitHandle(EntityShip unit, UnitManager unitManager)
        {
            this.unit = unit;
            this.unitManager = unitManager;
        }

        public float getPos(out float y)
        {
            var pos = unit.transform.position;
            y = pos.y;
            return pos.x;
        }

        public UnitHandle giveOrder(string orderName, params DynValue[] args)
        {
            unit.OrderManager.HaltAndGiveOrder(OrderFactoryRegistry.Find(orderName).Assemble(args));
            return this;
        }

        public UnitHandle enqueueOrder(string orderName, params DynValue[] args)
        {
            var order = OrderFactoryRegistry.Find(orderName).Assemble(args);
            if (order is BehaviourOrder)
            {
                throw new ArgumentException("Given order a behaviour and is not supposed to be enqueued. Use unitHandle.setBehaviour method instead.");
            }

            unit.OrderManager.EnqueOrder(order);
            return this;
        }

        public UnitHandle enqueueOrderWithCallback(string orderName, DynValue callback, params DynValue[] args)
        {
            if (callback.Type != DataType.Function)
            {
                throw new ArgumentException("Callback has to be a function.");
            }

            var order = OrderFactoryRegistry.Find(orderName).Assemble(args);
            if (order is BehaviourOrder)
            {
                throw new ArgumentException("Given order a behaviour and is not supposed to be enqueued. Use unitHandle.setBehaviour method instead.");
            }
            
            callback.Function.Call(new OrderHandle(order));
            unit.OrderManager.EnqueOrder(order);
            return this;
        }

        public UnitHandle setAllegiance(string factionId)
        {
            var faction = unitManager.CurrentInstance.GetFactionById(factionId);
            if (faction == null) {
                throw new ArgumentException("Couldn't find the faction by ID: " + factionId);
            }
            unit.Faction = faction;
            return this;
        }

        public string getAllegiance()
        {
            return unit.Faction.Id;
        }

        public string getRelations(UnitHandle other)
        {
            return Faction.GetRelations(unit.Faction, other.unit.Faction).ToString();
        }

        public UnitHandle setBehaviour(string behaviourName, params DynValue[] args)
        {
            var behaviour = OrderFactoryRegistry.Find(behaviourName).Assemble(args) as BehaviourOrder;
            if (behaviour != null)
            {
                unit.OrderManager.SetBehaviourOrder(behaviour);
            }
            else
            {
                throw new ArgumentException("Given order is not a behaviour. Use unitHanlde.enqueueOrder to add it to the order queue.");
            }

            return this;
        }

        public UnitHandle setTag(string tag)
        {
            unit.ScriptableTag = tag;
            return this;
        }

        public UnitHandle onDeath(DynValue fn)
        {
            if (fn.Type == DataType.Function)
            {
                unit.OnShipDestroyed += () => fn.Function.Call();
                return this;
            }

            throw new ArgumentException("Argument is not a function.");
        }

        public UnitHandle onDetected(DynValue fn)
        {
            if (fn.Type == DataType.Function)
            {
                unit.OnDetected += (detector) => fn.Function.Call(new UnitHandle(detector, unitManager));
                return this;
            }

            throw new ArgumentException("Argument is not a function.");
        }
    }
}