﻿using System.Linq;

namespace Irrelephant.SurgeProtector.Game
{
    public static class LayerConstants
    {
        public const int StaticGeometryLayer = 11;

        public const int SignatureLayer = 13;

        public const int Default = 0;

        public static int MaskFor(params int[] layers)
        {
            int resultMask = 0;
            for (int i = 0; i < layers.Length; i++)
            {
                resultMask += 1 << layers[i];
            }
            return resultMask;
        }
    }
}