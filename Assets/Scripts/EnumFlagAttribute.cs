﻿using UnityEngine;

public class EnumFlagAttribute : PropertyAttribute
{
    public int columnCount;
    public EnumFlagAttribute(int rowCount)
    {
        this.columnCount = rowCount;
    }
}