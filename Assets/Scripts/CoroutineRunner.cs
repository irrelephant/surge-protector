﻿using System.Collections;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Game
{
    public class CoroutineRunner : MonoBehaviour
    {
        public static CoroutineRunner Instance;

        public void Awake()
        {
            Instance = this;
        }

        public void AttachCoroutine(IEnumerator coroutine)
        {
            StartCoroutine(coroutine);
        }

        public void DetachCoroutine(IEnumerator coroutine)
        {
            StopCoroutine(coroutine);
        }
    }
}