﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Irrelephant.SurgeProtector.Game;
using Irrelephant.SurgeProtector.Game.Entities;
using Irrelephant.SurgeProtector.Game.Instancing;
using Irrelephant.SurgeProtector.Game.Persistance;
using Irrelephant.SurgeProtector.Game.Persistance.Schema;
using Irrelephant.SurgeProtector.UI.Hud;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Irrelephant.SurgeProtector.ScenarioEditor
{
    public class ScenarioEditorManager : MonoBehaviour
    {
        public Dropdown PlacementSelector;

        public Dropdown ModeSelector;

        public InputField ScenarioName;

        private Vector2 prevFramePosition;

        public Transform InstanceContainer;

        public Button SaveButton;

        public Button LoadButton;

        public Toggle RandomRotation;

        private InstanceManifest runningManifest;
        private InstanceManifest RunningManifest
        {
            get
            {
                return runningManifest ?? (runningManifest = new InstanceManifest
                {
                    Author = Environment.UserName
                });
            }
            set { runningManifest = value; }
        }

        [HideInInspector]
        public Transform Selected;

        public void Awake()
        {
            prevFramePosition = GetMousePosition();
           // PlacementSelector.options = SpawningManager.Instance.Library.Spawnables.Select(entry => new Dropdown.OptionData { text = entry.Name }).ToList();
        }

        public void Update()
        {
            var currentPos = GetMousePosition();
            var deltaMovement = currentPos - prevFramePosition;

            if (Input.GetKey(KeyCode.Space))
            {
                Camera.main.transform.Translate(-deltaMovement * 0.5f);
            }

            Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize - Input.mouseScrollDelta.y, 10f, 100f);

            if (!ScreenSpaceHoverCanvas.OverUi)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    switch (ModeSelector.value)
                    {
                        case 0:
                            PlaceObject(PlacementSelector.value, currentPos);
                            break;
                        case 1:
                            RemoveObject(currentPos);
                            break;
                        case 2:
                            SelectObject(currentPos);
                            break;
                    }
                    return;
                }
                if (ModeSelector.value == 2 && Selected != null && Input.GetMouseButton(0))
                {
                    Selected.transform.Translate(deltaMovement, Space.World);
                }

                if (Selected != null && Input.GetMouseButton(1))
                {
                    Selected.transform.Rotate(0, 0, deltaMovement.x);
                }
            }

            prevFramePosition = currentPos;
        }

        private void SelectObject(Vector2 currentPos)
        {
            var hit = Physics2D.Raycast(currentPos, Vector2.zero);
            if (hit && hit.transform.root == InstanceContainer)
            {
                if (Selected != null) SetObjectColor(Selected, Color.white);

                Selected = hit.transform;
                SetObjectColor(Selected, Color.green);
            }
            else
            {
                if (Selected != null) SetObjectColor(Selected, Color.white);
                Selected = null;
            }
        }

        private void SetObjectColor(Transform targetTransform, Color color)
        {
            var targetRenderer = targetTransform.GetComponent<Renderer>() ?? targetTransform.GetComponentInParent<Renderer>();
            targetRenderer.material.color = color;
        }

        private void PlaceObject(int val, Vector2 getMousePosition)
        {
           // var spawnLibEntry = SpawningManager.Instance.Library.Spawnables[val];
            //var placed = Instantiate(spawnLibEntry.Prefab,
            //                        getMousePosition,
           //                         Quaternion.Euler(0, 0, RandomRotation.isOn ? Random.Range(-180, 180) : 0),
          //                          InstanceContainer);
          //  placed.name = spawnLibEntry.Name;
        }

        private void RemoveObject(Vector2 mousePosition)
        {
            var hit = Physics2D.Raycast(mousePosition, Vector2.zero);
            if (hit)
            {
                var entity = hit.collider.GetComponentInParent<Entity>();
                if (Selected == entity)
                {
                    Selected = null;
                }
                Destroy(hit.transform.gameObject);
            }
        }

        private Vector2 GetMousePosition()
        {
            return Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        public void LoadScenario()
        {
            StartCoroutine(DisableAndEnable(SaveButton, 2f, "Loaded"));
            RunningManifest = ManifestRepository.ReadInstanceManifest(ScenarioName.text);
            InstanceLoader.LoadInstanceByManifest(RunningManifest, InstanceContainer, true);
        }

        public IEnumerator DisableAndEnable(Button button, float seconds, string modText)
        {
            var text = button.GetComponentInChildren<Text>();
            SaveButton.interactable = false;
            var originalText = text.text;
            text.text = "Saved";
            yield return new WaitForSeconds(seconds);
            button.interactable = true;
            text.text = originalText;
        }

        public void SaveScenario()
        {
            StartCoroutine(DisableAndEnable(SaveButton, 2f, "Saved"));

            UpdateRunningManifest();
            ManifestRepository.StoreInstanceManifest(RunningManifest);
        }

        private void UpdateRunningManifest()
        {
            RunningManifest.Entities = new List<EntityManifest>();
            RunningManifest.Objects = new List<ObjectManifest>();

            foreach (Transform instanceObject in InstanceContainer)
            {
                var entity = instanceObject.GetComponent<Entity>();
                if (entity != null)
                {
                    RunningManifest.Entities.Add(BuildObjectManifestForTransform(instanceObject));
                }
                else
                {
                    RunningManifest.Objects.Add(BuildObjectManifestForTransform(instanceObject));
                }
            }

            RunningManifest.ScenarioId = ScenarioName.text;
            RunningManifest.ScenarioName = ScenarioName.text;
        }

        private static EntityManifest BuildObjectManifestForTransform(Transform e)
        {
            return new EntityManifest
            {
                PrefabName = e.name,
                RandomRotation = false,
                Rotation = e.transform.rotation.eulerAngles.z,
                X = e.transform.position.x,
                Y = e.transform.position.y
            };
        }
    }
}