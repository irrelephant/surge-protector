﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Irrelephant.SurgeProtector.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> Each<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var e in source)
            {
                action(e);
                yield return e;
            }
        }

        public static IEnumerable<T> Apply<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var e in source)
            {
                action(e);
            }

            return source;
        }

        public static IEnumerable<T> Apply<T, T2>(this IEnumerable<T> source, Func<T, T2> action)
        {
            foreach (var e in source)
            {
                action(e);
            }

            return source;
        }

        public static IDictionary<T1, T2> Apply<T1, T2>(this IDictionary<T1, T2> source, Action<T1, T2> action)
        {
            foreach (var kvp in source) {
                action(kvp.Key, kvp.Value);
            }

            return source;
        }

        public static void ApplyToPairs<T>(this IEnumerable<T> source, Action<T, T> action)
        {
            var sourceArray = source as T[] ?? source.ToArray();
            if (sourceArray.Length < 2) return;

            for (int i = 0; i < sourceArray.Length - 1; i++)
            {
                action(sourceArray[i], sourceArray[i + 1]);
            }
        }
    }
}
