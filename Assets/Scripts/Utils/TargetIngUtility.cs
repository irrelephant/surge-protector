﻿using System.Collections;
using System.Collections.Generic;
using Irrelephant.SurgeProtector.Game;
using Irrelephant.SurgeProtector.Game.Entities;
using UnityEngine;

namespace Irrelephant.SurgeProtector.Utils {
	public class TargetingUtility {
        private const int RaycastBufferSize = 20;
        protected static RaycastHit2D[] RaycastBuffer = new RaycastHit2D[RaycastBufferSize];

		protected static Collider2D[] OverlapBuffer = new Collider2D[RaycastBufferSize];

		public static EntityShip GetEntityAtPoint(Vector3 position, float toleranceRadius) {
			var count = Physics2D.CircleCastNonAlloc(position, toleranceRadius, Vector2.up, RaycastBuffer, 0f, LayerConstants.MaskFor(LayerConstants.SignatureLayer));
            for (int i = 0; i < count; i++) {
                var ship = RaycastBuffer[i].transform.GetComponentInParent<EntityShip>();
                if (ship != null) {
					return ship;
				}
            }
			return null;
		}

		public static IList<EntityShip> GetEntitiesInBox(Rect rect) {
			var worldRect = WorldSpaceUtility.ScreenRectToDiagonalPoints(rect);
			var count = Physics2D.OverlapAreaNonAlloc(worldRect.xy(), worldRect.zw(),
			OverlapBuffer, LayerConstants.MaskFor(LayerConstants.SignatureLayer));

			var shipList = new List<EntityShip>();

            for (int i = 0; i < count; i++) {
                var ship = OverlapBuffer[i].transform.GetComponentInParent<EntityShip>();
                if (ship != null) {
					shipList.Add(ship);
				}
            }
			
			return shipList;
		}
	}
}