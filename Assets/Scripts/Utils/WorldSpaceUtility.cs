﻿using UnityEngine;

namespace Irrelephant.SurgeProtector.Utils {
	public static class WorldSpaceUtility {
	
		/// <summary>
		/// Converts the Y coordinate to screen space from the world space.
		/// </summary>
		/// <returns>The to screen space y.</returns>
		/// <param name="y">The y coordinate.</param>
		public static float WorldToScreenSpaceY(float y) {
			return Screen.height - y;
		}

		/// <summary>
		/// Converts screen-space rect to vector4, containing it's world position in XY coords and its extents in ZW coords.
		/// </summary>
		/// <param name="rect">Rect to convert</param>
		/// <returns></returns>
		public static Vector4 ScreenRectToWorldOriginAndExtents(Rect rect) {
			var camera = UiManager.Instance.CameraManager.ControlledCamera;
			var topRight = camera.ScreenToWorldPoint(new Vector3(rect.xMax, WorldToScreenSpaceY(rect.yMax)));
			var bottomLeft = camera.ScreenToWorldPoint(new Vector3(rect.xMin, WorldToScreenSpaceY(rect.yMin)));
			var extents = topRight - bottomLeft;
			var center = bottomLeft + extents / 2f;
			return new Vector4(center.x, center.y, extents.x, -extents.y);
		}

		/// <summary>
		/// Converts screen-space rect to vector4, containing its bottomLeft and topRight points in XY and ZW coords respectively
		/// </summary>
		/// <param name="rect">Rect to convert</param>
		/// <returns></returns>
		public static Vector4 ScreenRectToDiagonalPoints(Rect rect) {
			var camera = UiManager.Instance.CameraManager.ControlledCamera;
			var topRight = camera.ScreenToWorldPoint(new Vector3(rect.xMax, WorldToScreenSpaceY(rect.yMax)));
			var bottomLeft = camera.ScreenToWorldPoint(new Vector3(rect.xMin, WorldToScreenSpaceY(rect.yMin)));
			return new Vector4(bottomLeft.x, bottomLeft.y, topRight.x, topRight.y);
		}

	}
}