using UnityEngine;

namespace Irrelephant.SurgeProtector.Utils {
	public static class VectorMathUtility {
	
		public static Vector2 xy(this Vector4 v4) {
            return new Vector2(v4.x, v4.y);
        }

        public static Vector2 zw(this Vector4 v4) {
            return new Vector2(v4.z, v4.w);
        }

        public static Vector3 Rotate(this Vector3 v, float degrees)
        {
            float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
            float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

            float tx = v.x;
            float ty = v.y;
            v.x = cos * tx - sin * ty;
            v.y = sin * tx + cos * ty;
            return v;
        }
	}
}