﻿namespace Irrelephant.SurgeProtector.Utils
{
    public static class IdGenerator
    {
        private static int currentLastId = 1;

        public static int LastId
        {
            get { return currentLastId; }
        }

        public static int GetNextId()
        {
            return ++currentLastId;
        }
    }
}