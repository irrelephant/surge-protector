﻿using UnityEngine;

public class WarpEffectController : MonoBehaviour
{
    private ParticleSystem warpParticles;
    private bool isWarping;
    public Transform WarpingTarget;

    public static WarpEffectController Instance;

    public void Start()
    {
        warpParticles = GetComponent<ParticleSystem>();
        Instance = this;
    }

    public void Update()
    {
        if (isWarping && WarpingTarget != null)
        {
            warpParticles.transform.position = WarpingTarget.transform.position + WarpingTarget.transform.up * 20f;
            warpParticles.transform.rotation = Quaternion.Euler(90 - WarpingTarget.transform.eulerAngles.z, 90, 90);
        }
    }

    public void StartWarp()
    {
        var emitter = warpParticles.emission;
        emitter.enabled = true;
        isWarping = true;
    }

    public void StopWarp()
    {
        var emitter = warpParticles.emission;
        emitter.enabled = false;
        isWarping = false;
    }
}