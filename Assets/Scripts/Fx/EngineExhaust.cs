﻿using UnityEngine;

public class EngineExhaust : MonoBehaviour
{
    private float intensity;
    private float maxEngineIntensity = 3f;
    private float maxParticles = 300f;

    private ParticleSystem engineParticles;
    private ParticleSystem.EmissionModule emmisionModule;
    private Light engineLight;

    public bool engineActive;

    public void Start()
    {
        engineParticles = GetComponentInChildren<ParticleSystem>();
        emmisionModule = engineParticles.emission;
        engineLight = GetComponent<Light>();
    }

    public void Update()
    {
        if (engineActive)
        {
            intensity = Mathf.Lerp(intensity, 1f, 0.05f);
        }
        else
        {
            intensity = Mathf.Lerp(intensity, 0, 0.05f);
        }

        emmisionModule.rateOverTime = maxParticles * intensity * intensity;
        engineLight.intensity = intensity * maxEngineIntensity;
    }
}