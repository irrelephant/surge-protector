﻿using UnityEngine;

public class ParticleLibrary : MonoBehaviour
{
    public static ParticleLibrary Instance;

    public ParticleSystem ProjectileHitFx;

    public void Start()
    {
        Instance = this;
    }
}