﻿using UnityEngine;

namespace Irrelephant.SurgeProtector.Fx
{
    [RequireComponent(typeof(LineRenderer))]
    [ExecuteInEditMode]
    public class SmartTrail : MonoBehaviour
    {
        private LineRenderer trailRenderer;

        public Vector3[] TrailPositions;

        public int TrailLength;

        private int actualTrailLength;

        public void Awake()
        {
            trailRenderer = GetComponent<LineRenderer>();
            TrailPositions = new Vector3[TrailLength];
        }

        public void Update()
        {
            if (TrailLength == 0) return;

            ShiftTrailPositions(transform.position);
            SetTrailPositions();
        }

        private void SetTrailPositions()
        {
            if (actualTrailLength < 2)
            {
                return;
            }

            trailRenderer.positionCount = actualTrailLength;
            if (actualTrailLength < TrailLength)
            {
                for (int i = 0; i < actualTrailLength; i++)
                {
                    trailRenderer.SetPosition(i, TrailPositions[i]);
                }
            }
            else
            {
                trailRenderer.SetPositions(TrailPositions);
            }
        }

        private void ShiftTrailPositions(Vector3 pointToAdd)
        {
            actualTrailLength = Mathf.Min(TrailLength, actualTrailLength + 1);
            for (int i = actualTrailLength - 1; i > 0; i--)
            {
                TrailPositions[i] = TrailPositions[i - 1];
            }
            TrailPositions[0] = pointToAdd;
        }
    }
}