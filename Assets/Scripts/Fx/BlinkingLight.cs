﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Light))]
[RequireComponent(typeof(SpriteRenderer))]
public class BlinkingLight : MonoBehaviour
{
    private Light lightToBlink;
    private SpriteRenderer lightSprite;

    public float Period;
    public float Delay;

    public void Start()
    {
        lightSprite = GetComponent<SpriteRenderer>();
        lightToBlink = GetComponent<Light>();

        lightSprite.enabled = false;
        lightSprite.enabled = false;

        StartCoroutine(BlinkEvery(Period));
    }

    private IEnumerator BlinkEvery(float period)
    {
        yield return new WaitForSeconds(Delay);

        var delay = new WaitForSeconds(period - 0.05f);
        var blinkLength = new WaitForSeconds(0.05f);
        while (true)
        {
            lightToBlink.enabled = true;
            lightSprite.enabled = true;
            yield return blinkLength;
            lightToBlink.enabled = false;
            lightSprite.enabled = false;
            yield return delay;
        }
    }
}
