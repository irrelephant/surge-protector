scout = nil;
miners = nil;
bargeDetected = false;
collectionDone = false;

resourcesMin = 5;
resourcesDelta = 5;
resoucesCollected = 0;
resourcesRequired = 150;

destroyBargeObjectiveId = nil;
collectResourcesObjectiveId = nil;
protectMinersObjectiveId = nil;

warpoutX = 200;
warpoutY = 30;

refineryIndex = 0;

function spawnScout()
  local px, py = unitManager.getPlayerUnit().getPos();
  scout = unitManager
    .warpInByType("ReconCruiser", px + 5, py + 5)
    .setTag("SCOUT_CRUISER")
    .setAllegiance("RED_DWARF_GUARD")
    .enqueueOrderWithCallback("ORDER_STOP", (function(order)
      order.onCompleted(function()
        chat.addMessage(chat.color("Red Guard Scout: ", "#0088FF") .. "I am in position.");
      end);
    end))
    .setBehaviour("BEHAVIOUR_AGGRESSIVE");

  chat.addMessage(chat.color("Red Guard Scout: ", "#0088FF") .. "Long range scanners show \z
    signs of Scrappers activity in the area. Proceed with caution.");
end

function spawnRefineryNearPoint(x, y)
  local mx = 100 - math.random(20);
  local my = math.random(70) - 35;
  refineryIndex = refineryIndex + 1;

  debug.log("Moving to (" .. mx .. ", " .. my ..")");
  return unitManager
    .warpInByType("MobileRefinery", x + math.random(40) - 20, y + math.random(40) - 20)
    .setTag("REFINERY_" .. refineryIndex)
    .setAllegiance("HULK_IVORY")
    .enqueueOrder("ORDER_MOVE_TO", mx, my)
    .enqueueOrder("ORDER_STOP");
end

function onPirateBargeDestroyed()
  if (destroyBargeObjectiveId != nil) then
    objectiveManager.completeObjective(destroyBargeObjectiveId);
    chat.addMessage(chat.color("Red Guard Scout: ", "#0088FF") .. "Stolen industrial vessel is destroyed.");
  end
end

function onPirateBargeDetected(detector)
  if (destroyBargeObjectiveId == nil and detector.getAllegiance() == "PLAYER") then
    destroyBargeObjectiveId = objectiveManager.addObjective(chat.color("Optionally", "#F7D918") .. ", destroy the pirate barge.");
    chat.addMessage(chat.color("Red Guard Scout: ", "#0088FF") .. "We have picked up a signutare \z
      of an unknown industrial-class vessel. Scrappers probably use it as a base of operations. We \z
      should seek opportunities to sink it, but " .. chat.bold("our priority is to ensure safe \z
      mining for the Ivory Guild."));
  end
end

function spawnPirates()
  local px, py = unitManager.getPlayerUnit().getPos();
  unitManager
    .spawnByType("CommandCruiser", px + math.random(40), py + math.random(40))
    .setAllegiance("SCRAP_COLLECTORS")     -- [ This is a locally-defined faction. See appropariate Manifest.json for details ] --
    .enqueueOrder("ORDER_MOVE_TO", px, py)
    .enqueueOrder("ORDER_STOP")
    .setBehaviour("BEHAVIOUR_AGGRESSIVE")
    .onDeath(onPirateBargeDestroyed)
    .onDetected(onPirateBargeDetected);
end

function spawnMiners()
  local px, py = unitManager
    .getPlayerUnit()
    .getPos();
  miners = {
    spawnRefineryNearPoint(px, py),
    spawnRefineryNearPoint(px, py),
    spawnRefineryNearPoint(px, py),
    spawnRefineryNearPoint(px, py)
  };
end

function completeCollectionAndEvac()
  resoucesCollected = resourcesRequired;
  objectiveManager.completeObjective(collectResourcesObjectiveId);
  for _, miner in ipairs(miners) do
    miner
      .giveOrder("ORDER_MOVE_TO", warpoutX, warpoutY)
      .enqueueOrderWithCallback("ORDER_WARP_OUT", function(order)
        order.onStarted(function()
          objectiveManager.completeObjective(protectMinersObjectiveId);
        end);           
      end);
  end
end

function updateResources()
  if (not collectionDone) then
    resoucesCollected = resoucesCollected + resourcesMin + math.floor(resourcesDelta * math.random());
    if (resoucesCollected >= resourcesRequired) then
      collectionDone = true;
      completeCollectionAndEvac();
    end

    objectiveManager.editObjective(collectResourcesObjectiveId, getResourceCollectionObjectiveText());
  end
end

function getResourceCollectionObjectiveText()
  return "Collect the Loridium (" .. resoucesCollected .. "/" .. resourcesRequired .. ").";
end

function main()
  math.randomseed(os.time());
  timer.callAfter(1 * 1000, spawnScout);
  timer.callAfter(2 * 1000, spawnMiners);
  timer.callAfter(5 * 1000, spawnPirates);
  timer.callEvery(5 * 1000, updateResources);
  protectMinersObjectiveId = objectiveManager.addObjective("Protect the mining operation from the " .. chat.color("Scrap Collectors", "#B40000") .. ".");
  collectResourcesObjectiveId = objectiveManager.addObjective(getResourceCollectionObjectiveText());
  debug.log("Instance script initialized.");
end
