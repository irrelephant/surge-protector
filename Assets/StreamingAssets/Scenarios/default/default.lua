function main()
  debug.log("Instance script initialized.");
  timer.callAfter(1000, function()
    local px, py = unitManager.getPlayerUnit().getPos();
    unitManager
        .spawnByType("ReconCruiser", px + math.random(40), py + math.random(40))
        .setAllegiance("STEEL_VULTURES")
        .enqueueOrder("ORDER_STOP")
        .setBehaviour("BEHAVIOUR_AGGRESSIVE");
  end)
end